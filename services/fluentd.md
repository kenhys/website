---
layout: default
main_title: Fluentdに関するよくある質問集
sub_title:
type: service
Description: 拡張性の高いオープンソースのログ収集プログラム『Fluentd』に関する情報をよくある質問集の形でまとめています。
toc:
  h_min: 3
---

## Fluentdに関するよくある質問集

### Q1. Fluentdはどんなプログラムですか

**A1.** 『Fluentd』は、拡張性の高いオープンソースのログ収集プログラムです。
1000以上のプラグインを通じて、世界中のあらゆるサービスと接続することができます。
[Cloud Native Comuputing Foundation (CNCF)](https://www.cncf.io/) 傘下のプロジェクトの一つであり、オンプレミス・コンテナ環境の垣根を超えて世界中で幅広く利用されています。

### Q2. 情報はどこで知ることができますか
**A2.**  [Fluentdプロジェクトページ](https://www.fluentd.org/)
  Fluentdの公式情報は英語でアナウンスされています。英語の公式ページでは、プロジェクトの概要や、利用可能なデータの種類、プラグインなどを確認することができます。

### Q3. Fluentdのライセンスはなんですか
**A3.** FluentdはオープンソースのApache License 2.0で提供されています。詳しくは[GiHubのライセンスページ（英語）で確認できます。](https://github.com/fluent/fluentd/blob/5844f7209fec154a4e6807eb1bee6989d3f3297f/LICENSE)


### Q4. Fluentdはどこでダウンロードできますか
**A4.** 現在のパッケージは[fluentd.orgのダウンロードページ](https://www.fluentd.org/download)からダウンロードが可能です。

コミュニティでメンテナンス及び公開しているFluent Package(通常版)及びFluent Package LTS(Long Term Support版)のほかに、一部サービス用のパッケージがあります。

### Q5. リリースはどうなっていますか
**A5.** Fluentdは月末ごろに新しいバージョンがリリースされています。リリースの内容については公式ブログでリリースノートが紹介されています。また、ククログではすべてのリリースノートではないですが、大きな変更があったときなどに日本語でリリースノートを解説した記事を紹介しています。

* [Fluentd blog(英語)](https://www.fluentd.org/blog/)
* [ククログ Fluentdタグ（日本語）](https://www.clear-code.com/blog/fluentd/)

パッケージのリリースは、通常版は不定期にマイナーアップデートが行われ、LTS版は数か月に一度のバグフィクス及びセキュリティパッチと1年に一度のメジャーアップデートがあります。

### Q6. 過去のバージョン情報についてしりたいのですがどこで確認できますか
**A6.** これまでのFluentd及びFluent Package(旧：td-agent）とそれに同梱されているFluentd本体のバージョンについてはGitHub上にまとまっています。
* [Fluentd](https://github.com/fluent/fluentd/wiki/Releases)
* [Fluent Package](https://github.com/fluent/fluentd/wiki/The-chronicle-of-Fluent-Package)
  * 過去パッケージ（td-agent）の情報は[こちら](https://github.com/fluent/fluentd/wiki/The-chronicle-of-TD-Agent)
  
### Q7. Fluentdのサポート期間はどのくらいですか
**A7.** 新しいマイナーバージョンがリリースされると、その前のマイナーリリースバージョンはサポートされなくなります。たとえば、1.15.0 がリリースされると、1.14 系はサポートされなくなります。
(クリアコードでは、そういった古いバージョンをお使いのお客様向けにも[有償サポート]({% link services/fluentd-service.md %})を提供しています。)

パッケージ版については、通常版は不定期のマイナーアップデートがあり、アップデート後はその前のマイナーバージョンのサポートがなくなります。LTS版はマイナーアップデートが適用されず、バグフィックス及びセキュリティフィックスのパッチ提供のサポートが最低1年間継続します。メジャーアップデート後サポートがなくなります。

### Q8. Fluentdはいつも更新しないとだめですか
**A8.** バージョンアップでは、セキュリティ脆弱性や安定性の問題が修正され様々な機能が追加されます。安心してインターネットをお使いいただくため、できるだけ速やかに更新を適用することをお勧めします。最新版はGitHubのプロジェクトページでダウンロードが可能です。

### Q9. トラブルシューティングしたいときはどうすればいいですか
**A9.** 　[トラブルシューティングガイド]({% link downloads/fluentd-troubleshooting-guide.md %})を提供しています。また、ククログでいくつかの事例を紹介しています。

### Q10. バグなどがあった場合はどうすればいいですか
**A10.** Fluentdは、多くの人が開発に参加できるオープンソースのソフトウェアです。バグなどに遭遇した場合、GitHubからレポートしやすいようにテンプレートを使って報告できるようになっています。

[GitHub Issue作成ページ](https://github.com/fluent/fluentd/issues/new/choose)

### Q11. Fluentdの導入の手順はどこで確認できますか
**A11.** 公式ドキュメントがまとまっています。（英語のみ）　https://docs.fluentd.org/installation

### Q12. Fluentdと関連ソフトウェアの構築などの使い方はどこかにまとまっていますか
**A12.** KubernetesとFluentdをつかったログ収集に関してわかりやすく解説している入門編の動画があります。
<div class="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/BaJ2gXrZi3I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

### Q13. エンタープライズ向けのサポートサービスはありますか
**A13.** あります。エンタープライズ向けのサポートを提供している事業者は[公式ページにまとまっています。](https://www.fluentd.org/enterprise_services)（英語）

2015年からFluentd/Fluent Bit開発に参加しているクリアコードでは、Fluentd/Fluent Bitをエンタープライズ環境において導入/運用されるSIer様、サービス提供事業者様に対して以下のサービスを提供します。詳しくは[サポートサービス紹介ページ]({% link services/fluentd-service.md %})をご覧ください。
サービスに関するお問い合わせは、こちらの [お問い合わせフォーム]({% link contact/index.md %}) からご連絡ください。

### Q14. FluentdとFluent Bitの違いをおしえてください
**A14.** FluentdBitはFluentdの軽量版といわれており、Fluentdが主にRubyで実装されているのに対して、FluentBitは主にC言語で作られています。詳しくは[ブログで解説しています。](https://www.clear-code.com/blog/2021/5/18.html)

### Q15. FluentdとFluent Package(旧：td-agent）とFluent Package LTSはどう違うのですか？
**A15.** Fluentdは、ログ収集を行うプログラム本体です。

Fluent Package(旧：td-agent)は、Fluentdを含んだパッケージディストリビューションです。実際にプログラムを動作させるために必要なRubyや依存ライブラリーに加え、よく利用される3rdパーティープラグインが同梱されています。運用されるプラットフォーム環境に合わせて、RHEL / CentOS / Amazon Linux 、Ubuntu/ Debian 、Windows向けのパッケージが用意されています。不定期に機能追加などのマイナーアップデートが行われ、マイナーアップデート後、旧バージョンのメンテナンスは停止します。

Fluent Package LTS（長期サポート版）は、Fluent Packageと同様にFluentdを含んだパッケージディストリビューションです。通常版で不定期に行われるマイナーアップデートの適用が行われず、最低1年間のバグフィックスとセキュリティフィックスのパッチが提供され、メンテナンスが継続されます。その後メジャーバージョンアップが行われると、旧バージョンのメンテナンスは停止します。

### Q16. Fluentdや関連プラグインを使うのにお金はかかりますか？

**A16.** Fluentd及びどのパッケージも基本的にはソフトウェアの利用に費用はかかりません。

特定の環境などの利用に合わせたプラグインや、機能追加などが必要でご自分で作成するのが難しい場合は有料の技術サポートにお申込みいただくことをお勧めします。もし何か機能追加希望やご自分で作成されたプラグインの本体へのmergeをご希望の場合はpull requestをご利用ください。（※本体にmergeされるかどうかは内容によります）

### Q17. Fluentdと他のソフトウェアをセットでサポートしてもらうことはできますか

**A17.** クリアコードでは、[Fluentdのサポートサービス]({% link services/fluentd-service.md %})と合わせて他のオープンソースソフトウェア（nginxやRedisなど）のサポートもセットでご提供する事例があります。お気軽に [お問い合わせフォーム]({% link contact/index.md %}) からご相談ください。

このようなサポート事例について以下のブログで紹介しておりますので、ぜひご覧ください。

* [Fluentdサポート事例: Postfixのログが途中で途切れてしまう]({% post_url 2024-06-26-fluentd-support-case-postfix-log-truncated %})
* [サポート事例紹介: Redisのフェールオーバーの原因調査]({% post_url 2024-09-06-fluentd-support-case-redis-failover %})

---

こちらのページに記載があると嬉しい質問や情報などがあれば、[merge request](https://gitlab.com/clear-code/website/-/merge_requests)もしくは[Twitter](https://twitter.com/_clear_code)で教えていただけると可能な範囲で追加いたします。
