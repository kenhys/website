---
layout: default
main_title: Fluent Bitのご紹介
sub_title:
type: service
---

ログ監視はあらゆるサーバー運用に欠かすことができないものです。SplunkやElasticsearchに代表される統合的な分析アプリケーションの普及にともなって、ログはリアルタイムで監視するのが当たり前の時代となりました。このログのリアルタイム監視を支えているのが、運用サーバーと監視アプリケーションの間を接続する「ログ転送プログラム」です。

[Fluent Bit](https://fluentbit.io)は、Treasure Data社が開発した新しいログ転送プログラムです。サーバーでリアルタイムに生成されるログデータを、様々な出力先に自在に転送することができる、とても優れたプログラムです。

<div class="text-center">

![Fluent Bitの概念図](fluent-bit/fluent-bit-image.png)

</div>

## Fluent Bitの特徴

* C言語で実装されているので、高速に動作します。
* リソースの乏しい組み込み環境でも動作します。
* Apache Kafka・Splunk・Elasticsearchなどの数多くのサービスに転送できます。
* Stream Processorという集計エンジンを内蔵しており、SQLライクな構文でリアルタイムでデータ集計を実行できます。

従来のログ転送プログラムはJavaやRubyといった高級な言語で実装されていますが、Fluent BitはC言語でコンパクトに実装されているため、動作が軽快で、メモリ消費も少ないという際立った特性があります。このため、実行リソースの限られる組み込み環境やKubernetesなどのコンテナ環境で使えるログ収集プログラムとして、様々なプロジェクトで使われています。

### Fluent Bitはどれだけ速いのか？

Fluent Bitの特性の一つは高速であることです。この特徴を具体的に示すために、以下にスループット測定試験の結果を掲載します。比較に利用したバージョンはFluent Bit v1.3.5とFluentd v1.8.1で（いずれも本記事執筆時点の最新版）、計測は Intel Core i7-8700T (2.40GHz) を搭載した弊社の開発マシンで行いました。

<div class="text-center">

![Fluent BitとFluentdの性能比較](fluent-bit/fluent-bit-perf.png)

</div>

例えば、LTSV形式のログ（左のグラフ）を取り上げると、Fluent Bitを使うことで、Fluentdのおおよそ2倍に相当する20万行／秒を処理できることが分かります。

### Fluent Bitの利用例

2014年の開発開始からFluent Bitの利用は飛躍的に進んでいます。2019年時点で、毎日10万台以上の環境にデプロイされています。特に、近年はAWSに代表される各種のクラウドプロバイダとの統合が進んでいます。

*AWS*

* [「AWS Container Services が AWS 向け Fluent Bit プラグインをリリース」](https://aws.amazon.com/about-aws/whats-new/2019/07/aws-container-services-launches-aws-for-fluent-bit/) (2019年7月9日)
* [「AWS for Fluent Bit が Amazon Kinesis Data Streams をサポート」](https://aws.amazon.com/jp/about-aws/whats-new/2019/11/aws-for-fluent-bit-now-supports-amazon-kinesis-data-streams/) (2019年11月26日)

*Datadog*

* [「Centralize your logs with Datadog and Fluent Bit」](https://www.datadoghq.com/blog/fluentbit-integration-announcement/) (2019年10月8日)

## クリアコードとの関わり

クリアコードはFluent Bitの開発にコミットしています。2018年より開発に継続的に参加しており、本記事の執筆時点で、弊社のエンジニアが3番目に多くコミットした開発者（下図の緑枠）となっています。

クリアコードは本プロダクトについてソースコードレベルのサポートを提供している、おそらく日本で唯一の企業です。

<div class="text-center">

![Fluent Bitのコミット数](fluent-bit/fluent-bit-commit.png)

</div>

### サポートの問い合わせ

クリアコードではFluent Bitのサポートサービスを提供しています。自社のシステムで使ってみたい、また使っていて解決したい点があるなどの課題がございましたら、[こちらのフォーム](/contact/)からお問い合わせください。
