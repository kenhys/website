---
layout: default
main_title: db tech showcase 2021にクリアコード　須藤功平が登壇
sub_title: 
type: press
---

<p class='press-release-subtitle-small'>
11/17（水） 15:30- Apache Arrow Flight – ビッグデータ用高速データ転送フレームワーク</p>

<div class="press-release-signature">
  <p class="date">2021年11月15日</p>
  <p>株式会社クリアコード</p>
</div>

株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平）は、株式会社インサイトテクノロジー主催、国内最大規模のデータとデータベース関連のカンファレンス『db tech showcase 2021』にて、代表取締役　須藤功平が、2021年11月17日（水曜日）15時30分より『Apache Arrow Flight – ビッグデータ用高速データ転送フレームワーク』というタイトルで登壇することをお知らせします。

今回のセッションではApache Arrow Flightの仕組みを中心に紹介します。また、分散計算プラットフォームApache Arrow BallistaではどのようにApache Arrow Flightを利用しているかも紹介します。

【講演事前情報】
[{{ site.url }}{% post_url 2021-11-15-db-tech-showcase-2021 %}]({% post_url 2021-11-15-db-tech-showcase-2021 %})

## 講演の概要
* **日時**：2021年11月17日（水）15時30分-16時15分
* **講演タイトル**：Apache Arrow Flight – ビッグデータ用高速データ転送フレームワーク
* **講演者プロフィール**:  須藤功平（Apache Arrowプロジェクト管理委員会メンバー）

2016年よりApache Arrowの開発に参加し、2017年5月にコミッターに就任、2017年9月にプロジェクト管理委員会メンバーに就任。2021年11月時点でコミット数は2位。日本でApache Arrowを普及させるため、開発するだけでなく各所でApache Arrowについて紹介している。

【Apache Arrow関連講演事例】

* [『Apache Arrowフォーマットはなぜ速いのか』](https://bit.ly/3wnOwLH)

* [『Apache Arrow 1.0 - A cross-language development platform for in-memory data』](https://bit.ly/3mTK58j)

* [『Red Arrow - Ruby and Apache Arrow』](https://bit.ly/3mR9QWH)


## db tech showcase 2021概要
* **開催日程**：2021年11月17日（水）～19日（金）
* **会場**：オンライン
* **参加費**：無料（事前登録制）
* **公式サイト**：https://www.db-tech-showcase.com/2021/

[![]({% link /press-releases/20211115-db-tech-showcase-2021/dbts2021_banner2_jp.png %})](https://www.db-tech-showcase.com/2021/)[^1]
[^1]: db tech showcase 2021 banner© 2021 Insight Technology, Inc. バナー画像はイベント集客のためだけに使用しています。


## Apache Arrowについて
現代のデータ処理システムは複数のデータ処理モジュールを連携して構築されており、データ量の多い環境においては各モジュール間でデータ交換を効率化が必須です。この効率的なデータ交換のために設計されたデータフォーマットが「Apache Arrow」です。

Apache Arrow FlightはそのようなApache Arrowフォーマットの特徴を活かしてネットワーク越しに高速にデータを転送するフレームワークです。単なるデータ交換のためだけでなく、分散システムの基盤の実装にも活用できます。


## クリアコードについて
クリアコードは、 2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。クリアコードの目的は、単に会社を継続していくことではありません。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。この理念は、我々がフリーソフトウェアの開発で学んだことがベースとなっています。

2016年からApache Arrowの開発に参加しており、[Apache Arrow活用を支援するコンサルティングサポートサービス]({% link services/apache-arrow.md %})を提供しています。


### 参考URL
【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})

【本プレスリリース】[{{ site.url }}{% link press-releases/20211115-db-tech-showcase-2021.md %}]({% link press-releases/20211115-db-tech-showcase-2021.md %})

【関連サービス】[{{ site.url }}{% link services/apache-arrow.md %}]({% link services/apache-arrow.md %})


### 当リリースに関するお問合せ先
株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com
