---
layout: default
main_title: 年末年始休業のお知らせ
sub_title:
type: topic
---

お客さま各位


平素は格別のお引き立てを賜り、厚く御礼申し上げます。

弊社は、誠に勝手ながら下記の期間を年末年始休業とさせていただきます。


休業期間：2024年12月28日（土）から2025年1月5日（日）

（1月6日（月）より通常営業となります。）


上記期間中に受け付けましたお問い合わせは1月6日（月）より順次対応させていただきますので、ご了承ください。
