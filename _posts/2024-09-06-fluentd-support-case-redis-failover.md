---
title: "サポート事例紹介: Redisのフェールオーバーの原因調査"
author: daipom
tags:
  - free-software
  - fluentd
---

こんにちは。データ収集ツール[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

今回の記事では、有名なインメモリデータベースである[Redis](https://redis.io/)の障害調査を行った事例を紹介します。

また、この調査は[クリアコードのFluentd法人様向けサポートサービス]({% link services/fluentd-service.md %})の一貫で実施したもので、
Fluentdとセットで周辺のフリーソフトウェアのサポートを実施した事例となっています。

Redisのトラブルシューティングや、クリアコードの法人様向けサポートサービスに興味のある方は、ぜひ本記事をご覧ください。

<!--more-->

### 問題発生から調査実施までの経緯

今回のサポート事例は、お客様の[Redis](https://redis.io/)のシステムにおいてフェイルオーバー[^failover]が発生し、その原因を調査した、というものになります。

お客様は[クリアコードのFluentd法人様向けサポートサービス]({% link services/fluentd-service.md %})をご利用いただいており、
Fluentdとセットで他のフリーソフトウェアについてもサポートをご提供しておりました。
今回はこのFluentdサポートの一貫としてRedisの調査もできないかとご相談いただき、実施することとなりました。

また、Redisについてはこれまでのサポート実績がなく知見がなかったため、1からの調査となります。
そのため通常よりも調査に時間がかかる可能性があることを事前にお伝えし、ご了承いただいた上で調査を開始しました。

[^failover]: フェイルオーバー: 稼働中のシステムに障害が発生した際に待機中のシステムに切り替えることで、なるべくシステムが止まらないようにすること。

### Redis Clusterのフェイルオーバー

今回の事例では、[Redis Cluster](https://redis.io/docs/latest/operate/oss_and_stack/management/scaling/)という機能のフェイルオーバー[^failover]が発生しました。

Redis Clusterは、複数のRedisノードを並列稼働させてスケールさせることができます。
その際、特定のノードで障害が発生しても、フェイルオーバーをして処理を継続できるようにします。
例えば、あるマスターノードで障害が発生しても、対応するレプリカノードがマスターノードに切り替わって処理を継続できます。

今回の事例ではこのフェイルオーバーが発生したため、その障害原因を調査して明らかにしました。

### 調査結果: 負荷の高いコマンドの手動実行が原因

結論から言うと、負荷の高いコマンドを手動実行したことがフェイルオーバーの原因でした。

例えば[KEYS](https://redis.io/docs/latest/commands/keys/)コマンドは、ドキュメントに書いてある通り、大きなデータベースに対して実行するとパフォーマンスに致命的な影響を及ぼすため、本番環境での実行には注意が必要です。

> Warning: consider KEYS as a command that should only be used in production environments with extreme care. It may ruin performance when it is executed against large databases.

このような負荷の高いコマンドの手動実行により高負荷がかかり、マスターノードが一時的に応答不能になりました。
こうしてマスターノードの応答が途絶えたのでフェイルオーバーが発動した、というのが今回の障害の経緯であったことを突き止めることができました。

#### 補足: 対策

[Redisのコマンド](https://redis.io/docs/latest/commands/)は、使い方によってはこのような障害を招く可能性があるので、注意が必要です。

対策としては、事前にドキュメントを確認するとよさそうです。

例えば、負荷の高いコマンドの例として挙げた[KEYS](https://redis.io/docs/latest/commands/keys/)コマンドについては、
代替策として[SCAN](https://redis.io/docs/latest/commands/scan/)や[sets](https://redis.io/docs/latest/develop/data-types/sets/)も検討するように記載されています。

> If you're looking for a way to find keys in a subset of your keyspace, consider using [SCAN](https://redis.io/docs/latest/commands/scan/) or [sets](https://redis.io/docs/latest/develop/data-types/sets/).

### 調査方法

どのように原因を突き止めたのかを簡潔に紹介します。

また、調査に役立った情報も紹介します。

#### 調査に役立つ情報

##### コマンドラインインタフェース: redis-cli

[redis-cliコマンド](https://redis.io/docs/latest/develop/connect/cli/)を使うことで、[Redisの各コマンド](https://redis.io/docs/latest/commands/)を実行できます。

次のコマンドがトラブルシューティングに役立ちました。

* [redis-cli info](https://redis.io/docs/latest/commands/info/)
  * Redisのバージョン情報からメモリーやCPU消費に関する情報まで、重要な情報を多く得られます。
* [redis-cli cluster info](https://redis.io/docs/latest/commands/cluster-info/)
  * 本事例のようにRedis Clusterの機能を使っている場合、クラスターに関する主要な情報を得られます。
* [redis-cli config get \*](https://redis.io/docs/latest/commands/config-get/)
  * 設定値の一覧を確認できます。
* [redis-cli memory doctor](https://redis.io/docs/latest/commands/memory-doctor/)
  * メモリーに関する問題について診断結果を得られます。

##### Redisの出力ログ

いつどのノードの応答が途絶えたのか、などが分かります。

特定のノードの応答が途絶えたログ例:

* `FAIL message received from {ノードの応答断絶を発見したノードのID} about {応答が途絶えたノードのID}`
* `Marking node {応答が途絶えたノードのID} as failing (quorum reached).`

Redis Clusterの機能を使っている場合は各ノードがIDで表現されるので、[redis-cli cluster nodes](https://redis.io/docs/latest/commands/cluster-nodes/)コマンドを実行してIDを確認してください。
もしコマンドを実行できない状況でも、頑張って各ログの因果関係を追えば、ノードとIDの対応をパズルのように特定できます。

##### Docker Hubの公式イメージ

[公式のDockerイメージ](https://hub.docker.com/_/redis)を使って、すぐに簡単な動作確認を行うことができます。

RedisのDockerコンテナを起動:

```bash
# 必要に応じてタグを指定してください（例: v6を使うなら「redis:6」）
docker container run --rm -it --name redis-test redis
```

起動したコンテナに入って操作を行う:

```bash
# 別のシェルで
docker container exec -it redis-test bash
```

#### 大きいメモリーピークが瞬間的に発生したことを確認

本事例では、次の各値から大きいメモリーピークが瞬間的に発生したことが分かりました。

* [redis-cli info](https://redis.io/docs/latest/commands/info/)
  * `used_memory_peak` (`used_memory_peak_human`)
    * ピーク消費メモリーです。
    * 本事例では、大きなメモリーピークの発生を確認できました。
* [redis-cli config get \*](https://redis.io/docs/latest/commands/config-get/)
  * `maxmemory`
    * 消費するメモリー上限の設定値です。
    * 消費メモリーがこの値を超えると、[Key eviction](https://redis.io/docs/latest/develop/reference/eviction/)の仕組みによって、消費メモリーがこの値を下回るまでデータを削除します。
    * 本事例では、この値を大きく超える`used_memory_peak`が記録されていたことから、Key evictionが間に合わないくらい瞬間的に大きなメモリー負荷が生じたことが分かりました。

#### redis-cliプロセスがメモリーピークの原因であったことを確認

ログの調査からノードの応答が途絶えた時刻が分かっていたため、その時刻付近のプロセスの情報を調べました。
ちょうど応答断絶の直前に`redis-cli`プロセスが誕生し、その直後にRedisの消費メモリーが急上昇していたことが分かりました。

このことから、[redis-cliコマンド](https://redis.io/docs/latest/develop/connect/cli/)で負荷の高いコマンドを実行したことで、急激なメモリー負荷がノードにかかり、ノードが一時的に応答不能になった、という可能性が濃厚となりました。

そして、実際に負荷のかかるコマンドが実行されていた、ということが分かったため、これが原因であると断定することができました。

### 補足: Redisのライセンス変更とValkey

Redisはバージョン7.4からソースアベイラブルライセンスに変更し、フリーソフトウェア（オープンソース）ではなくなりました。
（今回の事例のRedisは、7.4よりも前のバージョンでした）。

[クリアコードはフリーソフトウェアの発展に寄与することを理念としています]({% link about/index.md %})。
Redisに代わるフリーソフトウェアのインメモリデータベースとしては、[Valkey](https://valkey.io/)があります。
このValkeyのバージョン8.0においては、マルチコアのノードの性能がRedis v7の2倍以上になる、という話もあります[^valkey]。
これらを踏まえ、今後はRedisからValkeyへの移行などもサポートしていけたらと思っています。

[^valkey]: Valkeyのバージョン8.0の性能が従来の2倍以上に: https://www.publickey1.jp/blog/24/redisdbvalkey2.html

### まとめ

本記事では、有名なインメモリデータベースである[Redis](https://redis.io/)の障害調査を行った事例について紹介しました。

この調査は[クリアコードのFluentd法人様向けサポートサービス]({% link services/fluentd-service.md %})の一貫で実施したもので、
Fluentd単体に限らず、このようにFluentdとその周辺のフリーソフトウェア（K8s, nginx, Embulkなど）をセットでサポートする事例やご相談もあります。

もしFluentdとその周辺のアプリケーションの運用でお困りのことがありましたら、
ぜひ[お問い合わせフォーム]({% link contact/index.md %})からお気軽にご相談ください。
