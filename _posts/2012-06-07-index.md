---
tags: []
title: logalimacs 1.0.0 をリリースしました
---
2012.6.7に [logaling-command](http://logaling.github.com/) のEmacs用のフロントエンドプログラム
[logalimacs](https://github.com/logaling/logalimacs) 1.0.0 をリリースしました。
Emacsのキーボードショートカットでlogaling-commandを利用できます。
<!--more-->


### logaling-commandとは

logaling-commandは翻訳作業に欠かせない訳語の確認や選定をサポートする CUI ツールです。
対訳用語集を簡単に作成、編集、検索することができます。

### 使い方

登録したキーボードショートカットを押すことでカーソル位置にある単語を
logaling-commandで検索（lookup）しツールチップやバッファに表示します。

### logalimacs 0.9.0 から 1.0.0 への変更点

#### 多階層ポップアップを利用するようにしました

以前はpopup.elの機能でツールチップで表示する機能を利用していましたが、
今回のバージョンアップでpopup.elのポップアップを多階層表示する機能を
利用するようにしました。
候補に注釈があった場合、C-fを押すことで注釈を表示できます。

下の画像の2段目の”猫科の動物”が注釈の部分です。

![ポップアップの画像]({{ "/images/blog/20120607_0.png" | relative_url }} "ポップアップの画像")

#### ポップアップ時の幅を選択できるようにしました

ポップアップ時のツールチップの幅を現在のバッファの幅の最大か
自動かを選択できるようにしました。
デフォルトでは自動になっています

#### logaling-commandの--dictionaryオプションに対応しました

logaling-commandの--dictionaryオプションに対応しました。
これはloga lookup時のオプションで
edict や gene95 といった辞書をインポートして使用している場合など、
訳語からも検索できたり、設定している言語コードとは異なる対訳用語集からも検索をしたい場合に設定します。
--dictionary オプションを使用すると、原文だけではなく、訳文からも検索を行い、完全マッチしたものから順に結果出力します。
検索の優先度は、"原文の完全一致 > 原文の部分一致 > 訳文の完全一致 > 訳文の部分一致"となっています。
利用方法としては.emacsなどの設定ファイルにloga-use-dictionary-option変数をtに設定するとlookup時の検索結果が--dictionaryオプションを利用した物になります。
（インストール方法の項で具体的な設定方法を書いています。）

#### viスタイルの移動方法の"j"、"k"で候補を上下に移動できるようになりました

バッファ表示と、ポップアップ表示する時にviスタイルの移動方法の"j"、"k"で候補を上下に移動できるようになりました。
"p"、"n"で移動する方法も残っています。

### インストール方法

詳しいlogaling-commandの設定やインストールの説明はlogalimacsの 
[チュートリアルページ](http://logaling.github.com/logalimacs/tutorial.html) で紹介しています。

logaling-commandは既にインストール済みでEmacsにlogalimacsを入れる方用に簡単に導入方法を説明します。
※既にlogalimacsを利用している場合はgit pullでlogalimacsを最新版にして
以下の設定の"お好みで設定してください"の部分をお好みで追加してください

  * GitHubからclone
  {% raw %}
```
$ cd ~/.emacs.d/
$ git clone https://github.com/logaling/logalimacs.git
```
{% endraw %}
  * logalimacsに関する設定を~/.emacs.d/init.elに以下を追加します。
  {% raw %}
```
;;; logalimacsディレクトリへのロードパスを通す
;; ~/.emacs.d/package/logalimacs/logalimacs.elが存在する場合
(add-to-list 'load-path "~/.emacs.d/package/logalimacs")

;;; keybinds
(global-set-key (kbd "C-:") 'loga-lookup-in-popup)

;;; お好みで設定してください
;; lookupに--dictionaryオプションを利用する場合
(setq loga-use-dictionary-option t)
;; :autoがデフォルトでカーソル位置の近くにポップアップ
;; :maxにするとbuffer一杯までpopupをのばし行の最初から表示します
(setq loga-popup-output-type :max)
```
{% endraw %}
