---
tags:
- cutter
- test
title: Cutter 1.0.5リリース
---
ここには書いていませんでしたが、[Cutter 1.0.3のリリース]({% post_url 2008-07-16-index %})のリリースの約1ヶ月後に1.0.4がリリースされました。
さらにその約2ヶ月後の昨日、1.0.5がリリースされました。
[Cutter](http://cutter.sourceforge.net/index.html.ja)とはC言語用の単体テス
トフレームワークです。
<!--more-->


### 新機能

詳細は
[NEWS](http://cutter.sourceforge.net/reference/ja/news.html)に
書いてありますが、1.0.5での目玉新機能は以下の3点です。

  * 画像データ入出力・操作ライブラリ
    [gdk-pixbuf](http://www.gnome.gr.jp/docs/gtk+-2.8.x-refs/gdk-pixbuf/)
    （←少し古い。[最新版（英語）](http://library.gnome.org/devel/gdk-pixbuf/stable/)
    ）をサポート
  * 多くの検証とテスト便利関数を追加
  * ユーザ定義検証作成のサポートを強化

### gdk-pixbufサポート

gdk-pixbufをサポートすることにより、画像が等しいかどうかを検
証できるようになりました。また、もし、画像が異なる場合は画像
間の差分を示し、画像のどこが異なるのかをわかりやすくしていま
す。

例えば、このような画像を比較したとします。

[![期待画像]({{ "/images/blog/20081016_0.png" | relative_url }} "期待画像")]({{ "/images/blog/20081016_1.png" | relative_url }})
[![実際の画像]({{ "/images/blog/20081016_2.png" | relative_url }} "実際の画像")]({{ "/images/blog/20081016_3.png" | relative_url }})

これらの画像では赤い丸の部分が異なっています。

これらの画像の差分画像は以下のようになります。

[![差分画像]({{ "/images/blog/20081016_4.png" | relative_url }} "差分画像")]({{ "/images/blog/20081016_5.png" | relative_url }})

左上に期待画像、右上に実際の画像、左下に異なるピクセルを示し
た画像、右下に異なるピクセルを強調表示、同じピクセルを弱めに
表示した画像を配置しています。左下の画像を見ることでどの部分
が異なるのかが具体的にわかり、右下の画像を見ることで比較画像
はどのあたりが異なるのかを相対的に確認する事ができます。

もっとよい表現方法があるかもしれませんが、しばらくはこの方法
を採用しする予定です。もしかすると、今後、よりよい表現方法に
変更されるかもしれません。

ちなみに、この機能はPDF操作・レンダリングライブラリである
[Poppler](https://ja.wikipedia.org/wiki/Poppler)の[テスト](http://github.com/kou/poppler-test/)で利用されていま
す。

### 検証・テスト便利関数の追加

Cutterを使用していて、こんな検証・便利関数があったら便利だ、
と感じたものは積極的にCutter本体に取り込んでいます。

1.0.5では12個の検証、9個の便利関数が追加されました。追加され
た検証・便利関数のリストは
[NEWS](http://cutter.sourceforge.net/reference/ja/news.html)に
書かれています。

### ユーザ定義検証作成のサポートを強化

今までもユーザが独自で検証を定義することはできたのですが、バッ
クトレースを取得するためにマクロとして定義する必要がありまし
た。

1.0.5では
[cut_trace()](http://cutter.sourceforge.net/reference/ja/cutter-cut-helper.html#cut-trace)
というマクロが追加され、検証を関数として定義してもバックトレー
スを取得することができるようになりました。マクロは可変長引数
が簡単に書けるなど便利な事も多いのですが、構文エラーが見つけ
にくいなどという問題もあります。1.0.5からは関数とマクロを使い
分けられるようになり、より便利にデバッグのしやすいテストが書
けるようになりました。

### まとめ

Cutter 1.0.5ではテスト作成・デバッグ支援の機能が強化され、よ
り便利なテスト環境を提供するようになりました。今までよりもC
言語でのテスト作成が楽しくなるかもしれません。

[[チュートリアル](http://cutter.sourceforge.net/reference/ja/tutorial.html)]
[[リファレンスマニュアル](http://cutter.sourceforge.net/reference/ja/reference.html)]
[[ダウンロード](http://sourceforge.net/project/showfiles.php?group_id=208375&package_id=249756&release_id=633416)]
