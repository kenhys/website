---
title: GitHub Actionsでccacheを使ってCMake+Microsoft Visual C++のビルドを高速化
author: kou
---

Apache ArrowとかMroongaとかいくつかC++ベースのビルドに時間がかかるプロジェクトの開発をしている須藤です。ccacheを使うことでこれらのプロジェクトのCI時間を短くする方法を紹介します。

<!--more-->

### ビルドキャッシュ

高速化のよくある方法がキャッシュです。一度計算した結果を保存しておき使いまわすことでそもそも処理をせず速くするというアプローチです。

C++のビルドでもキャッシュを使うことができます。これを実現するためのプロダクトはいくつかありますが[ccache](https://ccache.dev/)が代表的なプロダクトです。（元祖？ビルドキャッシュ関連のプロダクトの歴史には詳しくないのでだれか知っている人がいたら教えて。）

他にもMozillaが開発している[sccache](https://github.com/mozilla/sccache)（リモートストレージにキャシュを保存して複数マシンでキャッシュを共有できる）や、Microsoft Visual C++用に開発された[clcache](https://github.com/frerich/clcache)などがあります。

Microsoft Visual C++で使うならclcacheしか選択肢がない時期もありましたが、今はccacheやsccacheなど他のプロダクトもMicrosoft Visual C++をサポートしていたり、clcacheはメンテナンスされていなかったりしてclcacheの出番はなくなっています。

さらっとccacheもMicrosoft Visual C++をサポートしていると書きましたが、実はサポートしたのはわりと最近です。2022-02-27にリリースされた[ccache 4.6](https://ccache.dev/releasenotes.html#_ccache_4_6)からサポートしています。

> Added support for caching calls to Microsoft Visual C++ (MSVC) and clang-cl (MSVC compatibility for Clang).
> [contributed by Cristian Adam, Luboš Luňák, Orgad Shaneh and Joel Rosdahl]

それでは、ccacheを使ってCMakeを使っているプロダクトのMicrosoft Visual C++でのビルド結果をキャッシュする方法を紹介します。

### CMakeとccache

ccacheは`ccache cc ...`というように普通のコンパイラーのコマンドラインの前に`ccache`をつけて使います。（`ccache`を`cc`にシンボリックリンクして使う使い方もあります。）

CMakeはccacheのようにコンパイラーの前につけるタイプのツールをサポートするために[`CMAKE_<LANG>_COMPILER_LAUNCHER`](https://cmake.org/cmake/help/latest/variable/CMAKE_LANG_COMPILER_LAUNCHER.html)というCMake変数を用意しています。`<LANG>`の部分には`C`や`CXX`が入ります。C++のコンパイラーのコマンドラインの前に`ccache`をつける場合は`-DCMAKE_CXX_COMPILER_LAUNCHER=ccache`というように使います。

ということで、次のようにすればCMakeを使っているプロダクトのMicrosoft Visual C++でのビルド結果をccacheでキャッシュして高速化できそうですよね。（PowerShellのコマンドライン。）

```powershell
cmake `
  -G "Visual Studio 17 2022" `
  -A x64 `
  -DCMAKE_CXX_COMPILER_LAUNCHER=ccache `
  ...
```

しかし！これは動きません。なぜなら`CMAKE_<LANG>_COMPILER_LAUNCHER`はVisual Studio系のジェネレーター（CMakeがどのビルドシステム用のファイルを生成するかを指定するやつ、みたいな感じ）では使えないからです。[`<LANG>_COMPILER_LAUNCHER`プロパティー](https://cmake.org/cmake/help/latest/prop_tgt/LANG_COMPILER_LAUNCHER.html)の説明にあるとおり、MakefileかNinjaでしか使えません。

ということは、こう書くのか、と思いますよね。

```powershell
cmake `
  -G Ninja `
  -DCMAKE_CXX_COMPILER_LAUNCHER=ccache `
  ...
```

しかし！これは動きません。なぜならVisual C++のコンパイラー（`cl.exe`）がどこにあるかわからないからです。

ではどうすればよいかというと、Visual C++のコンパイラーの場所を教えてあげればよいです。そのための便利バッチファイルをVisual Studioが提供しています。GitHub Actionsの`windows-2019` hosted runnerでは`C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat`にあります。ということで、これを使ってこんな感じに書きます。バッチファイルを使うのでPowerShellではなく`cmd.exe`を使わないといけないことに注意してください。

```batch
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat" x64
cmake ^
  -G Ninja ^
  -DCMAKE_BUILD_CXX_COMPILER_LAUNCHER=ccache ^
  ...
```

### デバッグビルドとビルドキャッシュ

これでキャッシュが効くようになるのですが、この話はこれでは終わりません。実際は`-DCMAKE_BUILD_TYPE`に`Release`（リリース用ビルド）やら`Debug`（デバッグ用ビルド）やら`RelWithDebInfo`（デバッグ情報付きリリース用ビルド）やらを指定してビルドします。そして、`Debug`/`RelWithDebInfo`を指定するとキャッシュが効きません。なんと。

CMakeは`-DCMAKE_BUILD_TYPE`に`Debug`/`RelWithDebInfo`を指定したときはデフォルトで`/Zi`オプション[^zi]を使います。`/Zi`はデバッグ情報を含んだPDBファイルをビルド対象のオブジェクトファイルとは別に生成するためのオプションです。そして、`/Zi`が指定されているとキャッシュが効きません。

[^zi]: https://learn.microsoft.com/ja-jp/cpp/build/reference/z7-zi-zi-debug-information-format

詳細は以下の各プロダクトのissueやREADMEを読んでくださいなのですが、簡単にキャッシュできない理由を説明しておきます。`/Zi`を使うと複数のファイルで共有のPDBファイルを使います。複数のファイルが同じファイルを生成・変更すると各ファイル単位でビルド結果が確定しないので各ファイル単位でのビルド結果をキャッシュできないのです。`/Fd`オプションを使うことで各ファイルごとに別のPDBファイルを使うようにすることもでき、そうするとsccacheでは`/Zi`付きでもキャッシュできるようになります。が、ccacheはまだそこまで頑張っていません。

* https://github.com/ccache/ccache/issues/1040
* https://github.com/mozilla/sccache#usage の"To egnerate PDB files ..."あたり
* https://github.com/frerich/clcache/issues/30

では、どうするかというと`/Zi`の代わりに`/Z7`を使います。`/Z7`を使うと別途PDBファイルを作るのではなくオブジェクトファイルそのものにデバッグ情報を含めるようになります。そうすると、各ファイルのビルドでは他のファイルと成果物を共有しなくなるのでキャッシュできます。

`CMakeLists.txt`を変更できるなら、sccacheのREADMEにもある通り、次のようにして`/Zi`を`/Z7`に書き換えることになります。

```cmake
if(CMAKE_BUILD_TYPE STREQUAL "Debug")
  string(REPLACE "/Zi" "/Z7" CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")
  string(REPLACE "/Zi" "/Z7" CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}")
elseif(CMAKE_BUILD_TYPE STREQUAL "Release")
  string(REPLACE "/Zi" "/Z7" CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE}")
  string(REPLACE "/Zi" "/Z7" CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE}")
elseif(CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
  string(REPLACE "/Zi" "/Z7" CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
  string(REPLACE "/Zi" "/Z7" CMAKE_C_FLAGS_RELWITHDEBINFO "${CMAKE_C_FLAGS_RELWITHDEBINFO}")
endif()
```

変更できないなら`-DCMAKE_C_FLAGS_DEBUG=...`とか`-DCMAKE_CXX_FLAGS_DEBUG=...`をいい感じに指定することになります。デフォルト値はCMakeの[`Modules/Platform/Windows-MSVC.cmake`](https://gitlab.kitware.com/cmake/cmake/-/blob/v3.23.4/Modules/Platform/Windows-MSVC.cmake#L307-313)を見てください。

### GitHub Actionsとビルドキャッシュ

ということでキャッシュできるようになったので、後はキャッシュ結果を各ビルド間で共有すればよいです。GitHub Actionsでキャッシュ結果を共有するには[actions/cache](https://github.com/actions/cache)を使います。

問題はなにをactions/cacheでキャッシュすればよいかというところです。キャッシュするのはccacheがキャッシュした内容です。それがどこにあるかは`ccache --show-config`や`ccache --show-stats --verbose`でわかります。たとえば、`choco install ccache`でccacheをインストールした場合は`~\AppData\Roaming\ccache\`以下にキャッシュした内容が置かれます。

ということで、次のような設定を書けばよいということになります。

{% raw %}
```yaml
- uses: actions/cache@v3
  with:
    path: |
      ~\AppData\Roaming\ccache
    key: ccache-${{ hashFiles('**/*.cpp', '**/*.hpp') }}
    restore-keys: ccache-
```
{% endraw %}

具体例は[Mroongaの設定](https://github.com/mroonga/mroonga/blob/master/.github/workflows/windows.yml)でも見てみてください。

### まとめ

GitHub Actionsでccacheを使ってCMake+Microsoft Visual C++のビルドを高速化する方法をまとめました。

新しめのccacheを使わないといけない、`CMAKE_CXX_COMPILER_LAUNCHER`を使うならVisual Studio系のジェネレーターは使えない、`-DCMAKE_BUILD_TYPE=Debug`/`-DCMAKE_BUILD_TYPE=RelWithDebInfo`ではキャッシュが効かないとかいろいろハマりポイントがありますが、動くようになるとかなり高速になるのでCIが遅いと思っている人は使ってみてください。
