---
tags:
- cutter
- test
title: Cutter 1.0.6リリース
---
先日、C言語用単体テストフレームワークである[Cutter](http://cutter.sourceforge.net/index.html.ja)の新バージョン1.0.6がリリースされました。（[アナウンス](http://sourceforge.net/mailarchive/forum.php?thread_name=20090302.000929.200754750133062642.kou%40cozmixng.org&forum_name=cutter-users-ja)）
<!--more-->


### ハイライト

[NEWS](http://cutter.sourceforge.net/reference/ja/news.html)にも書いてあるように、今回のリリースでも多くの新機能がありますが、ここではその中でも特におすすめの[構造体定義なしで複雑なテストデータを使えるAPI](http://cutter.sourceforge.net/reference/ja/cutter-gcut-data.html)を紹介します。

この新機能によりもっと簡単にデータ駆動テストが書けるようになります。実際、[milter manager](http://milter-manager.sourceforge.net/index.html.ja)のテストが簡単に書けるようになりました。

### データ駆動テスト

データ駆動テストとは同じテストケースに対して複数のテストパターンを適用するテスト手法です。これにより多くのテストパターンを簡単に書くことができるという利点があります。

例えば、入力された文字列の小文字をすべて大文字に変換するto_upper()関数のテストをするとします。テストデータは以下の3つを考えます[^0]。

  * すべて小文字の文字列
  * 小文字と大文字が混ざった文字列
  * すべて大文字の文字列

これはこのように書けます。

{% raw %}
```c
cut_assert_equal_string("HELLO", to_upper("hello"));
cut_assert_equal_string("HELLO", to_upper("HelLo"));
cut_assert_equal_string("HELLO", to_upper("HELLO"));
```
{% endraw %}

データ駆動テストでは、テストケースはこのようになります。

{% raw %}
```c
cut_assert_equal_string(expected, to_upper(input));
```
{% endraw %}

このうち、expectedとinputを外部から与えることになります。Cutterではこのように書きます。

{% raw %}
```c
void
data_to_upper (void)
{
#define ADD_DATUM(label, expected, input)             \
  gcut_add_datum(label,                               \
                 "expected", G_TYPE_STRING, expected, \
                 "input", G_TYPE_STRING, input,       \
                 NULL)

    ADD_DATUM("all lower", "HELLO", "hello");
    ADD_DATUM("mixed", "HELLO", "HelLo");
    ADD_DATUM("all upper", "HELLO", "HELLO");

#undef ADD_DATUM
}

void
test_to_upper (gconstpointer data)
{
    const gchar *expected, *input;

    expected = gcut_data_get_string(data, "expected");
    input = gcut_data_get_string(data, "input");
    cut_assert_equal_string(expected, to_upper(input));
}
```
{% endraw %}

QtのQtTestLibではこのようになります。
[Chapter 2: Data Driven Testing](http://doc.trolltech.com/solutions/4/qttestlib/tutorial2.html)より:

{% raw %}
```cpp
void TestQString::toUpper_data(QtTestTable &t)
{
    t.defineElement("QString", "string");
    t.defineElement("QString", "result");

    *t.newData("all lower") << "hello" << "HELLO";
    *t.newData("mixed")     << "Hello" << "HELLO";
    *t.newData("all upper") << "HELLO" << "HELLO";
}

void TestQString::toUpper()
{
    FETCH(QString, string);
    FETCH(QString, result);

    COMPARE(string.toUpper(), result);
}
```
{% endraw %}

ほとんど同じくらいの手間で書けているのではないでしょうか。

テストプログラムは重複があってもいいからわかりやすい方がよい、とよく言われます。しかし、そのために、重複がたくさんある中から、興味がある重複していない部分が見つけづらくなるのは問題かもしれません。データ駆動テストでは、どのようにテストしたいのかがわかりやすいテストケースになる傾向がある気がしています。

### データ駆動テストのサポート状況

多くのテスティングフレームワークはデータ駆動テストをサポートしています。PerlのTest::Base、RubyのRSpec[^1]、C++のQtのQtTestLibや[Google Test](http://code.google.com/p/googletest/wiki/GoogleTestAdvancedGuide#Value_Parameterized_Tests)でもサポートされています。他にもまだまだたくさんあります。

テストデータの入力方法も様々で、テストプログラム中でテストデータを生成するものから、データベースからテストデータを取り出すもの、CSV、Excelなどから取り出すものもあります。

### まとめ

動的に複雑なことができるスクリプト言語、アノテーションなどでメタデータを指定できる最近の言語、トリッキーなC++などでは便利にデータ駆動テストを実行できるテスティングフレームワークは多くあります。しかし、C言語用のフレームワークではそんなになかったのではないかと思います。[^2]

Cutterに興味がある方は使ってみたり、[メーリングリスト](https://lists.sourceforge.net/lists/listinfo/cutter-users-ja)などで提案、質問などしてみてください。

[^0]: 他にも空文字列やアルファベット以外が入った文字列などが考えられます。

[^1]: 動的にspecを定義してね、という方針のようなのでサポートしているというか、Rubyを使っているからできちゃうという感じ

[^2]: 少なくともまだ見つけられていない。C言語用のテスティングフレームワークのAPIや機能に関してまとまっている場所はあるのだろうか。
