---
title: "Fluentdの最新動向について発表しました: 【GOJAS Meetup-24】SplunkケーススタディVol.8 #gojas_jp"
author: daipom
tags:
  - fluentd
---

こんにちは。データ収集ツール[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

2024年9月19日に開催された[【GOJAS Meetup-24】SplunkケーススタディVol.8](https://gojas.doorkeeper.jp/events/176726)において、
「データ収集ツール Fluentdの最新動向 With Splunk」というタイトルで発表（LT）しました。

Fluentdから見て、統合ログ管理プラットフォームであるSplunkは親和性が高く、Splunkと連携するためのプラグインもいくつかあります。
しかしその一方で、最近はSplunk関連のプラグイン開発が停滞しがちであるという課題があります。

今回は本イベントに参加して、主にSplunkのデータの前処理事情について勉強しつつ、
懇親会のLTでFluentdの最新動向やSplunk関連の開発におけるFluentdの課題について発表をしてきました。

本記事では、私の発表内容とSplunkのデータの前処理事情について勉強したことについて紹介します。

<!--more-->

### 発表スライド

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/daipom/recent-fluentd-with-splunk/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; box-sizing: content-box; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/daipom/recent-fluentd-with-splunk/" title="
データ収集ツール Fluentdの最新動向 With Splunk">データ収集ツール Fluentdの最新動向 With Splunk</a>
  </div>
</div>

### 発表内容

自己紹介の資料を用意した時に気がついたのですが、[過去2年間のFluentdへのコントリビュート](https://github.com/fluent/fluentd/graphs/contributors?from=9%2F24%2F2022&to=9%2F24%2F2024)で私が1位になっていました。
嬉しさもありますが、もっと多くの人にFluentdの開発に参加してもらいたいな、とも思います。

さて、データ収集ツールである[Fluentd](http://www.fluentd.org)は次のような特徴を持ちます。

* 多様なプラグインによって様々なデータソースからデータを収集できる
* フィルター機能などの前処理の機能や、バッファリング機能などを活用できる
* プラグインを簡単に作れる
* スケール可能

そのため、Fluentdから見て統合ログ管理プラットフォームであるSplunkは親和性が高いです。
実際にSplunkと連携するためのプラグインもいくつかあります。

* [fluent-plugin-splunk](https://github.com/fluent/fluent-plugin-splunk)
* [fluent-plugin-splunk-hec](https://github.com/splunk/fluent-plugin-splunk-hec)

しかし、最近はSplunk関連の開発が停滞しているという課題があります。
`fluent-plugin-splunk`は最後のコミットが3年前ですし、`fluent-plugin-splunk-hec`はSplunk公式が2024年1月1日をもってEnd of Supportとしました。

その上、私含め、現在アクティブなFluentdのメンテナーがSplunkに詳しくなく、Splunk側の事情が分からないことも問題です。

そのため、Fluentdの活用に興味があったり、実際にFluentdを使ってみて課題がある方がいらっしゃれば、
ぜひコミュニティーや弊社クリアコードに気軽に相談をしてほしい、とお話ししました。
課題やご要望をシェアしていただいて、改善していけたらいいなと思います。

* [Fluentdコミュニティーの日本語用Discussions](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)
* [弊社のお問い合わせフォーム]({% link contact/index.md %})

最後にFluentd側の最新動向を紹介しました。
要点は次の通りです。

* コミュニティー公式パッケージの最新版 [Fluent Package (fluent-package)](https://www.fluentd.org/download/fluent_package) の紹介
  * td-agentの後継パッケージです
  * td-agentと互換性を持っており、簡単にバージョンアップできます
  * 長期サポート版(LTS)が、長期の安定運用におすすめです
* 最近も`in_tail`の動作不良の改善など大きな修正やセキュリティー修正が入っていますので、ぜひ最新版をご利用ください

また、Fluentdのバージョンアップについては以前の記事でも解説していますので、興味のある方はぜひご覧ください。

* [Fluentdを安全にバージョンアップしよう（td-agent -> Fluent Package）]({% post_url 2024-09-17-how-to-update-fluentd-safely %})

### 講演会や懇親会で学んだこと: Splunkのデータの前処理事情

主に次の点を学びました。

* Splunkは自前の機能で各データソースの収集やフィルタリング、加工などを一通りできてしまう
* 一方で自前のスクリプトによってデータを収集するケースもある
* コスト削減のため、前処理でデータをフィルタリングしたり、検索の必要のないデータはS3の方に振り分けたりすることは大事

Splunkにおいても前処理はとても重要そうですが、Splunk自前の機能で色々と便利にできそうだったので、
Fluentdなどの別のツールが必要になるケースは多くないのかもしれないと感じました。

一方で、自前のスクリプトで色々なデータを収集されているケースもあったので、
そのようなケースにおいてはまさにFluentdが大活躍しそうです。
[公式ウェブページのWhat is Fluentd?](https://www.fluentd.org/architecture)のイメージ図の通り、
自前で多くのスクリプトを管理することはとても大変なはずで、実際に大変だというお話も聞けました。
Fluentdを使えば一元化して管理できるだけでなく、共通のフィルタリングやバッファリング機能を利用することも可能になります。

一方でFluentdは使ったことはあるがうまく使いこなせなかった、というお話もありました。
実際にFluentdはできることが多いがために設定やチューニングが難しい部分もあり、
より簡単に使えるようにできたらもっと活用してもらえるのかもしれないと感じました。

### まとめ

2024年9月19日に開催された[【GOJAS Meetup-24】SplunkケーススタディVol.8](https://gojas.doorkeeper.jp/events/176726)において、Fluentdの最新動向について発表（LT）しました。

Fluentdの活用に興味があったり、実際にFluentdを使ってみて課題がある方がいらっしゃれば、
ぜひコミュニティーや弊社クリアコードにお気軽にご相談ください！

* [Fluentdコミュニティーの日本語用Discussions](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)
* [弊社のお問い合わせフォーム]({% link contact/index.md %})

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
[Fluent Package (fluent-package)](https://www.fluentd.org/download/fluent_package) へのアップデートについてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。
