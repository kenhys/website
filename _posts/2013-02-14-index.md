---
tags:
- company
title: クリアコードのサポートサービス
---
クリアコードでは、Mozilla製品、milter manager、groonga、GStreamer、WebKitといったフリーソフトウェアのサポートサービスを提供しています。サポートの対象となるフリーソフトウェアについて、クリアコードで対応できるものは、できる限りサポートサービスの枠組みの中で対応しています。今回はクリアコードのサポート契約の特徴や障害調査における作業の進め方を紹介します。
<!--more-->


### サポート契約について

まず、クリアコードが提供するサポート契約の特徴を説明します。

#### 価格設定について

クリアコードのサポート契約は、1つの問い合わせへの対応がいくらという契約ではありません。問い合わせに対する作業時間1時間あたりいくらという契約になっています。そのため、問い合わせへ回答するために要した時間が少ないほどお客様にとっては価値があがります。つまり、自分たちのためだけでなく、お客様のためにも効率的に調査、回答することが求められています。

以下は該当の契約条項です。

<blockquote>

クリアコードがお客様に提供する本サービスは、本契約末尾に定めるインシデント数の範囲に限られるものとします。

･･･

消費するインシデント数は1時間あたり1インシデントとして、対応に要した時間から算出し、当事者間で確認するものとします。

</blockquote>

#### 作業時間について

クリアコードが独断で作業時間を使いすぎてしまわないように、契約書上で作業時間の上限を定めています。ほとんどの場合、クリアコードの判断で使用できる作業時間は4時間としています。4時間を超える場合はお客様が実施するかどうか判断することになります。調査回答にどれだけの時間をかけるのかはお客様が決定するので、クリアコードはそのための判断材料を適切に提供します。

以下は該当の契約条項です。

<blockquote>

1件あたりの対応時間が4時間を超えることが見込まれる場合、クリアコードはお客様に対応時間の見積を連絡します。お客様は内容を確認し実施するかどうかをクリアコードに通知するものとします。

</blockquote>

#### 回答までの期間について

問い合わせへの回答は早いほうがよいため、速く回答することを心がけています。契約上では「受付日の翌営業日から起算して3営業日以内に回答する」としていることが多いです。月曜日に受け付けたら、遅くとも木曜日の17:00までに回答します。ここでいう回答とは原因の究明が完了し、その内容を説明するものとは限りません。お客様と合意した作業時間に到達したが原因が究明できていない場合は、その状況を報告することも回答となります。

以下、該当の契約条項です。

<blockquote>

クリアコードはお客様から問合せを受け付けた場合、受付日の翌営業日中に回答期限を連絡するものとします。回答期限は受付日の翌営業日を起算日として3営業日以内とします。ただし、複数の問合せを一度に受け付けた場合や、回答までに4時間を超えて作業が必要な場合はこの限りではありません。

</blockquote>

### 障害調査の作業指針

障害調査の進め方は以下のククログ記事が具体的な事例です。

  * [デバッグ力:よく知らないプログラムの直し方]({% post_url 2011-12-06-index %})
  * [札幌Ruby会議2012: 「バグの直し方」、「クリアなコードの作り方」 #sprk2012]({% post_url 2012-09-19-index %})

ここでは特に大切にしている作業指針を紹介します。

#### ゴールを設定し意識する

調査するときは、まず、知りたいこと（得たい結果）をゴールに設定します。次に、ゴールに対して必要な手段を考えるようにします。手段を決めたらゴールを忘れないように作業を進めます。はじめに手段を決めて、そこから得られる結果を考えずに作業を進めてしまうことがないように注意しています。

#### 問題を絞り込む

問題がありそうなところを計画性なくしらみつぶしにあたっていく方法は、効率的とは言えません。それよりも、二分探索のように問題範囲を絞り込んでいく方が効率的です。

問題があるところを絞り込むとき、「問題があるところを確認する」ことだけにとらわれてはいけません。「問題がないことを確認する」ことでも問題があるところを絞り込めます。例えば、1から10までの処理があり、1から5の処理に問題がないことわかれば、6から10が問題があると絞り込めています。

「問題があるところを確認する」ことだけにとらわれることを防ぐために、作業を進めるごとにチケットにやったことと結果を記載し、そのつど状況を整理する方法が有効です。

### 回答の書き方

回答はお客様が判断するための重要な材料です。問題箇所が特定されれば問題を回避する策を検討します。問題が特定できていなければ、調査方針が正しいかどうか、継続して調査するかどうか判断します。そのため、回答には以下の事項をもれなく記載します。

  * 調査の目的（ゴール）
  * 調査結果（調査からわかったこと、まだわかっていないこと。）
  * 調査に要した時間
  * 対策案もしくは今後の作業方針（ひとつ以上、できる限り複数）。案には以下を含める。
    * やることと結果（効果）
    * メリット/デメリット
    * 作業工数（明確に時間を算出できない場合は他の案に比べて多い少ないでよい。）
    * 作業期間（工数によって完了時期が異なる。）
  * おすすめの案とその理由。

### 対応のフロー

上記を踏まえた問い合わせ受付から回答、調査の継続までのフローは以下の通りです。

#### 問い合わせの受付

問い合わせはメールで受け付けます。問い合わせのメールが届いたら、社内のRedmineにチケットを作成します。チケットの期日は受付日の翌営業日から起算して3営業日後となります。

#### 問い合わせ内容の確認

メールの中身を確認し、ゴールをチケットに書きます。明らかに調査にかかる時間が4時間を超えると判断できる場合は、調査を実施するかどうかの判断をお客様に確認します。

例えば、問題を再現させる環境を用意するのに6時間かかり、その上で調査に更に4時間かかることが見込まれるとします。この場合は、「合計10時間は必要だが実施してよいか」ということを確認します。

#### 調査

ゴールを確認し、意識して調査をすすめます。調査の過程は、随時チケットに記録します。作業途中であっても、回答作成を含めると4時間に到達することが明らかとなった場合は、調査を中断し回答作成に着手します。

#### 回答

回答の書き方に沿って回答文を作成します。調査を継続するかどうか、どの案を実施するかどうかなどお客様の判断が必要になる場合は、それを明記します。回答はチケットにも記載します。

#### 調査の継続

お客様と約束した4時間を超えて調査するかどうか判断を仰ぎ、お客様から調査実施の指示があった場合は、チケットの期日を更新し、作業を進めます。この時もお客様と合意した作業時間（回答に記載した工数）を遵守します。

### 最後に

クリアコードでは、ゴールを意識して作業に取り組むよう心掛けています。ゴールを意識せず、単に「やりやすいから」、「手段を知っているから」という理由で作業に取り組むと、効率的に作業を進められません。クリアコードのサポートサービスでは、非効率な作業はお客様の損失に直接つながります。これは、作業時間が増えればそれだけお客様との契約時間を消費してしまうからです。

例えば、障害調査で原因の目星がついていないケースでは、「ゴールを意識して作業に取り組むこと」を徹底しないと路頭に迷います。

フリーソフトウェアであればソースコードを確認できるため、このようなやり方を実践することができます。そして、このやり方を徹底するとよく知らないソフトウェアであってもサポートできます。よく知らないソフトウェアで問題が発生し、その調査をすることになった場合は試してみてください。

クリアコードのサポートサービスをご検討される際は、[お問い合わせフォーム](/contact/)からご連絡ください。
