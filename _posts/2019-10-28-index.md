---
tags:
  - apache-arrow
title: Apache Arrow Flightの日本語情報を公開
---
[Apache Arrowの最新情報]({% post_url 2019-09-30-index %})をまとめた須藤です。
<!--more-->


先日、Apache Arrowの公式ブログで[Introducing Apache Arrow Flight: A Framework for Fast Data Transport](https://arrow.apache.org/blog/2019/10/13/introducing-arrow-flight/)というApache Arrow Flightを紹介する英語の記事が公開されました。この記事はApache Arrow Flightのことを理解する上で非常に役に立つ情報だと思ったので、翻訳し、Apache Arrowの公式ブログで[Apache Arrow Flightの紹介：高速データトランスポートフレームワーク](https://arrow.apache.org/blog/2019/10/13/introducing-arrow-flight-japanese/)という記事として公開しました。

翻訳にあたり、[Speee](https://speee.jp/)の@mrknが[レビューアーとして協力](https://github.com/apache/arrow-site/pull/36)してくれました。ありがとうございます。

さて、そんなApache Arrowですが、そろそろ1.0.0がリリースされます。それにあわせて[Apache Arrow東京ミートアップ2019](https://speee.connpass.com/event/152887/)というイベントを2019年12月11日（水）の夜に開催します。[去年もApache Arrowイベントを開催]({% post_url 2018-12-10-index %})しましたが、今年も開催します。去年と同様、今年もSpeeeさんに会場・飲食物を提供してもらいます。ありがとうございます！

去年はApache Arrow関連プロダクトの開発に参加する人を増やすことを目的に設計しましたが、今年はApache Arrowユーザーを増やすことを目的に設計しています。そのため、次のような構成にしています。

  * Apache Arrowそのものの情報を紹介

  * Apache Arrowの利用事例を紹介

この構成を実現するためにApache Arrowの利用事例を紹介してくれる人をすごく探しています。特に次のように使っている人をすごく探しています。

  * `spark.sql.execution.arrow.enabled`を`true`にしてApache Sparkを使っている人（ようはApache SparkでApache Arrowを使っている人）

  * [TensorFlowのApache Arrowデータ読み込み機能](https://medium.com/tensorflow/tensorflow-with-apache-arrow-datasets-cdbcfe80a59f)を使っている人

  * [BigQuery Storage APIのApache Arrow対応機能](https://medium.com/google-cloud/announcing-google-cloud-bigquery-version-1-17-0-1fc428512171)を使っている人

他の使い方でもいいんですが、Apache Arrowをすでに使っている人は@ktouに連絡してください！ぜひApache Arrow東京ミートアップ2019で事例紹介をしてApache Arrowユーザーを増やすことに協力して欲しいのです！
