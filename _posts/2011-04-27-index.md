---
tags:
- milter-manager
title: milter managerとは？
---
先日milter manager 1.6.9をリリースしたので、久しぶりにmilter managerについて紹介します。
<!--more-->


### 特長

[milter manager](/software/milter-manager.html)はサーバーサイドの迷惑メール対策システムをできるだけ簡単に構築するためのフリーソフトウェアです。その秘密は強力な設定記述力にあります。

多くのソフトウェアはパラメーターを変更することでプログラムの動作をカスタマイズすることができます。milter managerにはRubyという本格的なプログラミング言語のインタプリタが内蔵されていて、単にパラメーターを変更するだけではなく、システムにインストールされている迷惑メール対策プラグインを自動検出する（設定の自動化）、怪しい送信元は迷惑メールチェックを強化するが認証済みユーザは緩くする、といったこともできてしまいます。しかも、多くの便利な設定は組み込みで含まれているため、そのまま使うだけでも自動でシステムに合わせて効果的な設定になってくれます。

もちろん、導入・運用を簡単にしたいという場合は、外部のメールシステムやアプライアンス製品の利用も十分選択肢に入るはずです。この場合は費用対効果やメールシステムを外部に出してもよいか、どこまでカスタマイズする必要があるか、などが判断基準になることでしょう。milter managerには[ユーザ向けメーリングリスト](https://lists.sourceforge.net/lists/listinfo/milter-manager-users-ja)もあるので、検討段階などで疑問点などがあった場合はそこで質問してみてください。

### まとめ

久しぶりにmilter managerについて紹介しました。

最近のmilter managerはパフォーマンス改善もおこなっており、個人サーバーから大規模なメールシステムまでいろいろな環境で効率的に動かせるようになっています。機会があったらmilter managerの利用を検討してみてはいかがでしょうか。

以下は参考情報です。

  * [milter managerのもう少し詳しい説明](http://milter-manager.sourceforge.net/reference/ja/introduction.html)（利用している技術など）
  * [milter managerのインストール方法](http://milter-manager.sourceforge.net/reference/ja/install-to.html)
  * [milter managerのソースコード](https://github.com/milter-manager/milter-manager)
