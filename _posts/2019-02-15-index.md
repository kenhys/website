---
tags:
- mozilla
- feedback
title: Firefox ESR60でタイトルバーのウィンドウコントロールボタンが機能しなくなる事がある問題の回避方法
---
Firefox ESR60をWindows 7で使用していると、ウィンドウコントロール（タイトルバーの「最小化」「最大化」「閉じる」のボタン）が動作しなくなる、という現象に見舞われる場合があります。Firefoxの法人サポート業務の中でこの障害についてお問い合わせを頂き、調査した結果、Firefox自体の不具合である事が判明しました。
<!--more-->


この問題は既にMozillaに報告済みで、Firefox 67以降のバージョンで修正済みですが、Firefox ESR60では修正されない予定となっています。この記事では、Firefox ESR60をお使いの方向けに暫定的な回避方法をご案内します。

### 問題の状況とその原因

この現象は、FirefoxのUIの設計に由来する物です。

一般的なWindowsアプリケーションは、タイトルバーなどを含めたウィンドウの枠そのものはWindowsに描画や制御を任せて、枠の内側だけでUIを提供します。それに対し、Firefoxのブラウザウィンドウでは、タイトルバー領域に食い込む形でタブを表示させるため、タイトルバーを含むウィンドウの枠まで含めた全体を自前で制御しています。そのため、タイトルバー領域に食い込む形でタブが表示されている場面では、Windowsが標準で提供しているウィンドウコントロールのボタンは実は使われておらず、それを真似る形で置かれた独自のUI要素で代用しています。

通常、これらの代用ボタンは他のUI要素よりも全面に表示されるため、タブなどの下に隠れる事はなく、ユーザーは見たままの位置にあるボタンをクリックできます。しかし、特定の条件下ではこれらの代用ボタンが他のUI要素の下に隠れてしまう形となり、ボタンをクリックできなくなってしまいます。

この問題が起こる条件は、以下の通りです。

  * Windows 7で、Windows自体のテーマとして「Classic」テーマを使用している。

  * ツールバー上の右クリックメニューから「メニューバー」にチェックを入れており、メニューバーが常時表示される状態になっている。

  * ウィンドウが`window.open()`で開かれ、その際、メニューバーを非表示にするように指定された。

Windowsのテーマ設定とFireofxのツールバーの設定を整えた状態で、[w3schools.comの`window.oepn()`の各種指定のサンプル](https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_win_open5)を開き「Try it」ボタンをクリックしてみて下さい。実際に、ボタンが機能しないためウィンドウを閉じられなくなっている[^0]事を確認できるはずです。

問題が再現した時のFirefoxの内部状態を詳しく調査すると、Classicテーマにおいては、[この状況下ではタブバーの`z-index`（重ね合わせの優先順位）が2になる](https://hg.mozilla.org/releases/mozilla-esr60/file/db09f3a62a37bbd3c34d3218459ff5304705f230/browser/themes/windows/browser.css#l204)のに対し、[ウィンドウコントロールの`z-index`は常に1になる](https://hg.mozilla.org/releases/mozilla-esr60/file/db09f3a62a37bbd3c34d3218459ff5304705f230/browser/themes/windows/browser.css#l304)ために、タブバーがウィンドウコントロールの上に表示されてしまっている状態である[^1]という事が分かりました。

なお、ClassicテーマはWindows 7以前のバージョンでのみ使用できる機能で、Windows 10以降では使えません[^2]。そのため、この問題はWindows 10以降では再現しません。

### 回避方法

原因が分かれば対策は容易です。

最も単純な対策は、ユーザープロファイル内に`chrome`という名前でフォルダを作成し、以下の内容のファイルを`userChrome.css`の名前で設置するというものです。

```css
@namescape url("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul");

/* WindowsでClassicテーマが反映されている場合、 */
@media (-moz-windows-classic) {
  /* タイトルバーにタブを表示する設定で、フルスクリーン表示でない時は、 */
  #main-window[tabsintitlebar]:not([sizemode=fullscreen]) #titlebar-buttonbox {
    /* ウィンドウコントロールの重ね合わせ順位をタブバーよりも上にする */
    z-index: 3 !important;
  }
}
```


ただ、ユーザープロファイルの位置はFirefoxの実行環境ごとに異なる[^3]ため、管理者側でこの対応を全クライアントに反映するのは難しいです。クライアント数が多い場合は、[Firefoxのインストール先の`chrome`や`browser\chrome`配下に置かれたCSSファイルを自動的に読み込むためのスクリプト](https://github.com/clear-code/globalchromecss/blob/master/autoconfig-globalchromecss.js)を[MCD用設定ファイル](https://www.mozilla.jp/business/faq/tech/setting-management/#mcd)に組み込むなどの方法をとるのがおすすめです。

### 修正の状況

原因が判明した時点で、本件はFirefox本体の問題としてbugzilla.mozilla.orgに以下の通り報告しました。

  * [1521692 - Unclickable window controls (minimize, maximize and close buttons) on a window opened via window.open() with "toolbar", without "menubar", when the menu bar is configured as always shown and running on Windows 7 with the Classic theme](https://bugzilla.mozilla.org/show_bug.cgi?id=1521692)

また、調査を行った時期にちょうどFirefoxのタブバー・タイトルバー周りの実装の仕方が変化していたため、最新の開発版でも状況を確認した所、Firefox 65以降ではWindows 7でなくても同様の問題が起こり、しかも今度はそもそもウィンドウコントロールが表示されなくなってしまっているという、より酷い状況でした。そのため、そちらは別の問題として以下の通り報告しました。

  * [1521688 - Missing window controls (minimize, maximize and close buttons) on a window opened via window.open() with "toolbar", without "menubar", when the menu bar is configured as always shown](https://bugzilla.mozilla.org/show_bug.cgi?id=1521688)

Firefox ESR60およびFirefox 64以前での問題（本記事で解説している問題）については、Firefox 65でタブバーの設計が変わったために現象としては再現しなくなった事と、セキュリティに関わる問題ではない事から、修正はされないという決定がなされています。

Firefox 65以降での問題については、既に修正のためのパッチを提供し、Firefox 67以降のバージョンに取り込まれる事が確定しています。Firefox 65に対しては修正はバックポートされず、Firefox 66については特に誰も働きかけなければ修正は反映されないままとなる見込みです。

### まとめ

以上、Firefoxの法人向け有償サポートの中で発覚したFirefoxの不具合について暫定的な回避方法をご案内しました。

当社のフリーソフトウェアサポート事業では、当社が開発した物ではないソフトウェア製品についても、不具合の原因究明、暫定的回避策のご提案、および（将来のバージョンでの修正のための）開発元へのフィードバックなどのサポートを有償にて行っております。Firefoxのようなデスクトップアプリケーションだけでなく、サーバー上で動作する各種ソフトウェアについても、フリーソフトウェアの運用でトラブルが発生していてお困りの企業のシステム管理担当者さまがいらっしゃいましたら、[メールフォーム](/contact/)よりご相談下さい。

[^0]: そのため、ウィンドウを閉じるにはCtrl-F4などのキーボードショートカットを使うなどの、ボタンを使わない方法を使う必要があります。

[^1]: タブバーの右端はあらかじめウィンドウコントロールを重ねて表示するために余白領域が設けられていますが、現象発生時には、この余白領域の上ではなく下にウィンドウコントロールが表示されており、「ボタンは見えているのにその手前に透明な壁があってクリックできない」というような状況が発生しているという状況です。

[^2]: Classicテーマ風のテーマは存在していますが、Windows 7以前のそれとは異なり、単に配色等をClassicテーマ風にするだけの物です。

[^3]: 安全のため、パスにランダムな文字列が含まれる形になっています。
