---
tags:
- ruby
- presentation
title: 'RubyKaigi 2015：The history of testing framework in Ruby #rubykaigi'
---
RubyKaigi 2015の2日目（2015年12月12日）に[The history of testing framework in Ruby](http://rubykaigi.org/2015/presentations/kou)というタイトルでRubyのテスティングフレームワークの歴史を紹介しました。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2015/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2015/" title="The history of testing framework in Ruby">The history of testing framework in Ruby</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2015/)

  * [スライド（SlideShare）](httsp://www.slideshare.net/kou/rubykaigi-2015)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-rubykaigi-2015)

### 内容

この発表の内容は次の2つにわかれています。

  1. Rubyにバンドルされているテスティングフレームワークを中心に、Ruby用のテスティングフレームワークの歴史を紹介

  1. Test::Unit APIの特徴を紹介

歴史は[Rubyのテスティングフレームワークの歴史（2014年版）]({% post_url 2014-11-06-index %})をベースにそろそろリリースされるRuby 2.3までの情報を追加したものになっています。RubyUnitについてもっと知りたい方はRubyUnitの開発者である助田さんが書いた[[Ruby] RubyKaigi 2015(2日目): 遠回りするかな](http://suke.cocolog-nifty.com/blog/2015/12/ruby-rubykaig-1.html)も読むとよいです。

Test::Unit APIについてはスライドの[72ページ](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2015/?page=72)以降を参照してください。ざっくりというとTest::Unit APIは普通のRubyのコードを書くようにテストを書けることを大事にしている、ということを具体例を交えて説明しています。

ただし、フィクスチャーのAPIはそれほどRubyらしくありません。いつものRubyでは次のようにブロックを使って後処理の詳細を隠蔽できます。

```ruby
File.open("README") do |file|
  # ...
end # ← ブロックが終わったら開いたファイルを確実に閉じる
```


しかし、Test::Unit APIでは次のように別のメソッドで後処理を書かなければいけません。いつものRubyの書き方をできていないということです。

```ruby
def setup
  @file = File.open("README")
end

def teardown
  @file.close
end
```


この点について[Fastly, Travis CI, GitHub 共催 RubyKaigi 前々夜祭](http://eventdots.jp/event/576111)で[田中 哲](http://www.a-k-r.org/)さんから指摘がありました。そこで、発表までの間にtest-unit（発表を聞いた人なら区別をつけられるでしょうがgemのやつです）のmasterでは次のように使えるようにしました。

```ruby
def setup
  File.open("README") do |file|
    @file = file
    yield # ここでtest_readが動く
  end # テストが終わったらファイルを閉じる
end

def test_read
  assert_equal("XXX", @file.read)
end
```


ただ、`@file = file`の部分がもやっとします。ふだんのRubyのコードではインスタンス変数に設定する使い方をしないからです。Test::Unit APIではインスタンス変数にテストで使うオブジェクトを設定するのは普通なので、テストコードという文脈では普通ですが、ふだんのRubyのコードという文脈では不自然なのです。

別の案として次のようなAPIも検討しましたが、やはりインスタンス変数に設定するところがもやっとします。（この書き方は既存のtest-unitですでに使えますが、非公開のAPIを使うことになるので実験用のコード以外では使わないでください。）

```ruby
def run_test
  File.open("README") do |file|
    @file = file
    super
  end
end
```


なお、環境変数を設定するような次のケースでは`setup`の中で`yield`する書き方で違和感はありません。インスタンス変数を使わないからです。

```ruby
def setup
  original_lang = ENV["LANG"]
  begin
    ENV["LANG"] = "C"
    yield
  ensure
    ENV["LANG"] = original_lang
  end
end
```


`File.open`のようにブロックにデータを渡す使い方のときは、次のようにテストの中で実行した方がよいのかもしれません。

```ruby
def test_read
  File.open("README") do |file|
    assert_equal("XXX", file.read)
  end
end
```


APIについてなにかアイディアがある方は[test-unit/test-unitのissue](https://github.com/test-unit/test-unit/issues)にコメントをお願いします。RubyKaigi 2015中には次のフィードバックをもらいました。

  * `setup`ではなく`around`など違う名前のAPIにした方がよいのではないか

  * そもそもなぜ`setup`なのか。テスト毎にインスタンスを作るなら`initialize`でやるのが普通ではないか

  * テストメソッド（この例では`test_read`）が引数を受け取るようにしたらよいのではないか（そうすればインスタンス変数に設定する必要はなくなる）

  * `yield`にオブジェクトを渡したら自動でインスタンス変数に設定するのはどうか

違う名前のAPIはよいかもしれませんが、`around`はなじまないのが悩ましいところです。Test::Unitの既存のフィクスチャーのAPIは`setup`/`teardown`というように役割を名前にしているので`around`というように順序を名前にするのはなじまないのです。RSpecのように`before`/`after`というように順序を名前にしているならなじむのかもしれません。

なお、`setup`の中で後処理も実行されることがもやっとするので`setup`ではない名前がよさそう、ということであれば、そこは心配しなくてもよいです。`File.open`のように本処理を名前にしているメソッドがブロック付きで呼ばれたら後処理をするようになることは普通のRubyのコードだからです。`setup`も本処理の名前です。

`initialize`でやるのはよいかもしれませんが、後処理をするタイミングを作れないのが悩ましいところです。普通のRubyのコードでは`File.open {...}`のように処理の範囲を明示しないときはGCのタイミングで後処理が動きます。テストでは他のテストに影響を残さないように、テストが終わったら確実に後処理を実行しておきたいです。

テストメソッドが引数を受け取ると複数のセットアップ処理を指定したときに破綻しそうな点が悩ましいところです。

`yield`にオブジェクトを渡すと自動でインスタンス変数を設定するのは可読性が悪くなりそうなところが悩ましいところです。

ちなみに、`setup`で`yield`しても動くようにする仕組みは次の擬似コードのように実装しています。

```ruby
def run_test
  block_is_called = false
  setup do
    block_is_called = true
    test_read
  end
  test_read unless block_is_called
end
```


### 他の発表

先日[RubyKaigi 2015にスピーカー・スポンサーとして参加予定]({% post_url 2015-12-09-index %})でオススメした通り、[咳さん](https://twitter.com/m_seki/)の[Actor, Thread and me](http://d.hatena.ne.jp/m_seki/20151214#1450022068)が非常に興味深かったです。

[bartender](https://github.com/seki/bartender)（咳フリークはこの名前を聞くと[Div](https://www2a.biglobe.ne.jp/seki/ruby/div.html)を思い出すでしょう）は[RubyWorld Conference 2014で話した同期っぽいAPI](https://slide.rabbit-shocker.org/authors/kou/rwc-2014/?page=32)の[実装](https://github.com/kou/rabbit-slide-kou-rwc-2014/blob/master/sync-like-api.rb)と同じようなものでした。いまだに着手していませんが、これをベースに[複数の通信を並行して進めるときにそれっぽく書けそうなAPI](https://slide.rabbit-shocker.org/authors/kou/rwc-2014/?page=36)があるとよさそうと検討していたのでした。

### まとめ

クリアコードがシルバースポンサーとして応援したイベント「RubyKaigi 2015」での発表「The history of testing framework in Ruby」の内容を簡単に紹介しました。また、オススメの咳さんの発表についても少し触れました。

今回、スポンサーとして参加した成果はまだわかりません。配布物として[リーダブルコードワークショップ](/services/code-reader/readable-code-workshop.html)のチラシとGroonga族のステッカーを置いておきました。参加者が帰った後に残った分を回収しましたが、どちらもほぼなくなっていました。興味があった方はぜひお問い合わせください。
