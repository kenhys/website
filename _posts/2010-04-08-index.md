---
tags: []
title: Muninプラグインの作り方
---
モニタリングツール[Munin](http://munin-monitoring.org/)のプラグインの作り方を簡単に紹介します。ざっくり紹介しているので、説明を省いているところもあります。というのも、次の話の準備のための説明だからです。
<!--more-->


Muninはネットワークのモニタリングに用いられることが多く、監視対象の各ホストに情報収集デーモン（munin-node）を配置し、そのデーモンがネットワークの情報などを収集します。監視ホストは定期的に情報収集デーモンにアクセスし、監視対象のホストの情報をグラフにします。グラフはHTMLで出力されます。PHPなどを導入しなくてもよいため、導入が楽ですし、リソース消費も少ないです。

監視対象ホスト上でどの情報を収集するかはインストール後に追加・削除できます。情報を収集する部分がプラグイン化されているためです。情報収集デーモンmunin-nodeにプラグインをインストールすればそのプラグインが収集する情報がモニタリング対象となり、自動的に監視ホスト上でグラフ化されます。

Muninはネットワークをモニタリングするために使われることが多いですが、プラグインが収集する情報はネットワーク情報に限定されていないため、仕様に従ったプラグインさえインストールすれば任意の情報をグラフ化することができます。

先日リリースされた[milter manager](/software/milter-manager.html) [1.5.0](http://milter-manager.sourceforge.net/blog/ja/2010/3/29.html)でもSMTPクライアントの同時接続数や各milterの適用結果をモニタリングするMuninプラグインを提供しています。SMTPクライアントの同時接続数はネットワークっぽい感じがしますが、milterの適用結果はあまりネットワークっぽい感じがしませんね。

それでは、まず、プラグインの仕様です。

### Muninプラグインの仕様

Muninプラグインは以下の入力と出力を満たす実行ファイルです。RubyスクリプトでもシェルスクリプトでもC言語で書いてコンパイルしたプログラムでもかまいません。ただ、後述するmunin-node-configureを使いやすいのでスクリプト言語の方がオススメです。

<dl>






<dt>






入力






</dt>






<dd>


実行ファイルへの引数。以下のいずれかが渡される。


  * config: 収集する情報のメタ情報を要求
  * autoconf: ホスト上でプラグインが利用可能かどうかの判断を要求
  * snmpconf: 収集する情報のSNMP情報を要求
  * suggest: プラグインのパラメータを要求（後述）
  * 引数なし: 収集する値を要求


</dd>








<dt>






出力






</dt>






<dd>


出力は入力毎に異なります。


  * config: 以下のような1行1パラメータというフォーマットでグラフや収集する情報を出力します。パラメータのキーの一覧は[protocol-config - Munin - Trac](http://munin-monitoring.org/wiki/protocol-config)（英語）にあります。
  {% raw %}
```shell
echo "graph_title milter manager statistics"
echo "graph_category milter-manager"
# ...
exit 0
```
{% endraw %}
  * autoconf: プラグインが利用可能なら"yes"を出力し、終了コード0で終了します。利用不可なら"no"を出力し、終了コード1で終了します。以下はシェルスクリプトでの例です。
  {% raw %}
```shell
if [ "$available" = "yes" ]; then
  echo "yes"
  exit 0
else
  echo "no (daemon isn't running)"
  exit 1
fi
```
{% endraw %}
  * snmpconf: [ConcisePlugins - Munin - Trac](http://munin-monitoring.org/wiki/ConcisePlugins#snmpconf)（英語）あたりを参照してください。
  * suggest: 利用可能なプラグインのパラメータを1行に1つずつ出力します。（詳しくは後述）
  {% raw %}
```shell
echo "report"
echo "status"
exit 0
```
{% endraw %}
  * 引数なし: 以下のように収集したデータを出力します。
  {% raw %}
```shell
echo "accept.value 4"
echo "reject.value 31"
exit 0
```
{% endraw %}


</dd>


</dl>

と、このくらいのことは検索すると日本語の情報もいろいろでてきます。が、なぜかautoconfとsuggestのことはあまりでてきません。これがMuninプラグインの便利なところな気がしますが、どうしてでしょう。

ということで、そのあたりの紹介もします。

### munin-node-configure

Muninにはmunin-node-configureというプラグインの追加・削除を自動的に行ってくれるツールがついています。ブログなどに書かれているプラグインの追加方法は以下のように手動でやっていることが多いです。

{% raw %}
```
% sudo ln -s /usr/share/munin/plugins/cpu /etc/munin/plugins/
```
{% endraw %}

しかし、手動で追加・削除は面倒です。システム管理をしている人ならなるべく自動化したいと思うことでしょう。

munin-node-configureを使うと、プラグインの引数に"autoconf"を渡して、プラグイン自身に利用可能かを判断させ、その結果に応じて追加・削除することができます。自作プラグインでも"autoconf"に対応することで管理がだいぶ楽になります。ブログなどに載っている自作Muninプラグインは一回限りの利用だけを想定しているからなのか、"autoconf"に対応しているものが少ないように見えます。

"autoconf"に対応していることをmunin-node-configureに伝えるためには以下の行をプラグイン内に埋め込んでおく必要があります。バイナリの実行ファイルよりスクリプトの方がよいと書いたのはこのためです。

{% raw %}
```shell
#%# family=auto
#%# capabilities=autoconf
```
{% endraw %}

このような行があるとmunin-node-configureは"autoconf"引数付きで呼び出してくれます。このときに"yes"と出力すれば自動で追加対象にしてくれます。[ConcisePlugins - Munin - Trac](http://munin-monitoring.org/wiki/ConcisePlugins#autoconf)（英語）あたりも参照してください。

munin-node-configureを利用した自動追加・削除は以下のコマンドでできます。

{% raw %}
```
% sudo -H /usr/sbin/munin-node-configure --shell --remove-also | sudo -H sh
```
{% endraw %}

munin-nodeを再起動すれば反映されます。

{% raw %}
```
% sudo /etc/init.d/munin-node restart
```
{% endraw %}

最近だと、/etc/init.d/以下のスクリプトを直接実行するよりもservice(8)を使う方がよいでしょう。

### ワイルドカードプラグイン

ワイルドカードプラグインとは1つのプラグインで複数のグラフの情報を収集できるプラグインのことです。（1.4からは同じようなことができる[マルチグラフプラグイン](http://munin-monitoring.org/wiki/MultigraphSampleOutput)という仕組みも増えています。）

ワイルドカードプラグインはプラグインファイルのファイル名の最後が「_」で終わっています。使うときは、ファイル名の最後にパラメータをつけたシンボリックリンクを張ります。

例えば、「milter_manager_」というワイルドカードプラグインに「status」というパラメータを渡したいときは以下のようにします。

{% raw %}
```
% sudo ln -s /usr/share/munin/plugins/milter_manager_ /etc/munin/plugins/milter_manager_status
```
{% endraw %}

プラグイン内では自分のファイル名（シェルやRubyでいえば`$0`）を見てパラメータを受けとります。これにより、1つのプラグインファイルで複数の情報を収集することができるようになります。

そして、どのパラメータを指定すればよいかをプラグインに判定させて自動でワイルドカードプラグインを追加・削除することもできます。このときに使われる入力が"suggest"です。この機能に対応していることをmunin-node-configureに教えるためにはプラグイン内に以下のような行を書いておきます。

{% raw %}
```shell
#%# family=auto
#%# capabilities=autoconf suggest
```
{% endraw %}

これを書いておけばmunin-node-configureが"suggest"引数付きでワイルドカードプラグインを実行してくれます。このとき、"status"と"report"というパラメータが利用できるのであれば、以下のように1行につき1パラメータで出力します。

{% raw %}
```
status
report
```
{% endraw %}

こうすると、1つのワイルドカードプラグインを2種類のプラグインとしてインストールしてくれます。このとき、インストールされるプラグインは「milter_manager_status」、「milter_manager_report」というような名前になります。

追加・削除方法は変わりません。

{% raw %}
```
% sudo -H /usr/sbin/munin-node-configure --shell --remove-also | sudo -H sh
```
{% endraw %}

munin-nodeの再起動も忘れないようにしましょう。

{% raw %}
```
% sudo /etc/init.d/munin-node restart
```
{% endraw %}

### まとめ

Muninプラグインの作り方を（実例は示さずに）紹介しました。実例を示さない代わりに本家のドキュメントの概要箇所をリンクしています。具体例はドキュメントやインストール済みのプラグインを見てください。プラグインは小さいシェルスクリプトかPerlスクリプトのことが多いので、読むとすぐにわかるでしょう。

また、管理がとても楽になり便利なのに、なぜかあまり触れられていない"autoconf"と"suggest"についても説明しました。自分でMuninプラグインを作るときは"autoconf"に対応することをオススメします。もちろん、ワイルドカードプラグインにする場合は"suggest"にも対応することをオススメします。

いつか、ソフトウェア開発にもMuninを使う例を紹介したいものです。例えば、自動化された単体テストのテスト数やカバレッジ率などもMuninを使ってグラフ化することができます。
