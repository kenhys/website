---
title: "Fluentd: Inputプラグインをゼロダウンタイム・リスタート機能に対応させる方法"
author: daipom
tags:
  - fluentd
---

こんにちは。[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

2024年12月14日にリリースした Fluent Package v5.2.0 では、ゼロダウンタイム・リスタートおよびアップデートの機能を追加しました。
この機能では、`in_udp`, `in_tcp`, `in_syslog`, の3種のInputプラグインの動作を止めずに、リスタートやアップデートを行うことができます。

本記事では、Fluentdのプラグイン開発者向けに、その他のInputプラグインをこの機能に対応させる方法について紹介します。

<!--more-->

### 前置き: Fluent Package v5.2.0 のゼロダウンタイム・リスタート/アップデート機能

2024年12月14日にリリースした Fluent Package v5.2.0 では、ゼロダウンタイム・リスタートおよびアップデートの機能を追加しました。
詳しくは、次の記事をご覧ください。

* [Fluent Package v5.2.0 リリース - ゼロダウンタイム・アップデート]({% post_url 2024-12-27-fluent-package-v5.2.0 %})

### Inputプラグインをゼロダウンタイム・リスタート機能に対応させる方法

Inputプラグインをゼロダウンタイム・リスタート機能に対応させる方法は、マルチワーカーに対応させる方法と基本的に同様です。

そのInputプラグインをマルチワーカーに対応させる場合は、それがマルチワーカーで動作して問題ないことを確認した上で、次のように`multi_workers_ready?`をオーバーライドして、`true`を返すようにします。

```rb
def multi_workers_ready?
  true
end
```

同様にゼロダウンタイム・リスタートに対応させる場合は、ゼロダウンタイム・リスタートで動作して問題ないことを確認（後述）した上で、次のように`zero_downtime_restart_ready?`をオーバーライドして、`true`を返すようにします。

```rb
def zero_downtime_restart_ready?
  true
end
```

例: https://github.com/fluent/fluentd/blob/d7164ddc73f006de345acec8d9beb1846fb6bcb3/lib/fluent/plugin/in_udp.rb#L73-L75

ゼロダウンタイム・リスタートで動作可能である条件は次の通りです。

* 他のFluentdインスタンスと並列して動作できること

これは、ゼロダウンタイム・リスタートの際に旧プロセスと新プロセスが並列して稼働する時間帯があり、その時間帯に動作可能な必要があるからです（詳細は後述する「Inputプラグインにダウンタイムが生じない仕組み」をご覧ください）。

マルチワーカー対応のInputプラグインであれば、大抵は他のFluentdインスタンスと並列動作できるはずです。
注意が必要なのは、ソケットやファイルの取り扱いです。
以下それぞれ説明します。

* ソケットの取り扱い
  * [server helper](https://docs.fluentd.org/plugin-helper-overview/api-plugin-helper-server)で提供されるソケットは、新旧プロセス間でもシェアされるため、並列動作可能です
    * ただし、`shared`オプションを`false`にする場合はシェアできないため、注意が必要です
  * 自前でソケットを管理している場合は、新旧プロセス間で同じソケットを開こうとして競合する可能性があるので、注意が必要です
* ファイルの取り扱い
  * `in_tail`の`pos_file`のように、ファイルに状態を書き込む場合などは、競合に注意が必要です
  * 大抵このようなInputプラグインはマルチワーカーに非対応のはずで、その場合はゼロダウンタイム・リスタートにも対応できないと考えればよいです

以上の点に注意し、問題なさそうであれば`zero_downtime_restart_ready?`をオーバーライドしてください。
これによって、ゼロダウンタイム・リスタートを実行する際に、そのプラグインにダウンタイムが生じなくなります。

### Inputプラグインにダウンタイムが生じない仕組み

ゼロダウンタイム・リスタートの際には、以下の「4.」の時点で新旧のワーカープロセスが並列動作します。

![zerodowntime-mechanism]({% link /images/blog/fluent-package-v5.2.0/zerodowntime-mechanism.png %})

この際、新ワーカープロセスの方は、`zero_downtime_restart_ready?`が`true`であるInputプラグインのみを起動します。
その他のプラグインは「6.」の時点で起動することになります。

つまり、「4.」の時点の新ワーカープロセスでは、Filter/Outputプラグインが動作しません。
この間Inputプラグインは、[with-source-only](https://github.com/fluent/fluentd/pull/4661)の機能を利用して、自動で用意される一時ファイルバッファー`source_only_buffer`にデータを出力します。
そのデータは、「6.」で全プラグインが稼働した後、自動でロードされて処理されます。

### まとめ

今回はFluentdのプラグイン開発者向けに、ゼロダウンタイム・リスタートおよびアップデート機能にInputプラグインを対応させる方法について紹介しました。

対応自体は3行のコードを足す簡単なものですので、`in_udp`や`in_tcp`プラグインのようなサーバー型のInputプラグインであれば、ここで説明した条件を満たしているかを確認した上で、ぜひ対応してみてください！

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Package へのアップデートについてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。
