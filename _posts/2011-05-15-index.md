---
tags:
- ruby
title: 64bit版Windows用のRubyInstallerの作り方
---
WindowsにRubyをインストールする場合、どうやってインストールしますか？現在のところ、以下のようにいくつも選択肢があります。
<!--more-->


  * [Ruby-mswin32 (ja)](http://www.garbagecollect.jp/ruby/mswin32/ja/download/release.html)
  * [ActiveScriptRuby](http://www.artonx.org/data/asr/)
  * [RubyInstaller for Windows](http://rubyinstaller.org/)
  * [Rumix](http://ruby.morphball.net/rumix/)
  * [能楽堂](http://www.artonx.org/data/nougakudo/jman.html)
  * ...

それぞれ特徴があるので自分の使い方にあったものを選ぶ必要がありますが、今回の話の趣旨は「これがオススメです！」というものを伝えることではないので、簡単に紹介するだけにしておきます。ピンときたものがあったら、それについてもう少し詳しく調べてみることをおすすめします。

  * Ruby-mswin32: インストーラではなく、バイナリをzip形式のアーカイブで配布。展開すればそのまま使える。32bit版と64bit版の両方あり。1.8と1.9の両方あり。Visual Studioでビルド。
  * ActiveScriptRuby: インストーラ形式。32bit版のみ。1.8と1.9の両方あり。HTML Application内でRubyを使うことができる。Visual Studioでビルド。
  * RubyInstaller for Windows: インストーラ形式。ただし、メッセージは英語。32bit版のみ。1.8と1.9の両方あり。[MinGW](https://ja.wikipedia.org/wiki/MinGW)でビルド。
  * Rumix: インストーラ形式。32bit版のみ。1.8と1.9の両方あり。バイナリはRuby-mswin32のものを利用。
  * 能楽堂: インストーラ形式。64bit版のみ。1.9.3（2011/05/15時点では未リリース）のみ。Visual Studioでビルド。Rails実行環境入り。

今回の話ではRubyInstaller for Windowsを使います。では、どうしてRubyInstaller for Windowsを使うのでしょうか。

### RubyInstaller for Windowsの特長

今回挙げたビルド済みRubyのパッケージの中で、RubyInstaller for WindowsのみがMinGWでRubyをビルドしています。これが、今回RubyInstaller for Windowsを選んだ理由です。RubyがMinGWでビルドされていると[Debian GNU/Linux上で拡張ライブラリをクロスコンパイル]({% post_url 2010-04-21-index %})することができます[^0]。

GNU/Linuxなどでは64bit版Rubyが普通に使われていますが、Windowsではまだそうでもないようです。しかし、今後はWindowsでも64bit版Rubyの利用が進んでいくでしょう。そうなった場合、拡張ライブラリは64bit版Ruby用のバイナリ入りで配布することが求められます[^1]。

普段からWindowsで開発している拡張ライブラリであれば簡単に64bit版Ruby用バイナリを作成できるでしょうが、メインの開発環境がGNU/Linux[^2]という場合は難しいです。GNU/Linuxで開発している場合は、GNU/Linux上でクロスコンパイルして64bit版Ruby用バイナリを作成できるととても便利です。そして、MinGW-w64を利用すれば64bit版Windows用のDLLをクロスコンパイルできるのです。

しかし、RubyInstallerはまだ64bit版Rubyには対応していません。つまり、64bit版Ruby用の拡張ライブラリをクロスコンパイルしてバイナリを作っても、それを使うための64bit版Rubyの入手が困難な状況ということです。

### 64bit版Ruby対応RubyInstaller

RubyInstallerが64bit版Rubyに対応していないため、クロスコンパイルした拡張ライブラリを使える64bit版Rubyを簡単にインストールできない問題をどのように解決すればよいでしょうか？簡単ですね。RubyInstallerを64bit版Rubyに対応させればいいのです。

ということで、対応させたものが[GitHub上のkou/rubyinstallerのmingw-w64ブランチ](https://github.com/kou/rubyinstaller/tree/mingw-w64)にあります。これを使って作成した64bit版Ruby[^3]用のRubyInstallerと、Debian GNU/Linux上でクロスコンパイルしたrroongaのgemを以下に置いておきます[^4]。

  * [64bit版Ruby用RubyInstaller for Windows](http://pub.cozmixng.org/~kou/archives/rubyinstaller-1.9.2-p180.exe)
  * [64bit版Ruby用RubyInstaller for Windows用バイナリ入りrroonga](http://pub.cozmixng.org/~kou/archives/rroonga-1.2.1-x64-mingw32.gem)

[rroongaのチュートリアル](http://groonga.rubyforge.org/rroonga/text/tutorial_ja_rdoc.html)で、実際にきちんと動くことを確認できます。

というように、なんとなく動くようになっていますが、まだいくつかやらなければいけないことがあります。

  * [インストーラの64bit対応](http://groups.google.com/group/rubyinstaller/msg/8a014a145cc95b5a?hl=en)
  * Ruby本体のMinGW-w64対応改善

ということで、Ruby 1.9.3がリリースされるまでにみなさんがこれらの修正できるように64bit版Ruby用RubyInstallerの作り方を紹介します。タイトルからは想像できなかった展開ですね。

### 64bit版Ruby用RubyInstallerの作り方

まず、初期設定の方法を紹介し、それからRubyをビルド・テストする方法とRubyInstallerを作成する方法を紹介します。

#### 初期設定

必要なものは以下の通りです。

  * Ruby 1.8.7の実行ファイル
  * [Inno Setup](http://www.jrsoftware.org/isinfo.php)
  * Cygwin
  * Ruby trunkのソースコード
  * RubyInstall for Windowsのソースコード

Ruby 1.8.7は[すでにあるRubyInstaller for Windows](http://rubyinstaller.org/downloads/)を使ってインストールします。Ruby 1.9.2では動作しないので注意してください。

Inno SetupはWindowsインストーラを作成するフリーソフトウェアです。[ダウンロードページ](http://www.jrsoftware.org/isdl.php)からisetup-5.4.2.exeをダウンロードしてインストールします。

本当は[Cygwin](http://www.cygwin.com/)はなくてもよいのですが、初期設定が楽なのでCygwinを使います。まず、[setup.exe](http://cygwin.com/setup.exe)をダウンロードしてCygwinをインストールします。追加でインストールするソフトウェアは以下の通りです。

  * subversion
  * autoconf
  * git

必須ではありませんが、mingw64-x86_64-binutilsもあると便利でしょう。

CygwinをインストールしたらRubyをチェックアウトします。trunkのRubyを修正しないと取り込んでもらえないからです。また、作業は~/work/ruby/[^5]以下で行うことにします。

{% raw %}
```
% mkdir -p ~/work/ruby
% cd ~/work/ruby
% svn co http://svn.ruby-lang.org/repos/ruby/trunk ruby
% cd ruby
% autoconf
```
{% endraw %}

RubyInstaller for Windowsのソースコードもチェックアウトします。mingw-w64ブランチの成果は[オフィシャルリポジトリ](https://github.com/oneclick/rubyinstaller)に取り込まれそうな雰囲気はあるのですが、まだ取り込まれていないのでmingw-w64ブランチを使います。

{% raw %}
```
% cd ~/work/ruby
% git clone https://github.com/kou/rubyinstaller.git
% cd rubyinstaller
% git checkout mingw-w64
```
{% endraw %}

以上が初期設定です。ここまでの作業は最初に1回行うだけです。今後は~/work/ruby/rubyinstaller/で作業を行います。

#### ビルド

ここからはcmd.exe上で実行します。ただし、cmd.exe上で長いコマンドを打つのは大変なので、バッチファイルを作成します。

build.bat:

{% raw %}
```
c:\Ruby187\bin\ruby.exe c:\Ruby187\bin\rake ruby19 LOCAL='C:\Cygwin\home\kou\work\ruby\ruby' ProgramFiles='c:\Program Files (x86)' dkver=mingw64-64-4.5.4 --trace > build.log 2>&1
```
{% endraw %}

これをC:\cygwin\home\kou\work\ruby\rubyinstaller\で実行します。

{% raw %}
```
C:\cygwin\home\kou\work\ruby\rubyinstaller>build.bat
```
{% endraw %}

小一時間ほど待つとsandbox\ruby19_mingw\以下に64bit用Rubyが作成されているはずです。

{% raw %}
```
C:\cygwin\home\kou\work\ruby\rubyinstaller>sandbox\ruby19_mingw\bin\ruby.exe --version
ruby 1.9.3dev (2011-05-14) [x64-mingw32]
```
{% endraw %}

ビルド時のログはbuild.logに保存されているので、問題が発生した場合はログを見て問題を解決し、RubyやRubyInstallerの開発チームに報告して解決内容を取り込んでもらいましょう。開発チームにコンタクトをとる方法など、関連リソースについては一番最後に書いてあるので参考にしてください。

#### パッケージの作成

パッケージの作成もコマンドが長いのでバッチファイルを作成します。

package.bat:

{% raw %}
```
c:\Ruby187\bin\ruby.exe c:\Ruby187\bin\rake ruby19:package LOCAL='C:\cygwin\home\kou\work\ruby\ruby' ProgramFiles='c:\Program Files (x86)' dkver=mingw64-64-4.5.4 --trace > package.log 2>&1
```
{% endraw %}

これをC:\cygwin\home\kou\work\ruby\rubyinstaller\で実行します。

{% raw %}
```
C:\cygwin\home\kou\work\ruby\rubyinstaller>package.bat
```
{% endraw %}

10分ほど待つとpkg\以下にRubyInstallerが作成されているはずです。このRubyInstallerにはsandbox\ruby19_mingw\以下にビルドされたRuby用が含まれています。RubyInstallerのファイル名はpkg\rubyinstaller-1.9.2-p180.exeになっていて、直さなければいけないものの1つです。ただ、このファイル名は後で直すことにして、まずは、試しにインストールしてみましょう。インストーラをダブルクリックするとインストールできます[^6]。

ここまでくればパッケージの修正作業はできますね。現在のところ、以下のような問題があります。

  * インストーラのファイル名がRubyのバージョンを表していない
  * [インストーラの64bit対応](http://groups.google.com/group/rubyinstaller/msg/8a014a145cc95b5a?hl=en)

#### テスト

テストの実行もコマンドが長いのでバッチファイルとシェルスクリプトを作成します。テストは[MSYS](https://ja.wikipedia.org/wiki/MSYS)上で実行するため、まずはcmd.exeからMSYSのbashに入るためのバッチファイルが必要です。

shell.bat:

{% raw %}
```
c:\Ruby187\bin\ruby.exe c:\Ruby187\bin\rake devkit:sh LOCAL='C:\Cygwin\home\kou\work\ruby\ruby' ProgramFiles='c:\Program Files (x86)' dkver=mingw64-64-4.5.4
```
{% endraw %}

次に、bash上でテストを実行するためのシェルスクリプトです。

run-test.sh:

{% raw %}
```shell
#!/bin/sh

export PATH=$PWD/sandbox/ruby19_mingw/bin:$PATH
ruby --version
cd sandbox/ruby19_build
make test-all
```
{% endraw %}

以下のように使います。

{% raw %}
```
C:\cygwin\home\kou\work\ruby\rubyinstaller>shell.bat
...
sh-3.1#$ ./run-test.sh > test.log 2>&1
```
{% endraw %}

テスト結果はtest.logに出力されます。この結果ですべてのテストがパスするようにしましょう。なお、r31560では「9486 tests,1877705 assertions, 29 failures, 8 errors, 82 skips」となりました。結果の詳細は以下にあります。

  * ['"make test-all" on ruby 1.9.3dev (2011-05-14) [x64-mingw32] (r31560) - Gist'](https://gist.github.com/972923)

やりがいがありそうですね。

### まとめ

MinGW-w64で64bit版Rubyをビルド・テストする方法と、ビルドした64bit版RubyのRubyInstallerを作成する方法を紹介しました。まだ、いくつか問題があるのでピンときた人はRuby 1.9.3がでるまでに修正してみてはいかがでしょうか？

関連リソース:

  * [RubyInstallerのリポジトリ](https://github.com/oneclick/rubyinstaller)
  * [RubyInstallerのメーリングリスト](http://groups.google.com/group/rubyinstaller?hl=en)
  * [Inno Setupのドキュメント](http://www.jrsoftware.org/ishelp/index.php)
  * [Rubyのリポジトリ](http://www.ruby-lang.org/ja/documentation/repository-guide)
  * [Ruby開発者の手引き](http://redmine.ruby-lang.org/projects/ruby/wiki/DeveloperHowtoJa)
  * [Ruby 1.9系ですでに報告されている問題](http://redmine.ruby-lang.org/projects/ruby-19/issues)（MinGW-w64でビルドした64bit版Rubyで発生している問題のいくつかはすでに報告済みです。すでに報告済みの問題はそのチケットに修正パッチなどを添付するとよいでしょう。）

[^0]: 補足すると、Ruby-mswin32とActiveScruptRubyで配布されている32bit版Ruby用の拡張ライブラリもクロスコンパイルできます。しかし、64bit版Ruby用の拡張ライブラリをクロスコンパイルできません。理由はVisual Studioでビルドした64bit版Rubyではmsvcrt.dllではなくmsvcr80.dllやmsvcr100.dllが使われているためです。MinGWでクロスコンパイルするとmsvcrt.dllを使うようになるため、異なるCランタイムライブラリを使うことになってしまい、うまく動かないのです。参考: [C ランタイム ライブラリ](http://msdn.microsoft.com/ja-jp/library/abx4dbyh.aspx)の「アプリケーションで msvcrt.dll と msvcr100.dll の両方を使用した場合に発生する問題」のところなど。

[^1]: Windows上でも需要がある拡張ライブラリであれば。

[^2]: rroongaやrcairoのケース。

[^3]: `ruby --version`は"ruby 1.9.3dev (2011-05-14) [x64-mingw32]"。

[^4]: 一時的に作ったものを置いているだけなので、ある日突然削除されているかもしれません。

[^5]: WindowsのパスではC:\cygwin\home\kou\work\ruby\。

[^6]: 右クリックで「管理者として実行」を選ばないといけないかもしれません。
