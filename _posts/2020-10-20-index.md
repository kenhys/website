---
tags:
- fluentd
title: libvirtを使ってppc64leの検証環境を立ち上げる話
---
### はじめに

クリアコードは[Fluentd](http://www.fluentd.org)の開発に参加しています。
Fluentd本体といくつかのプラグインがセットになったtd-agentというパッケージが提供されていますが、
td-agentのversion 4からaarch64(arm64)やppc64le(PowerPC)といった非x86_64環境にもパッケージの提供を開始しました。
<!--more-->


そのためtd-agent4のppc64leパッケージの動作確認をする必要がありましたが、実ハードが無いのでx86_64上のqemu-system-ppc64leで動作確認を行いました。
QEMUとは、FLOSSのプロセッサシミュレーターです。

今回はその手順を畑ケが紹介します。　[^0]

### libvirtでppc64leのCentOS 8環境を立ち上げるには

#### libvirtでppc64leのCentOS 8環境をインストールする

libvirtにてppc64leのCentOS 8環境を立ち上げるには、QEMUのディスクイメージを作成する必要があります。

libvirtを使うホスト環境はUbuntu 18.04.5 LTS amd64環境です。

今回の動作確認に必要なコマンドをインストールします。

```console
$ sudo apt install libvirt-clients virtinst libvirt-daemon-system qemu-utils qemu-system-ppc
```


CentOS 8 ppc64leのDVDイメージを取得します。

http://isoredirect.centos.org/centos/8.2.2004/isos/ppc64le/ より、近いミラーからCentOS 8のイメージを取得します。
ネットワークインターフェースの設定を行えばDVDイメージでなくてもインストールできますが、
今回はDVDイメージをダウンロードしてきました。

CentOS 8の最新版のDVD ISOイメージのCentOS-8.2.2004-ppc64le-dvd1.isoを入手しました。
CentOS 8をインストールする準備が整ったので、ppc64leのCentOS 8のDVDからブートしてみます。[^1]

```console
$ sudo virt-install --name=centos8-ppc64le --ram=4096 --vcpus=4 \
                    --os-type=linux --os-variant=rhel7 --arch=ppc64le \
                    --machine pseries-2.12 \
                    --disk=/var/lib/libvirt/images/centos8-ppc64le.img,format=qcow2,size=20 \
                    --cdrom=CentOS-8.2.2004-ppc64le-dvd1.iso \
                    --serial=pty --console=pty --boot=hd --graphics=none
```


ネットワークにはlibvirtが作成しているデフォルトネットワーク(virbr0)を指定します。

CentOSのインストーラが起動するので、指示に従ってインストールします。

後続の節ではCentOS 8 ppc64le環境が出来たものとして説明します。

#### libvirt環境のCentOS 8 ppc64leにパッケージをインストールするには

virt-installでネットワークの設定を有効化してインストールした場合、ゲストの`ip a`の出力は以下のようになります。

```console
$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:2b:5e:d1 brd ff:ff:ff:ff:ff:ff
    inet 192.168.123.250/24 brd 192.168.123.255 scope global dynamic noprefixroute enp0s1
       valid_lft 3462sec preferred_lft 3462sec
    inet6 fe80::2e8c:ddc7:91ff:f5fe/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```


ゲストからインターネットにも出られます。

```console
$ ping -c4 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=58 time=9.95 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=58 time=8.37 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=58 time=7.60 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=58 time=7.72 ms

--- 1.1.1.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 9ms
rtt min/avg/max/mdev = 7.599/8.410/9.952/0.939 ms
```


libvirtで立ち上げているゲストへホスト環境からscpでファイルを送ります。
今回作成した仮想マシンには192.168.123.250が割り当てられています。

```console
$ scp /path/to/td-agent-4.0.1-1.el8.ppc64le.rpm <awesome_user>@192.168.123.250:
```


libvirtで動作しているゲストのCentOS 8 ppc64le環境にてdnfを使ってscpを使って転送してきたパッケージをインストールします。

```console
$ sudo dnf install -y td-agent-4.0.1-1.el8.ppc64le.rpm
[sudo] password for user: 
Failed to set locale, defaulting to C.UTF-8
CentOS-8 - AppStream                                                                  2.0 MB/s | 5.0 MB     00:02    
CentOS-8 - Base                                                                       1.9 MB/s | 1.9 MB     00:01    
CentOS-8 - Extras                                                                     9.4 kB/s | 8.1 kB     00:00    
Last metadata expiration check: 0:00:01 ago on Mon Oct 19 22:28:38 2020.
Dependencies resolved.
======================================================================================================================
 Package                   Architecture             Version                       Repository                     Size
======================================================================================================================
Installing:
 td-agent                  ppc64le                  4.0.1-1.el8                   @commandline                   29 M

Transaction Summary
======================================================================================================================
Install  1 Package

Total size: 29 M
Installed size: 96 M
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                              1/1 
  Running scriptlet: td-agent-4.0.1-1.el8.ppc64le                                                                 1/1 
  Installing       : td-agent-4.0.1-1.el8.ppc64le                                                                 1/1 
  Running scriptlet: td-agent-4.0.1-1.el8.ppc64le                                                                 1/1 
Created symlink /etc/systemd/system/multi-user.target.wants/td-agent.service \u2192 /usr/lib/systemd/system/td-agent.service.
prelink detected. Installing /etc/prelink.conf.d/td-agent-ruby.conf ...

  Verifying        : td-agent-4.0.1-1.el8.ppc64le                                                                 1/1 

Installed:
  td-agent-4.0.1-1.el8.ppc64le                                                                                        

Complete!
```


libvirtゲスト環境にてtd-agentサービスを上げてみます。

```console
$ sudo systemctl start td-agent
```


libvirtゲスト環境にて立ち上がっていることを確認します。

```console
$ systemctl status td-agent
● td-agent.service - td-agent: Fluentd based data collector for Treasure Data
   Loaded: loaded (/usr/lib/systemd/system/td-agent.service; enabled; vendor pr>
   Active: inactive (dead)
     Docs: https://docs.treasuredata.com/articles/td-agent
$ sudo systemctl start td-agent
$ sudo systemctl status td-agent
● td-agent.service - td-agent: Fluentd based data collector for Treasure Data
   Loaded: loaded (/usr/lib/systemd/system/td-agent.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2020-10-19 22:32:44 EDT; 17s ago
     Docs: https://docs.treasuredata.com/articles/td-agent
  Process: 9127 ExecStart=/opt/td-agent/bin/fluentd --log $TD_AGENT_LOG_FILE --daemon /var/run/td-agent/td-agent.pid >
 Main PID: 9236 (fluentd)
    Tasks: 4 (limit: 22015)
   Memory: 134.6M
   CGroup: /system.slice/td-agent.service
           ├─9236 /opt/td-agent/bin/ruby /opt/td-agent/bin/fluentd --log /var/log/td-agent/td-agent.log --daemon /var>
           └─9239 /opt/td-agent/bin/ruby -Eascii-8bit:ascii-8bit /opt/td-agent/bin/fluentd --log /var/log/td-agent/td>

Oct 19 22:32:12 localhost.localdomain systemd[1]: Starting td-agent: Fluentd based data collector for Treasure Data...
Oct 19 22:32:44 localhost.localdomain systemd[1]: Started td-agent: Fluentd based data collector for Treasure Data.
```


無事に立ち上がっている事を確認しました。

### まとめ

td-agent 4のppc64leのパッケージを題材にppc64leのQEMU環境を作成し、実際にパッケージをインストールできるか、また、サービスとして立ち上げられるかを確認した時のやり方を思い出しながらまとめました。
FLOSSを触っていると大抵の場合x86_64環境や、行ってもaarch64(arm64)環境がせいぜいのため、このような大掛かりなエミュレーション環境を準備する事は少ないかもしれません。
しかし、Fluentdのような大きなプロジェクトになるとPowerPC環境でのパッケージを必要とされることがあります。その場合にも慌てないでエミュレーション環境を作成して動作確認を実施してみてはいかがでしょうか。

当社では、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Fluentdサポートサービス](/services/fluentd.html)を提供しています。Fluentd/Fluent Bitをエンタープライズ環境において導入/運用されるSIer様、サービス提供事業者様は、[お問い合わせフォーム](/contact/)よりお問い合わせください。

[^0]: ちなみに、Travis CIではppc64leのワーカーを提供していますが、td-agentのsystemdのUnitファイルのデバッグが主目的の今回の作業には不向きでした。そのため、Travis CIでテストするというのは検討対象外でした。

[^1]: No Transactional Memory support in TCG, try cap-htm=offというエラーが出るため、--machineには'pseries-2.12'を指定しました。
