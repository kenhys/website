---
tags:
- fluentd
title: Fluentd v0.14.13で追加されたコマンドのご紹介
---
Fluentd v0.14.13が2017-02-27にリリースされていました。新しいコマンドが2つ追加されたのでご紹介します。
<!--more-->


  * fluent-plugin-config-format

  * fluent-plugin-generate

### fluent-plugin-config-format

プラグインの設定をいくつかのフォーマットで出力するコマンドです。
README.mdを書くのを楽にしたりコマンドラインからプラグインの設定値の意味を調べるためのものです。
[Fluentd v0.12.16でプラグインの設定をコマンドラインから確認する方法]({% post_url 2015-10-09-index %})で紹介したものの実装を改善して使いやすくしました。

```
$ fluent-plugin-config-format --help
Usage: fluent-plugin-config-format [options] <type> <name>

Output plugin config definitions

Arguments:
        type: input,output,filter,buffer,parser,formatter,storage
        name: registered plugin name

Options:
        --verbose                    Be verbose
    -c, --compact                    Compact output
    -f, --format=FORMAT              Specify format. (markdown,txt,json)
    -I PATH                          Add PATH to $LOAD_PATH
    -r NAME                          Load library
    -p, --plugin=DIR                 Add plugin directory
```


以下のコマンドは同じ意味です。

```
$ fluentd --show-plugin-config input:tail
$ fluent-plugin-config-format --format=txt input tail
```


plugin helperや `Fluent::Plugin::Output` の設定についてはdocs.fluentd.orgへのリンクにしたいところですが、2017年2月某日時点ではまだページがないので、リンクを生成することができていません。
ドキュメントが書かれたら、リンクを追加したいと考えています。

### fluent-plugin-generate

新規プラグインを開発するときに使用するテンプレートを生成するためのコマンドです。
これまでは、既存のプラグインからコピーしたり、[公式のドキュメント](http://docs.fluentd.org/v0.12/articles/plugin-development)からコピーしたりしていましたが、このコマンドを使うことによって、これまでよりも簡単にプラグインの雛形を生成することができるようになりました。

```
$ fluent-plugin-generate --help
Usage: fluent-plugin-generate [options] <type> <name>

Generate a project skeleton for creating a Fluentd plugin

Arguments:
        type: input,output,filter,parser,formatter
        name: Your plugin name

Options:
        --[no-]license=NAME          Specify license name (default: Apache-2.0)
```


例えば、[Zulip](https://zulip.org/)に出力するプラグインを作成するときは以下のように利用します。

```
$ fluent-plugin-generate output zulip
License: Apache-2.0
        create Gemfile
        create README.md
        create Rakefile
        create fluent-plugin-zulip.gemspec
        create lib/fluent/plugin/out_zulip.rb
        create test/helper.rb
        create test/plugin/test_out_zulip.rb
Initialized empty Git repository in /tmp/fluent-plugin-zulip/.git/
```


このように、必要なファイルが一通り生成されるのですぐに開発を始めることができます。
ユニットテストにはFluentd本体と同じ [test-unit](https://test-unit.github.io/ja/) を使用しています。
ただし、このコマンドではFluentd v0.12以前のAPIを使用したプラグインを開発するためのファイルを生成することはできません。

[fluent-plugins-nursery/fluent-plugin-zulip](https://github.com/fluent-plugins-nursery/fluent-plugin-zulip) はこのコマンドを使用して開発を始めました。
README.mdのConfigurationの項は fluent-plugin-config-format を使用して生成しました。

### まとめ

Fluentd v0.14.13で追加された2つの新しいコマンドについて紹介しました。
他の変更点については[ChangeLog](https://github.com/fluent/fluentd/blob/master/ChangeLog)を参照してください。
