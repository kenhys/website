---
tags:
  - apache-arrow
title: Apache Arrowデータのメディアタイプ（MIMEタイプ）
author: kou
---
[Apache Arrow](https://arrow.apache.org/)に2番目に多くコミットしている須藤です。Apache Arrowはデータフォーマットも定義しているのですが、2021年6月24日に正式なメディアタイプ（MIMEタイプ）と拡張子が決まったので詳細をまとめます。

<!--more-->

### メディアタイプ（MIMEタイプ）

[メディアタイプ](https://www.iana.org/assignments/media-types/media-types.xhtml)はデータがどのようなものかを示す情報です。[IANA（Internet Assigned Numbers Authority）](https://www.iana.org/)が管理しています。

新しいメディアタイプは[フォーム](https://www.iana.org/form/media-types)から申請することができます。Apache Arrowデータのメディアタイプの申請内容は[Apache Arrowコミュニティーでまとめ](https://lists.apache.org/thread.html/re80b8c87d472ceaa2a38a26f853bb278cff3ea01a3a119696a540eee%40%3Cdev.arrow.apache.org%3E)、実際の申請は私が行いました。

申請するとIANAの担当の人に仲介してもらいながらレビューアーとやりとりすることになります。IANAの担当の人は有能でした。すごく反応が早く、レビューアーから返事がないと毎週レビューアーにリマインダーを送ってくれました。2021年5月10日に申請し、2021年6月24日に承認されました。1.5ヶ月ほどかかったのは数週間ほどレビューアーから反応がない期間があったためです。

IANAに登録せずにApache Arrowプロジェクトとして「このメディアタイプを使ってねー」とすることもできましたが、Apache ThriftはIANAに登録していたのでApache Arrowも登録することにしました。

### Apache Arrowのデータフォーマット

Apache Arrowは次の2つのデータフォーマットを定義しています。

  * [ファイルフォーマット](https://arrow.apache.org/docs/format/Columnar.html#ipc-file-format)
  * [ストリーミングフォーマット](https://arrow.apache.org/docs/format/Columnar.html#ipc-streaming-format)

中身はほとんど同じですが、次のような違いがあります。

  * ファイルフォーマットはデータすべてがないと処理を始められない。
  * ストリーミングフォーマットはすべてのデータがなくてもデータを先頭から順に読み出しながら処理できる。
  * ファイルフォーマットはランダムアクセス可能だがストリーミングフォーマットは先頭から順に読むことしかできない。

ファイルに保存してデータをやり取りする場合はファイルフォーマットが適切で、ネットワーク経由でデータをやり取りする場合はストリーミングフォーマットが適切です。

### Apache Arrowデータのメディアタイプ

それぞれメディアタイプは次のようになりました。

  * ファイルフォーマット：[`application/vnd.apache.arrow.file`](https://www.iana.org/assignments/media-types/application/vnd.apache.arrow.file)
  * ストリーミングフォーマット：[`application/vnd.apache.arrow.stream`](https://www.iana.org/assignments/media-types/application/vnd.apache.arrow.stream)

`application/vnd.apache.arrow.`まで共通です。

### Apache Arrowデータの拡張子

メディアタイプの申請内で拡張子も決める必要があったのでそれぞれ次のようにしました。

  * ファイルフォーマット：`.arrow`
  * ストリーミングフォーマット：`.arrows`

ストリーミングフォーマットの拡張子はなかなかパッとした案がでなかったのですが、JSON Linesは`.json`に`l`をつけて`.jsonl`にしているからStreamingの`s`をつけて`.arros`にするのはどう？という私の案が通って`.arrows`になりました。

### まとめ

Apache Arrowデータのメディアタイプと拡張子が決まったのでこれからはこのメディアタイプと拡張子を使ってください！HTTPでApache Arrowデータをやりとりするときとかに必要になります。

[Groonga](https://groonga.org/ja/)は違うメディアタイプ（`application/x-apache-arrow-streaming`）と拡張子（ストリーミングフォーマットに`.arrows`じゃなくて`.arrow`）を使っていたので近いうちに正式なメディアタイプに変更します。もともと、GroongaでApache Arrowデータを扱うときに公式なメディアタイプと拡張子があるといいなぁと思ったのでApache Arrowコミュニティーで私がメディアタイプどうするー？と声をあげたことからこの話が始まったのでした。一段落ついてよかったです。
