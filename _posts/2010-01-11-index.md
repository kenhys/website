---
tags:
- embedded
title: 'あしたのオープンソース研究所: GStreamer'
---
先日、[あしたのオープンソース研究所](http://oss.infoscience.co.jp/)の第6回でオープンソースのマルチメディアフレームワークである[GStreamer](https://ja.wikipedia.org/wiki/GStreamer)を紹介してきました。
<!--more-->


[![GStreamer]({{ "/images/blog/20100111_0.png" | relative_url }} "GStreamer")](/archives/ashitanoken-06/)

あしたのオープンソース研究所では、海外のオープンソースソフトウェアのドキュメントを翻訳されていて、[翻訳対象の文書も募集](http://oss.infoscience.co.jp/boshu.html)されています。GStreamerなどいくつか応募したのですが、そのうちの1つとしてGStreamerを採用してもらえたのでGStreamerの概要を紹介をしてきました。

スライドを見ただけでは伝わらないはずなので少し説明も加えておきます。いくつか省略しているページもあるので、完全版が見たい場合は画像のリンク先を見てください。

### GStreamerとは

[![GStreamerでできること]({{ "/images/blog/20100111_1.png" | relative_url }} "GStreamerでできること")](/archives/ashitanoken-06/gstreamer-03.html)

GStreamerはマルチメディアのフレームワークです。音声・動画の再生、フォーマットの変換、録音・録画など基本的なことはもちろん、[RTSP](https://ja.wikipedia.org/wiki/RTSP)などを用いたネットワーク通信を行うこともできます。

### 使い方

GStreamerにはgst-launchというコマンドラインツールが付属していて、gst-launchを使うことによりプログラムを作らなくてもGStreamerの機能を利用することができます。

[![再生]({{ "/images/blog/20100111_2.png" | relative_url }} "再生")](/archives/ashitanoken-06/gstreamer-04.html)

GStreamerにはplaybinという機能があり、再生するときはこの機能が便利です。URIを指定するだけで、内容からフォーマットを自動で判別し、フォーマットに合わせた再生処理をします。音声・動画どちらでも再生することができます。

[![変換]({{ "/images/blog/20100111_3.png" | relative_url }} "変換")](/archives/ashitanoken-06/gstreamer-05.html)

GStreamerではshのパイプのように機能をつなぎ合わせて目的を実現します。[Ogg](https://ja.wikipedia.org/wiki/Ogg) [Vorbis](https://ja.wikipedia.org/wiki/Vorbis)から[AAC](https://ja.wikipedia.org/wiki/AAC)へフォーマットを変換する場合は、以下のように機能を組み合わせます。

  * ファイルから読み込み（filesrc）
  * OggコンテナからVorbisデータを取り出し（oggdemux）
  * Vorbisデータをデコード（vorbisdec）
  * 音声データのフォーマットを微調整（audioconvert）
  * AACでエンコード（faac）
  * AACデータを[MP4](https://ja.wikipedia.org/wiki/MP4)コンテナに格納（ffmux_mp4）
  * ファイルへ書き込み（filesink）

[![]({{ "/images/blog/20100111_4.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-06.html)

GStreamerはVideo4Linuxにも対応しているので、Linux環境ではVideo4Linuxを利用してPCに接続されたWebカメラで録画することもできます。ここでは以下のように機能を組み合わせて、Webカメラで撮影した動画を画面に出力しています。

  * Video4LinuxでWebカメラから撮影した動画を取り込み（v4l2src）
  * 動画を画面に出力（autovideosink）

[![]({{ "/images/blog/20100111_5.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-07.html)

撮影した動画を画面に出力する前にテキストを挿入することもできます。GStreamerでは機能をつなぎ合わせて目的を実現するので、間に追加の機能を挿入することが簡単にできます。

  * Video4LinuxでWebカメラから撮影した動画を取り込み（v4l2src）
  * *[追加]* 動画にテキストを挿入（textoverlay）
  * 動画を画面に出力（autovideosink）

[![]({{ "/images/blog/20100111_6.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-08.html)

動画と合わせて音声も録音することができます。動画用のパイプライン（機能のつなぎ合わせ）と音声用のパイプラインが別々になっていることがポイントです。

[![]({{ "/images/blog/20100111_7.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-09.html)

GStreamerでは機能を付け替えることが簡単にできるため、データの入力元・出力先をファイルからネットワーク通信に変えてやることで別のホストに動画を転送することもできます。この例ではRTPなどを使わず、直接TCPでOggデータをやりとりしています。まず、データを受信して画面に表示するクライアント側を動かします。

[![]({{ "/images/blog/20100111_8.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-10.html)

別の端末で録画したものをOggにエンコードしてクライアント側にTCPでデータを送信します。[Theora](https://ja.wikipedia.org/wiki/Theora)にエンコードしている部分で「video/x-raw-yuv,framerate=10/1」としているのは、[フレームレート](https://ja.wikipedia.org/wiki/%E3%83%95%E3%83%AC%E3%83%BC%E3%83%A0%E3%83%AC%E3%83%BC%E3%83%88)をおとしてデータ量を減らすためです。

[![]({{ "/images/blog/20100111_9.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-11.html)

サーバ側・クライアント側で何もデータを変換しなければネットワーク経由でのファイルコピーも実現できます。GStreamerは機能をつなぎ合わせるための汎用的な環境を提供しているので、こんなこともできるよ、という話です。

GStreamerでどういうことができるのかというイメージをつかめたでしょうか。

### GStreamerの周辺

[![]({{ "/images/blog/20100111_10.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-12.html)

GStreamerは[GNOME](https://ja.wikipedia.org/wiki/GNOME)アプリケーションで利用されています。メディアプレイヤーの[Totem](https://ja.wikipedia.org/wiki/Totem)、音楽プレイヤーの[Rhythmbox](https://ja.wikipedia.org/wiki/Rhythmbox)、CDリッパーの[Sound_Juicer](https://ja.wikipedia.org/wiki/Sound_Juicer)、VoIP・ビデオ会議アプリケーションの[Ekiga](https://ja.wikipedia.org/wiki/Ekiga)などはGStreamerを利用しています。GStreamerはGNOMEアプリケーション以外でも利用されています。Flashプレイヤーの[Gnash](https://ja.wikipedia.org/wiki/Gnash)や動画編集ソフトの[PiTiVi](http://www.pitivi.org/)、Mozillaテクノロジーをベースとした音楽プレイヤーである[Songbird](https://ja.wikipedia.org/wiki/Songbird)などもGStreamerを利用しています。

[![]({{ "/images/blog/20100111_11.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-13.html)

GStreamerと類似しているソフトウェアには[Ffmpeg](https://ja.wikipedia.org/wiki/Ffmpeg)、[Phonon](https://ja.wikipedia.org/wiki/Phonon)、[QuickTime](https://ja.wikipedia.org/wiki/QuickTime)、[DirectShow](https://ja.wikipedia.org/wiki/DirectShow)などがあります。どれもマルチメディアを扱うソフトウェアなのでGStreamerと似ているのですが、GStreamerはこれらのソフトウェアと競合するものではありません。

[![]({{ "/images/blog/20100111_12.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-14.html)

これらのソフトウェアの関係を図示したものです。ここでは、「API」、「フレームワーク」、「ライブラリ」という層に分けていますが、一般的な分け方ではないので注意してください。実際、複数の層にまたがるソフトウェアが多く、この分類にすっきり当てはまるわけではありません。ただ、このようにざっくりと分類した方がイメージがつかみやすいのではないかということでこのような分類を導入しました。

上の層ほど高レベルのソフトウェアで下の層のソフトウェアを利用したりして実現されています。それぞれの層は以下のように分類しています。

ライブラリ層は[コーデック](https://ja.wikipedia.org/wiki/%E3%82%B3%E3%83%BC%E3%83%87%E3%83%83%E3%82%AF)などマルチメディアデータのフォーマットを扱う機能などを提供します。GUIがないことが多く、プログラムや付属のコマンドラインツールなどからライブラリの機能を利用することになります。1つのライブラリで必要な機能がすべて満たされる場合はこの層を直接利用するとよいでしょう。複数のライブラリが必要になる場合は、フレームワーク層やAPI層を利用した方が開発効率がよくなることが多いです。この層にあるソフトウェアは、サーバ上でも広く利用されているFFmpegやTheoraをエンコード・デコードする機能を提供するlibtheoraなどの各種コーデック、などです。

フレームワーク層はメディアフォーマットのエンコード・デコード機能だけではなく、複数のフォーマットを統一的に扱う機能や、マルチメディア再生時の制御機能など、メディアプレイヤーで必要になるようなマルチメディア関連の機能を包括的に提供します。フレームワークに後から機能（コーデックなど）を追加する仕組みがあることが多く、この仕組みにより、プログラムの変更を最小限に抑えながらアプリケーションを新しいフォーマットに対応させたりすることができます。より汎用的なアプリケーションを開発する場合はこの層を使って開発するとよいでしょう。この層にあるソフトウェアは、GStreamerやMac OS XのQuickTime、WindowsのDirectShowなどです。

API層は実際の処理を行わず、フレームワークやライブラリを利用してプログラマが安心してマルチメディア機能を使うための安定したAPIを提供する層です。プログラマがAPI層を利用する利点は、環境（やフレームワークやライブラリ）に依存せずに同じコードでマルチメディアの機能を利用できることです。この層にあるソフトウェアはQtに含まれているPhononやMac OS XでのQTKitです。QtプログラマはPhononが提供するAPIを用いてプログラムを開発することで、クロスプラットフォームで動作するマルチメディア機能を実現することができます[^0]。

GStreamerが他のソフトウェアと競合しないのは、GStreamerが他のソフトウェアの機能を利用できるからです。GStreamerは、後からGStreamerに機能を追加できるプラグインシステムを実装しています。プラグインシステムを用いて、FFmpegの機能を利用したり、Mac OS XではQuickTimeの機能を利用したり、WindowsではDirectShowの機能を利用したりできます。つまり、GStreamerは他のソフトウェアと協調して動作することができます。このため、他のソフトウェアと競合しないのです。

### GStreamerの仕組み

[![]({{ "/images/blog/20100111_13.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-16.html)

GStreamerの概要を理解するために大事な概念は以下の4つです[^1]。

  * エレメント
  * リンク
  * パッド
  * パイプライン（ビン）

エレメントが個々の機能に対応します。エレメントにはデータの出入り口となるパッドがあります。エレメントから別のエレメントにデータを渡す場合は、エレメント同士を接続しなければいけません。これをリンクといいます。エレメント同士を接続するときは、パッドとパッドを接続します。

エレメントをつなぎ合わせて目的とする機能を実現したら、エレメントをパイプラインに入れます。パイプラインに入れると、エレメントの処理の開始・停止などを一括で指示できるようになります。それぞれのエレメントに対して指示する必要はありません。

[![]({{ "/images/blog/20100111_14.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-17.html)

エレメントはパッドの持ち方で以下の3種類に分類できます。

  * ソースエレメント
  * フィルタエレメント
  * シンクエレメント

ソースエレメントはデータ出力用のパッド（ソースパッド）のみを持つエレメントです。データ生成用のエレメントで、エレメントのつなぎ合わせの先頭におきます。ファイルからデータを読み込むエレメントなどがソースエレメントです。

[![]({{ "/images/blog/20100111_15.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-18.html)

フィルタエレメントはデータの入力用パッド（シンクパッド）と出力用パッド（ソースパッド）を持つエレメントです。入力用と出力用のパッドを1つずつ持つエンコーダーやデコーダーのようにデータを変換するエレメントがあります。エレメントはパッドを複数持つことができます。マルチプレクサーは複数の入力から1つのコンテナフォーマットのデータを生成し、デマルチプレクサーは1つのコンテナフォーマットのデータを分解し、複数の出力データを生成します。

[![]({{ "/images/blog/20100111_16.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-19.html)

シンクエレメントはデータ入力用のパッド（シンクパッド）のみを持つエレメントです。データ受信用のエレメントで、エレメントのつなぎ合わせの最後におきます。ファイルにデータを出力するエレメントなどがシンクエレメントです。

[![]({{ "/images/blog/20100111_17.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-20.html)

目的の機能を実現するためにはソースエレメント→フィルタエレメント→…→フィルタエレメント→シンクエレメントというようにリンクします。

[![]({{ "/images/blog/20100111_18.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-21.html)

リンクしたエレメントはビン（パイプライン。パイプラインはビンの1種）に入れて利用します。

[![]({{ "/images/blog/20100111_19.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-22.html)

例えば、Ogg Vorbisを再生する場合はこのようにエレメントをリンクします。

[![]({{ "/images/blog/20100111_20.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-23.html)

どのエレメント同士もリンクできるわけではありません。パッドは受け付けられるMIME-typeを複数持っています。パッド同士が同じMIME-typeを利用する場合のみエレメントをリンクできます。

Ogg Vorbisプレイヤーの場合はこのようなMIME-typeのパッドでリンクしています。

[![]({{ "/images/blog/20100111_21.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-24.html)

Ogg Vorbis/Theoraプレイヤーはこのようになります。Oggデマルチプレクサーからの出力を両方とも利用しています。

### まとめ

[![]({{ "/images/blog/20100111_22.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-25.html)

GStreamerはマルチメディアフレームワークで、マルチメディアを扱う場合に必要な機能が一通り揃っていて、GNOMEアプリケーションなど多くのアプリケーションで利用されていています。PhononやQuickTimeなど類似のソフトウェアがありますが、GStreamerはそれらと競合するソフトウェアではなく、それらと協調して動作します。

[![]({{ "/images/blog/20100111_23.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-26.html)

GStreamerにはエレメントとパッドとビンという概念があります。

エレメントはデータを処理するもので、複数のエレメントをつなぎ合わせて目的の機能を実現します。エレメント同士をつなぎ合わせることをリンクといい、エレメントをリンクするときはエレメントのパッドとパッドをつなぎ合わせます。リンクしたエレメントをビン（パイプライン）に入れて目的の機能を利用します。

[![]({{ "/images/blog/20100111_24.png" | relative_url }} "")](/archives/ashitanoken-06/gstreamer-27.html)

ここではGStreamerの概要のみを扱ったので、省略したことがたくさんあります。

あしたのオープンソース研究所では[GStreamerのチュートリアル](http://gstreamer.freedesktop.org/data/doc/gstreamer/head/manual/html/)を翻訳してくれるそうです。チュートリアルにはここで説明したことより多くのことが書かれているので、GStreamerに興味を持った方はチュートリアルも読んでみてください。翻訳は1,2ヶ月後には公開されているようなので、2月か3月になると日本語でチュートリアルが読めるのではないでしょうか。楽しみですね。

解説付きで資料を公開してみました。スライド中で使っているSVGの画像やRabbitのソースもあわせて公開しました[^2]。もし利用する場合ははじめにCOPYINGに書かれたライセンスを確認してください。

[^0]: GStreamerもクロスプラットフォームで動作するので、GTK+プログラマは直接GStreamerを利用することでクロスプラットフォームで動作するマルチメディア機能を実現することができます。GTK+アプリケーションではありませんが、GStreamerを用いてクロスプラットフォーム対応しているアプリケーションとしてSongbirdがあります。

[^1]: 概要より詳しいところまで理解する場合はもっと概念が増えます

[^2]: スライド公開ページに「ソース」というリンクがあります。
