---
tags:
- company
- presentation
title: 'クリアコード10周年祝い開催 #cc10th'
---
[お知らせしていた]({% post_url 2016-07-13-index %})通り、クリアコードの設立記念日である7月25日に[クリアコード10周年祝い](https://clear-code.doorkeeper.jp/events/48646)イベントを開催しました。今回はGroongaのサポートサービスでお付き合いのあった[小峯](https://twitter.com/s977043)さんにイベントの写真を撮ってもらったので写真多めでイベントの様子を紹介します。小峯さん、ありがとうございました！
<!--more-->


![クリアコード10周年祝い]({{ "/images/blog/20160727_0.jpg" | relative_url }} "クリアコード10周年祝い")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>

会場を提供してくれた[Speee](http://speee.jp/)さんは非常に柔軟に対応してくれました。非常に助かりました。ありがとうございます！

![対応してくれたSpeeeのみなさん]({{ "/images/blog/20160727_1.jpg" | relative_url }} "対応してくれたSpeeeのみなさん")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>

設立以来10年に渡り協力してきた[Mozilla Japan](https://www.mozilla.jp/)さんからはフォクすけコラボのケーキを差し入れてもらいました。とてもおいしかったです！

![フォクすけコラボケーキ]({{ "/images/blog/20160727_2.jpg" | relative_url }} "フォクすけコラボケーキ")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>

[フォクすけFAQ](http://foxkeh.jp/faq/)にある通り、フォクすけの最初のデザインはクリアコードメンバーの結城です。クリアコードのロゴも結城がデザインしたものなので、結城デザインをふんだんに使ったケーキとなりました。

Mozilla Japanからは[浅井](https://twitter.com/dynamitter)さんと伊藤さんが参加してくれました。代表の[瀧田](http://www.itmedia.co.jp/im/articles/1111/07/news127.html)さんからは花束と手書きのメッセージカードをもらいました。ありがとうございます！

![Mozilla Japanのみなさん]({{ "/images/blog/20160727_3.jpg" | relative_url }} "Mozilla Japanのみなさん")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>

同じく、設立以来10年に渡り協力してきた[ミラクル・リナックス](https://www.miraclelinux.com/)さんからは樽酒を差し入れてもらいました。（呑みきれなかったお酒は会場を提供してくれたSpeeeのみなさんに振る舞いました。お祝いのおすそ分け！）

![ミラクル・リナックスの高橋さんと差し入れてもらった樽酒]({{ "/images/blog/20160727_4.jpg" | relative_url }} "ミラクル・リナックスの高橋さんと差し入れてもらった樽酒")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>

最近は営業面でもミラクル・リナックスさんと協力しており、南がミラクル・リナックスさんのオフィスにいる機会が増えています。お祝いに来てくれた高橋さんともがっちり協力して仕事をしています。

他にも[@yuumi3](https://twitter.com/yuumi3)さんからは10年もののワイン（クリアコードと同じ！）を、[安川さん](https://twitter.com/yasulab)からはスパークリングワインをいただきました。

![@yuumi3さんと安川さん]({{ "/images/blog/20160727_5.jpg" | relative_url }} "@yuumi3さんと安川さん")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>

[@smellman](https://twitter.com/smellman)さんからはおつまみのナッツを、大久保さんからは和菓子をいただきました。

みなさんからお祝いいただき、クリアコードメンバー一同大変嬉しく思っています。

![参加したクリアコードメンバー]({{ "/images/blog/20160727_6.jpg" | relative_url }} "参加したクリアコードメンバー")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>

イベントに合わせてたくさんの[お祝いメッセージ](https://clear-code.doorkeeper.jp/events/48646#messages)をいただきました。嬉しいのでここにも同じものを掲載します。

---

<span id="message-iwaim">[いわい](https://twitter.com/iwaim)さん</span>

おめでとうございます！

---

<span id="message-yuumi3">[@yuumi3](https://twitter.com/yuumi3)さん</span>

おめでとうございます〜

---

<span id="message-smellman">[smellman](https://twitter.com/smellman)さん</span>

ぴろたんのかっこいい姿を是非みたいです。

---

<span id="message-hayamiz">[はやみず](https://twitter.com/hayamiz)さん</span>

10周年たいへんおめでとうございます！！

---

<span id="message-yasulab">[安川要平](https://twitter.com/yasulab)さん（YassLab代表）</span>

10周年おめでとうございます!
クリアコードさんを１つの成功事例として、弊社もOSSに継続的に貢献できる組織になれたらと考えております!

---

<span id="message-nisshieeorg">[西岡](https://twitter.com/nisshieeorg)さん（株式会社Speee）</span>

おめでとうございます！

---

<span id="message-ito">伊藤さん（一般社団法人Mozilla Japan）</span>

10周年おめでとうございます。
まだ御社とのおつきあい短い私なのですが、参加させていただいてもよろしいでしょうか？
どうぞよろしくお願いいたします。

---

<span id="message-watanabe">渡邉さん（株式会社Speee取締役）</span>

めでたいっす！
10周年のうち、ほんの一部しか期間はかぶってませんが、想いは強いっす！！
全力でお祝いしたいっす！！！

---

<span id="message-tsu-root">[@tSU_RooT](https://twitter.com/tSU_RooT)さん</span>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">Debianのパッケージってどうやって入ってるのとか、気になった時にククログ読んでます。 <a href="https://t.co/kR3VvarcUB">https://t.co/kR3VvarcUB</a></p>&mdash; つる (@tSU_RooT) <a href="https://twitter.com/tSU_RooT/status/753078508407156737">2016年7月13日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


---

<span id="message-hiroysato">[@hiroysato](https://twitter.com/hiroysato)さん</span>

<blockquote class="twitter-tweet" data-cards="hidden" data-lang="ja"><p lang="ja" dir="ltr"><a href="https://twitter.com/piro_or">@piro_or</a> 10周年おめでとうございます。昨年の肉の日にGroonga事例で発表しました。<a href="https://t.co/3b3GDeuuAw">https://t.co/3b3GDeuuAw</a> いつもお世話になっております。ありがとうございます。</p>&mdash; hiroyuki sato (@hiroysato) <a href="https://twitter.com/hiroysato/status/753166190952927232">2016年7月13日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


---

<span id="message-zunda">[zunda](https://twitter.com/zundan)さん</span>

10周年おめでとうございます!! 皆様が今後ともクリアコードとともにフリーソフトウェアの各方面でご活躍を続けることと期待しています :hearts:

---

<span id="message-lef">[lef](https://twitter.com/lef)さん（株式会社レピダム代表取締役）</span>

クリアコード10周年、本当におめでとうございます！
OSSを中心にISVとしてここまでの業績を残されるのがどれほど大変なことか。ソフトウェアを生業としている経営者として、ただただ尊敬です。
今後の更なる繁栄と躍進を祈念しております！

---

<span id="message-takahashi">高橋さん（ミラクル・リナックス株式会社）</span>

クリアコードの皆様
創立10周年をお迎えになり誠におめでとうございます。
クリアコードさんは、様々な開発案件を通じて共に成長してきた仲間であると感じております。
今後ますますのご躍進をお祈りいたしております。

---

<span id="message-takano">高野さん（株式会社Speee）</span>

10周年おめでとうございます！
クリアコードさんの勉強会のおかげでOSS活動を始めることができました！

---

<span id="message-ohkubo">Hiroshi Ohkuboさん</span>

最近、関係プロジェクトにて（しょぼい）パッチを取り込んでもらっています。親切にご対応頂きまして、アウトプット恐怖症が少し和らいできました。
もちろん、ブログをはじめとして、各種情報にも大変お世話になっています！

---

<span id="message-arika">[やまだあきら](https://twitter.com/arika)さん（有限会社ヴァインカーブ）</span>

10周年おめでとうございます。

お仕事でも、プライベートでも、いつも本当にお世話になっています。お話をする中で刺激を受け、そのたびにOSSの楽しみを新たにしています。

---

<span id="message-sezemi-admin">[広瀬俊哉](https://twitter.com/sezemi_seplus)さん（株式会社SEプラス取締役）</span>

10周年おめでとうございます!!
どちらの日程とも参加できず、すいません。

というわけで、Oiwaiの気持ちと感謝を書きました

[クリアコード Before/After](https://gist.github.com/sezemiadmin/3a14d0e7693348f6416fb365018f779d)

---

<span id="message-snoozer05">[島田浩二](https://twitter.com/snoozer05)さん（株式会社えにしテック代表取締役）</span>

会社を継続して運営していくことと、価値のあるソフトウェアを継続して開発していくことには、重なる部分があると考えています。

変化に耐えられるよう、コードをよい状態に維持し続けなければ、開発を継続することはできません。
そして会社も、よい状態に維持し続けなければ、いずれ変化に耐えられなくなってしまいます。

10年という節目を迎えられたということは、クリアコードのみなさまが「開発を続けられるコードを書く」ように、会社運営と真摯に向き合ってこられた結果なのだろうと拝察いたします。

10周年、誠におめでとうございます。心よりお祝い申しあげます。

「自分達がフリーソフトウェアを書いたり使ったりするというだけではなく、より多くの人がフリーソフトウェアを書いたり使ったりするようになればよい」

そんなビジョンで活躍し続ける会社が世の中にあるのは、とてもかっこいい、とぼくは思います。
これからもぜひ、そのかっこいい姿を後ろから追わせてください。
いつもありがとうございます。

クリアコードのみなさまの、今後ますますのご活躍を心よりお祈りしております。

---

<span id="message-tigger-501st">[てぃがー](https://twitter.com/tigger_501st)さん</span>

以前OSS Gate WorkShopに参加させていただいたものです！
10周年おめでとうございます。

---

<span id="message-su-kun-1899">[@su_kun_1899](https://twitter.com/su_kun_1899)さん</span>

10周年おめでとうございます！

「リーダブルコード」の読書ログに「この本で一番よかったのは解説だ」って書いています！リーダブルコードの解説を書いてくれてありがとうございます！

> しかし、この本で最も素晴らしいと感じたのは巻末の解説だ。
正直なところ、可読性の高いコードを書くかどうかなんての個々人のパーソナリティに依存してしまうと思っていた。
(丁寧に変数名をつける人は、そもそも気を使えるでしょう、というように)
> つまり、本書を手に取るような人はそもそもコードの品質に気を使っている人であり、問題はそうではない人たちが残念ながらいることなのだ。
> 解説では、この本を読んだ後にまず自分が実践し、
そして次に仲間たちに伝えていくことの重要さを説いている。
> ほぼ全てのプロダクトが、一人だけで作っているわけではない。
こういう書籍を読んだ後に大事なのは、自分が得たものを如何に現場にフィードバックしていけるかどうかだと改めて教えられた。


関連ツイート：

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">あとがきがいいって聞かないというインタビューがあったわけですが直後に須藤さんにあとがきがよかったっていう思いの丈をesaに書いた人がいたのが弊社で、インタビューのことも知らないだろうにあれはなんかよかった <a href="https://t.co/9MDMhJMiBY">https://t.co/9MDMhJMiBY</a></p>&mdash; tanabe sunao/たなべすなお (@sunaot) <a href="https://twitter.com/sunaot/status/720998152250068992">2016年4月15日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


---

<span id="message-tricknotes">[tricknotes](https://twitter.com/tricknotes)さん（株式会社えにしテック）</span>

10周年おめでとうございます！

クリアコードさんからは気づけばいつも多くの学びをもらっているなと感じます。
それは例えば、気になって調べてみたライブラリ(クリアコードの方がメンテされている、というのに後から気づく)のコードからであったり、調べ物で行き着いた先のククログからであったり。

ちゃんと実績を積んで、その価値ある成果を誰でも自由に利用できる形で世界に残して続けている姿には尊敬の念を抱くばかりです。
わたしもその姿を目指して頑張っていきたいと思います！

今後のご活躍も陰ながら応援してます！

---

<span id="message-dynamis">[dynamis](https://twitter.com/dynamitter)さん（一般社団法人Mozilla Japan）</span>

クリアコード10周年！ステキ！(・・).
これから更に10年、ますます発展されていくクリアコードさんとより緊密に仕事をしていければ幸いです。

---

<span id="message-daisuke-suzuki">[鈴木大輔](https://twitter.com/daisuke_suzuki)さん（有限会社ヴァインカーブ）</span>

10周年おめでとうございます!
これからもますますのご繁栄とご活躍をお祈りしています!

---

<span id="message-mas">[野村](https://twitter.com/monkey_mas)さん</span>

会社設立十周年、誠におめでとうございます！
今後ますますご活躍くださいますこと、心より期待しております！ :)

---

<span id="message-naoa-y">[村上](https://twitter.com/naoa_y)さん</span>

クリアコード10周年おめでとうございます！

---

イベントではお祝いに駆けつけてくれたみなさんからのトークもありました。

安川さんからは「[Ruby on Railsガイド](http://railsguides.jp/)を翻訳する活動はOSS Gateで取り組んでいる活動のようにOSS開発に参加する人を増やすきっかけだよなぁ」というトークがありました。その通りですね！ドキュメント関連の活動をきっかけにOSS開発に参加するというのもとてもよいですね！

Speeeの[高野](https://www.facebook.com/totoro.yt)さんからは「[Speeeさん向けOSS Gateワークショップ](http://technica-blog.jp/entry/2016/06/24/185427)をきっかけにOSS開発へ参加するようになった！」というトークがありました。とてもうれしいことです。発表資料は[クリアコード10周年祝い LTスライド](https://speakerdeck.com/yasuomitakano/kuriakoto10zhou-nian-zhu-i-ltsuraido)で公開されています。高野さんのOSS開発への歩みは[クリアコードさんのおかげでOSS活動デビューすることができました！ - TECHNICA Speee engineer blog](http://technica-blog.jp/entry/2016/07/28/141440)にまとまっています。

![安川さんと高野さんのトーク]({{ "/images/blog/20160727_7.jpg" | relative_url }} "安川さんと高野さんのトーク")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>

このような感じでみなさんとクリアコード10周年を楽しくお祝いできました！

![イベントの様子]({{ "/images/blog/20160727_8.jpg" | relative_url }} "イベントの様子")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>

これからもクリアコードをよろしくお願いします！

![集合写真]({{ "/images/blog/20160727_9.jpg" | relative_url }} "集合写真")
<span class="photo-license">撮影：[小峯将威](https://twitter.com/s977043)さん、ライセンス：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)</span>
