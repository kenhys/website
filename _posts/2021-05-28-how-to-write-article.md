---
title: ククログの記事の書き方
author: piro_or
---

結城です。

このブログ（ククログ）は現在、Jekyllで作られています。この[Webサイト自体のリポジトリはgitlab.comで公開されている](https://gitlab.com/clear-code/website)ため、記事のデータを追加するマージリクエストをGitLabで送っていただくことで、社内のみならず社外の方からも記事を書いていただけるようになっています。

この記事では、社内に向けての説明も兼ねて、この記事自体を例に、ククログの記事の執筆から公開までのワークフローを説明します。

<!--more-->

### 事前準備

ククログの執筆にあたっては、以下の準備が必要です。

* Gitクライアント
* [gitlab.com](https://gitlab.com/)のアカウント
* ローカルでプレビューを実施する場合、Jekyllの実行環境
  * RubyとBundlerを実行できる必要があります。Ubuntuであれば、`sudo apt install ruby`でRubyとRubyGemsをインストールし、`gem install bundler`でBundlerをインストールしておいてください。
* 記事にするネタ

[gitlab.comのclear-codeグループ](https://gitlab.com/clear-code)に参加していないユーザーや、コミット権がないユーザーの場合、まずは自分のアカウント配下に[clear-code/websiteプロジェクト](https://gitlab.com/clear-code/website)をforkしてください。

[![（スクリーンショット：プロジェクトのトップページ）]({% link /images/blog/how-to-write-article/fork-button.png %} "スクリーンショット：プロジェクトのトップページ")]({% link /images/blog/how-to-write-article/fork-button.png %})

プロジェクトのトップページの右上の「Fork」ボタンをクリックして、プロジェクトのフォーク先のオーナーになる名前空間（通常は自分のアカウント）を選択すると、プロジェクトがforkされます。以後は、これをローカルにcloneして作業することになります。

### 記事の執筆手順

記事を追加する際は、1記事ごとに専用のブランチを作成します。ブランチ名は分かりやすいものであれば何でもよく、今回の例では、ブログ記事のブランチということで `blog-` をprefixにし、記事の内容に基づいて `blog-how-to-write-article` としました。Bashのシェルでgitコマンドを使っている状況であれば、以下の要領です。

```console
$ git clone https://gitlab.com/piroor/website ~/clear-code-website
$ cd ~/clear-code-website
$ git checkout -b blog-how-to-write-article
```

ブログの記事は、執筆段階では下書きとして作成します。`_drafts/` 配下に `(任意の識別子).md` のような形式のファイル名で、テキストファイルとしてファイルを作成してください。この記事では、ブランチ名と同様に（ただし、prefixを取り除いて） `how-to-write-article.md` としました。

告知の都合などで公開日を明示したい場合は、 `_drafts/` 配下ではなく `_posts/` 配下に `_posts/YYYY-MM-DD-(識別子).md` という形式で日付を含めたパスにした上で、後述するレビューの際にその旨を相談するようにしてください。

次に示すようにいくつかルールがありますが、これらのルールに則っていない場合はCIのチェックで気づけるためそれほど神経質になる必要はありません。

ファイルの内容は原則としてCommonMarkのMarkdown形式で、ファイルのエンコーディングはUTF-8、改行コードLFで記述します。このとき、いくつか注意点があります。

* 記事のタイトルはレベル1の見出しではなく、ファイル先頭に記述する[前付](https://jekyllrb.com/docs/front-matter/)[^front-matter]の一部として記述します。
  前付は、YAML形式で以下の要領で記述します。
  
  ```yaml
  ---
  title: (記事のタイトル)
  author: (執筆者の識別子)
  tags:
  - (分類の識別子1)
  - (分類の識別子2)
  ...
  - (分類の識別子N)
  ---
  ```
  
  例えば、[2021年3月3日の記事]({% post_url 2021-03-03-index %})の場合は以下のようになっています。
  
  ```yaml
  ---
  title: OSSの法人向け技術サポート業務の中で行った、Thunderbirdへのフィードバック事例
  author: piro_or
  tags:
  - mozilla
  - feedback
  ---
  ```
  
  * 執筆者の識別子は、[`_config.yml`の`author_labels`に列挙されている物](https://gitlab.com/clear-code/website/-/blob/5afdb943b9536436b5e439cb3cca5dee9c75746e/_config.yml#L56)を記述します。
    * 初めて記事を執筆するときは、自分の識別子を`_config.yml`に追加する変更も併せて行って、後述するマージリクエストに含めるようにしてください。
  * 記事の分類の識別子は、[`_config.yml`の`tags`と`tag_labels`に列挙されている物](https://gitlab.com/clear-code/website/-/blob/5afdb943b9536436b5e439cb3cca5dee9c75746e/_config.yml#L17)を記述します。
    * 既存の分類で適切な物が無い場合は、こんな分類を足したいがどうか？ということを、後述のレビュー段階で相談してみてください。
    * 了承を得られたら、`_config.yml`の`tags`と`tag_labels`にタグの情報を追加し、同時に、`blog/（タグ名）/index.html`の位置にそのタグの記事の一覧用ページも追加してください。（例：[`clear-code`タグの記事一覧ページ](https://gitlab.com/clear-code/website/-/blob/596a4568fea5075cbbaa2ff26f127db5d9bb802d/blog/clear-code/index.html)）
* 記事本文は、序文にあたる部分と本論にあたる部分の間に `<!--more-->` というマークを必ず入れてください。
  * 記事一覧では、このマークより前の部分のみが出力されます。このマーク以降の部分は省略されて、代わりに「もっと読む」のリンクが出力されます。
* 記事本文内の見出しは、レベル3が最上位です。
* ククログ内の過去記事を参照する場合、URLは公開記事の `https://www.clear-code.com/blog/...` ではなく、ブログ記事を参照する記法で `{% raw %}{% post_url YYYY-MM-DD-識別子 %}{% endraw %}` のように書いてください。例えば、[2021年3月3日の記事]({% post_url 2021-03-03-index %})の場合は `{% raw %}{% post_url 2021-03-03-index %}{% endraw %}` となります[^identifier]。
  * Markdownのリンク記法を使う場合は、`{% raw %}[リンクテキスト]({% post_url YYYY-MM-DD-識別子 %}){% endraw %}` のようになります。
  * レビューが進行中の別の記事など、存在しない日付の記事を参照するとエラーになるので、その場合は一時的に `/` などを仮のリンク先に指定して回避してください。
<!--
* サイト内にある画像ファイルを参照する場合、URLは公開用の `https://www.clear-code.com/...` ではなく、内部リソースを参照する記法で `{% raw %}{% link /path/to/file.png %}{% endraw %}` のように書いてください。例えば、[この記事に埋め込まれている1つ目の画像]({% link /images/blog/how-to-write-article/fork-button.png %})の場合は `{% raw %}{% link /images/blog/how-to-write-article/fork-button.png %}{% endraw %}` となります[^identifier]。
  * Markdownのリンク記法を使う場合は、`{% raw %}[リンクテキスト]({% link /path/to/file.png %}){% endraw %}` のようになります。
-->
* ククログ記事以外のサイト内のページにリンクする場合は、ステージング環境でリンクが自動的に解決されるよう、`{% raw %}{% link ～ %}{% endraw %}` を使って `{% raw %}[リンクテキスト]({% link path/to/file %}){% endraw %}` のようにします。（たとえば、[サービスのトップページ]({% link services/index.md %})であれば `{% raw %}[サービス]({% link services/index.md %}){% endraw %}` です。）
  * **2021年11月9日時点の注記：** ただし、[ククログのトップページ]({{ "blog/" | relative_url }})だけはこの方法でリンクするとエラーになります[^link-to-blog-top-problem]。代替として、 `{% raw %}[リンクテキスト]({{ "blog/" | relative_url }}){% endraw %}` と書く必要があります。
* 脚注はQiitaなどと同様にGitHub Flavored Markdown形式で記述します。脚注への参照を入れたい箇所に `[^脚注の識別子]` と書き、本文の別の場所（お薦めは参照を挿入した段落の直後）に `[^脚注の識別子]: 脚注の内容` と書くと、自動的に脚注に変換されます。例えば以下の要領です。
  
  ```markdown
  パッチは無事Thunderbird開発版[^daily]にマージされました。
  
  [^daily]: 次期メジャーリリースに向けてのブランチ。Dailyとも呼ばれる。
  ```
  
  なお、[MarkdownをHTMLに変換するために使用しているライブラリの既知の不具合](https://github.com/github/cmark-gfm/issues/121)のために、識別子に `w` （小文字のアルファベットのW）が含まれると、脚注が脚注として表示されない結果となります。この不具合を回避するためには、`w` を含まない識別子とするか、もしくは `uu` のように別の文字で置き換えるかする必要があります。
* 記事に画像を埋め込みたい場合は、`images/blog/(記事の識別子)/` に画像ファイルを追加した上で、本文に `{% raw %}![代替テキスト]({% link /images/blog/(記事の識別子)/画像のファイル名 %} "ツールチップとして表示されるテキスト"){% endraw %}` という書式で埋め込み指示を記述します。
  * 記事の幅を超えるサイズの画像は、自動的に縮小表示されます。
  * 画像をクリックしたらフルサイズの画像に遷移するようにしたい場合は、これにリンク記法を組み合わせて `{% raw %}[![代替テキスト]({% link /images/blog/(記事の識別子)/画像のファイル名 %} "ツールチップとして表示されるテキスト")]({% link /images/blog/(記事の識別子)/画像のファイル名 %}){% endraw %}` と記述してください。
* [Rabbitで作成したスライド](https://slide.rabbit-shocker.org/)の埋め込み用HTMLコードを貼り付ける場合は、レイアウト乱れを防ぐために、埋め込み用コード全体を `<div class="rabbit-slide">～</div>` で囲って下さい。

[^front-matter]: `---` から `---` までの部分。
[^link-to-blog-top-problem]: Jekyllのpaginationプラグインの未解決の不具合のようです。本来は `{% raw %}[リンクテキスト]({% link blog/index.html %}){% endraw %}` と記述して問題なく処理されることが期待されます。
[^identifier]: Jekyllへの移行前の記事や、それ以後に書かれた記事の一部は、運用の都合で識別子が `index` になっています。

### プレビューの確認手順

プレビューはローカルで確認するかGitLab Pagesで確認します。GitLab Pagesで確認する場合は `https://(執筆者のアカウント).gitlab.io/website/` 以下に各ブランチ用のプレビューのリンクがあるのでそこから執筆中の記事の内容を確認してください。たとえば、 `clear-code` アカウントのGitLab Pagesでのプレビューは https://clear-code.gitlab.io/website/ で確認できます。

ローカルでのWebブラウザを使ったプレビューには、Jekyllを使用します。RubyとBundlerが利用できる状態で、以下のようにコマンドを実行してください。

```console
$ cd ~/clear-code-website
$ bundle install # これによりJekyllがインストールされる。2回目以降は省略してよい。
$ bundle exec jekyll serve --drafts --livereload --incremental --force-polling
```

これによりJekyllのローカルサーバーが起動し、既存ファイルのパース処理が始まり、以下のようなメッセージが出力されます。

```text
Configuration file: .../website/_config.yml
            Source: .../website
       Destination: .../website/_site
 Incremental build: enabled
      Generating...
         AutoPages: Disabled/Not configured in site.config.
        Pagination: Complete, processed 18 pagination page(s)
```

なお、初期状態では下書きの記事は表示されないため、Jekyll起動時には必ず `--drafts` オプションを指定します。筆者はさらに、[Windows 10のWSL1上で動作させた際にサーバーがフリーズしてしまう問題](https://talk.jekyllrb.com/t/jekyll-serve-hangs-under-wsl-requires-restart-to-kill-process/5424)を回避するため `--force-polling` オプションも指定しています[^force-polling]。

[^force-polling]: こうしないと、Rubyのプロセスがハングアップしてしまい、Windows自体を再起動するまでサーバーを停止できなくなってしまいます。

しばらく待つと、以下の通り続きのメッセージが出力されます。

```text
                    done in 96.128 seconds.
                    Auto-regeneration may not work on some Windows versions.
                    Please see: https://github.com/Microsoft/BashOnWindows/issues/216
                    If it does not work, please upgrade Bash on Windows or run Jekyll with --no-watch.
 Auto-regeneration: enabled for '.../website'
LiveReload address: http://127.0.0.1:35729
    Server address: http://127.0.0.1:4000
  Server running... press ctrl-c to stop.
```

メッセージで示されているとおり、この状態で `http://127.0.0.1:4000` をWebブラウザで開けば、Webサイトのプレビューを閲覧できます。「ブログ」のリンクから辿れる記事一覧では、下書き状態の記事がファイルの日付の記事として表示されています。誤記がないか、外部の記事を参照するリンクがリンク切れになっていないか、などを確認して、問題がなくなるまで記事本文を修正してください。`--livereload` と `--incremental` を指定することで、Jekyllがファイルの変更を検出して自動的にページを再生成し、Webブラウザ上でページを開いていた場合は即座に再読み込みまで行ってくれます。

記事の編集が完了したら、Jekyllのローカルサーバーを終了しましょう。メッセージに出力されているとおり、キーボードショートカットのCtrl-Cで終了できます。

### マージリクエストとレビュー

記事のファイルを作業ブランチにコミットし、gitlab.com上にpushします。例えば、BashでGitを使用している場面であれば以下の要領です。

```console
$ cd ~/clear-code-website
$ git add config.yml # 自分の識別子をauthor_labelsに追加した場合
$ git add _drafs/how-to-write-article.md # 記事本文の追加
$ git add images/how-to-write-article/ # 参照画像の追加
$ git commit -m 'blog: Add article to describe how to write articles'
$ git push origin blog-how-to-write-article # 作業ブランチ名を明示する
```

変更をコミットする際は、コミットメッセージの先頭に `blog:` というプレフィクスを付けるようにしてください。コミットメッセージ全体としては、例えば「blog: ◯×の使い方の記事を追加」のような要領にするとよいでしょう。

作業ブランチの変更をpushした状態でgitlab.com上のプロジェクトページを訪問すると、ページ最上部にマージリクエスト[^merge-request]の作成を促すメッセージが表示されます。

[^merge-request]: GitHubでいう「プルリクエスト」のこと。

[![（スクリーンショット：マージリクエストを作成できる状態）]({% link /images/blog/how-to-write-article/ready-to-create-merge-request.png %} "スクリーンショット：マージリクエストを作成できる状態")]({% link /images/blog/how-to-write-article/ready-to-create-merge-request.png %})

ここで「Create merge request」ボタンをクリックすると、イシュー登録時と同様のUIを持つ、マージリストの作成画面に遷移します。

[![（スクリーンショット：マージリクエスト作成画面）]({% link /images/blog/how-to-write-article/creating-merge-request.png %} "スクリーンショット：マージリクエスト作成画面")]({% link /images/blog/how-to-write-article/creating-merge-request.png %})

コミット時のメッセージが適切に付けられているのであれば、タイトルや説明は特に凝った物を書く必要は無いでしょう。不安があれば、より分かりやすくなるように説明を書き添えても問題ありません。「Submit merge request」ボタンをクリックすると、マージリクエストの作成は完了です。実際の[この記事のマージリクエスト](https://gitlab.com/clear-code/website/-/merge_requests/23)も参照してください。

マージリクエストに対するレビュー[^revieuu]結果のフィードバックは、マージリクエストへのインラインのコメントとして得られます。指摘箇所を適宜修正した上で、作業用ブランチに変更をコミットし、gitlab.comのリポジトリへpushしてください。修正結果は、作成済みのマージリクエストに自動的に反映されます。

[^revieuu]: 基本的にはレビュアー側からアクションを取る想定です。もし数営業日以上経過しても反応がない場合、何らかの理由で見落とされている可能性がありますので、お手数ですがコメントや別チャンネルでのメンションなどで連絡を取ってみてください。

すべての指摘事項が解決されたら、クリアコード内でコミット権を持っているユーザーによって、マージリクエストが処理されるのを待ちましょう。マージが完了したら、記事執筆者の作業は終わりです。お疲れ様でした！

### マージの流れ

マージはレビュアーが行います。
具体的な手順は[READMEに記載があります](https://gitlab.com/clear-code/website#ククログのpublish)。

### まとめ

社内向けの案内を兼ねて、このブログへの記事の投稿手順を解説しました。

今後は、クリアコードでセミナーを実施した後のレポートを参加者の方に書いて頂いたり、クリアコードの業務中での自由なソフトウェアの開発にご協力を頂いた社外の方にそのソフトウェアの紹介記事を書いて頂いたり、といった形でこのブログをより活用していければ幸いです。
