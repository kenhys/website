---
tags:
- cutter
title: CutterをWindowsでgrowlnotifyと一緒に使う方法
---
C/C++に対応しているテスティングフレームワークの一つに[Cutter](/software/cutter.html)があります。今回はテスト結果の通知という観点からCutterの紹介をします。
<!--more-->


Cutterそのものについては何度かククログでもとりあげています。
Cutterの特徴について知りたい方は[Cutterの機能](http://cutter.sourceforge.net/reference/ja/features.html)を参照してください。

  * [Cutter導入事例: Senna (1)]({% post_url 2008-07-25-index %})
  * [Cutter導入事例: Senna (2)]({% post_url 2008-08-25-index %})
  * [C言語用単体テストフレームワークCutterへのHTTPテスト機能追加]({% post_url 2009-08-21-index %})
  * [SoupCutter で全文検索エンジンgroongaのHTTPインターフェースのテスト作成]({% post_url 2009-08-26-index %})
  * [C++用xUnitでのテストの書き方]({% post_url 2009-11-07-index %})

### テスト結果の通知

Cutterに限らず、テスティングフレームワークではテスト結果の通知方法を工夫しています。例えば、コンソールに色分けして結果を表示したり、HTML形式のレポートを生成したりします。

Cutterでもそのようなテスト結果の通知方法の工夫の一つとして、[TDDきのたん]({% post_url 2011-02-09-index %})によるテスト結果の通知をサポートしています。

せっかくテストを書いても、それが頻繁に実行されなかったり、実行した結果を確認して修正する継続的なサイクルが維持されなければ意味がありません。テストが充実するのに伴って、時間のかかるテストの実行中に他の作業をするということも増えてきます[^0]。他の作業をしている間にテストが失敗したら、まずはそのテストをパスするための作業に取り組む必要があります。しかし、他の作業をしていると失敗を見逃してしまうこともあります。他の作業をしていてもテストの失敗に素早く気づける仕組みが必要です。

そのため、Cutterではテスト結果を「通知」する仕組みを取り入れています。

#### TDDきのたんによるテスト結果の通知

CutterではTDDきのたんでテスト結果を通知します。GNOMEやMax OS Xでは以下のように通知されます。

![GNOME上での通知]({{ "/images/blog/20121212_0.png" | relative_url }} "GNOME上での通知")

![Mac OS X上での通知]({{ "/images/blog/20121212_1.png" | relative_url }} "Mac OS X上での通知")

Ubuntuではnotify-osdもしくはnotification-daemonをインストールすれば、TDDきのたんを利用してテスト結果を通知できます。TDDきのたんによるテスト結果の通知は1.1.6からサポートされています。

### Windowsでもテスト結果を通知する

前項ではTDDきのたんでどのようにテスト結果が通知されるかを紹介しました。

CutterはWindowsでも利用することができますが、TDDきのたんによるテスト結果の通知はできませんでした。

Cutter 1.2.1からWindowsでもTDDきのたんによるテスト結果の通知ができるようになりました。なお、この機能を利用するには[Growl for Windows](http://www.growlforwindows.com/gfw/)と[growlnotify](http://www.growlforwindows.com/gfw/help/growlnotify.aspx)が必要です。

#### Growl for Windowsとgrowlnotifyについて

Windowsでも通知系のアプリケーションがあり、Ubuntuでのnotify-osdやnotification-daemonに相当するものがGrowl for Windowsで、notify-sendに相当するのがgrowlnotifyです。

Growl for Windowsとgrowlnotifyをセットアップすれば、テスト結果を通知することができます。

### セットアップ

Windowsでテスト結果を通知できるようにするための環境構築手順を紹介します。

CutterのWindows向けのバイナリは提供されていないため、ソースコードからコンパイルしてインストールする必要があります。インストール方法は以下の通りです。

  1. Growl for Windowsをインストールする

  1. growlnotifyをインストールする

  1. MinGWをインストールする

  1. GTK+ Windowsバイナリをインストールする

  1. intltoolをインストールする

  1. Cutter 1.2.2をインストールする


#### Growl for Windowsをインストールする

[Growl for Windowsのインストーラーをダウンロード](http://www.growlforwindows.com/gfw/d.ashx?f=GrowlInstaller.exe)、インストールします。

インストールしたら、スタートメニューのGrowlアイコンをクリックしGrowlを起動します。

#### growlnotify.exeをインストールする

[growlnotify.exeが含まれているzipをダウンロード](http://www.growlforwindows.com/gfw/d.ashx?f=growlnotify.zip)します。

ダウンロードしたzipアーカイブにはgrowlnotify.exeとgrowlnotify.comの2つが含まれています。以降の説明ではc:\WinApp\growlnotify以下にzipアーカイブに含まれていたバイナリが配置されているものとします。

実際にコマンドプロンプト[^1]を起動し、growlnotifyコマンドが正常に動作することを確認します。

{% raw %}
```
c:\Users\ユーザー名>c:\WinApp\growlnotify\growlnotify.exe /t:title body
```
{% endraw %}

![growlnotify動作確認結果]({{ "/images/blog/20121212_2.png" | relative_url }} "growlnotify動作確認結果")

正しくセットアップできていれば、スクリーンショットのように「title」と「body」を含んだ通知が表示されます。

#### MinGWをインストールする

CutterをビルドするためのツールはMinGWのものを利用します。ここではネットワーク経由でのインストールを行うことのできるインストーラー[mingw-get-inst-20120426.exe](http://sourceforge.net/projects/mingw/files/Installer/mingw-get-inst/mingw-get-inst-20120426/mingw-get-inst-20120426.exe/download)を使ってインストールします。

インストールの途中にインストールするコンポーネントを選択する箇所があります。そこでは以下を選択します。

  * C Compiler
  * C++ Compiler
  * MSYS Basic System
  * MinGW Developer Toolkit

以降の説明ではc:\MinGWへインストールしたものとして説明します。

正しくインストールできていれば、c:\MinGW\msys\1.0\msys.batがインストールされています。msys.batを実行すると以下のようなウィンドウが表示されます。このウィンドウが表示されれば正しくインストールできています。

![msys.bat動作確認結果]({{ "/images/blog/20121212_6.png" | relative_url }} "msys.bat動作確認結果")

#### GTK+ Windowsバイナリをインストールする

Cutterの動作にはGLib 2.16以降が必要です。そのため、GLibのWindowsバイナリをインストールします。

[The GTK+ Project](http://www.gtk.org/)が配布しているGTK+のWindowsバイナリにGLibも含まれているので、これを使います。[all-in-one bundle](http://ftp.gnome.org/pub/gnome/binaries/win32/gtk+/2.24/gtk+-bundle_2.24.10-20120208_win32.zip)をダウンロードして、c:\WinApp\GTKへ展開します。

c:\MinGW\msys\1.0\msys.batを実行し、シェルを起動します。以降シェルでの操作はプロンプトに$をつけて説明します。

シェル上で以下のコマンドを実行します。GTK+のデモアプリケーションが起動したら正常にインストールできています。

{% raw %}
```
$ /c/WinApp/GTK/bin/gtk-demo.exe
```
{% endraw %}

#### intltoolをインストールする

Cutterをビルドする際にはconfigureのチェックで[intltool](http://freedesktop.org/wiki/Software/intltool)が古いとエラーになる[^2]ため、あらかじめ最新のintltoolをインストールします。依存するPerlのモジュールについてはMinGWをインストールした時点で一緒にインストールされています。

intltoolのサイトから[intltool-0.50.2.tar.gz](https://launchpad.net/intltool/trunk/0.50.2/+download/intltool-0.50.2.tar.gz)をダウンロードします。

あとは通常の手順でコンパイルおよびインストールします。

{% raw %}
```
$ tar xvf intltool-0.50.2.tar.gz
$ cd intltool-0.50.2
$ ./configure --prefix=/c/WinApp/GTK
$ make
$ make install
```
{% endraw %}

正しくイントールできていれば、以下のコマンドを実行するとバージョンが表示されます。

{% raw %}
```
$ /c/WinApp/GTK/bin/intltoolize --version
intltoolize (GNU intltool) 0.50.2
```
{% endraw %}

#### Cutter 1.2.2をインストールする

Cutterの最新リリース版のソースコードのダウンロードと展開には以下のコマンドを実行します。

{% raw %}
```
$ mingw-get install msys-wget
$ wget http://downloads.sourceforge.net/cutter/cutter-1.2.2.tar.gz
$ tar xvf cutter-1.2.2.tar.gz
```
{% endraw %}

GTK+関連で必要な環境変数の設定を行います。

{% raw %}
```
$ export PATH=$PATH:/c/WinApp/GTK/bin
$ export PKG_CONFIG_PATH=/c/WinApp/GTK/lib/pkgconfig
```
{% endraw %}

pkg-configコマンドが実行できることを確認します。[^3]

{% raw %}
```
$ pkg-config --version
0.26
```
{% endraw %}

展開したソースコードのディレクトリへと移動し、インストールします。

{% raw %}
```
$ cd cutter-1.2.2
$ ./configure --prefix=/c/WinApp/GTK
$ make
$ make install
```
{% endraw %}

正しくインストールできていれば、Cutterのバージョンが表示されます。

{% raw %}
```
$ cutter --version
1.2.2
```
{% endraw %}

Cutterをインストールできたので、実際に小さなサンプルプログラムを用意します。

{% raw %}
```c
#include <cutter.h>

CUT_EXPORT void
test_sample(void)
{
  cut_assert_true(FALSE);
}
```
{% endraw %}

これは必ず失敗するテストです。

このサンプルを以下のようにしてコンパイルします。

{% raw %}
```
$ gcc -shared -o sample.dll sample.c `pkg-config --cflags --libs cutter`
```
{% endraw %}

以下のコマンドで実際にテストを実行します。

{% raw %}
```
$ cutter .

F
===============================================================================
Failure: test_sample
expected: <FALSE> is TRUE value
test.c:5: test_sample(): cut_assert_true(0, )
===============================================================================

Finished in 0.109200 seconds (total: 0.000000 seconds)

1 test(s), 0 assertion(s), 1 failure(s), 0 error(s), 0 pending(s), 0 omission(s), 0 notification(s)
0% passed
```
{% endraw %}

上記のようにテストが失敗します。

### growlnotifyでテスト結果を通知する

テスト結果をgrowlnotifyで通知するにはgrowlnotifyコマンドへのパスを通してから、cutterコマンドを実行します。

{% raw %}
```
$ export PATH=$PATH:/c/WinApp/growlnotify
$ cutter .
```
{% endraw %}

![テスト失敗時の通知結果]({{ "/images/blog/20121212_3.png" | relative_url }} "テスト失敗時の通知結果")

失敗したことが通知されます。

先程のサンプルプログラムで、FALSEとなっていたところをTRUEへと修正し、必ずパスするようにします。

{% raw %}
```c
#include <cutter.h>

CUT_EXPORT void
test_sample(void)
{
  cut_assert_true(TRUE);
}
```
{% endraw %}

再度コンパイルし、cutterコマンドを実行します。

{% raw %}
```
$ gcc -shared -o sample.dll sample.c `pkg-config --cflags --libs cutter`
$ cutter .
```
{% endraw %}

![テスト成功時の通知結果]({{ "/images/blog/20121212_4.png" | relative_url }} "テスト成功時の通知結果")

成功したことが通知されます。

これで、growlnotifyを使ってCutterのテスト結果を通知することができるようになりました。

最初にテストを書いておくと、実装が終わった時点で通知がグリーンに変わるので、テストがうまくいったことがわかりやすいです。

growlnotifyでの通知を抑制する場合には、以下のように--notify=noオプションを指定します。

{% raw %}
```
$ cutter . --notify=no
```
{% endraw %}

なお、Cutter自身のテストはCutterで書かれているため、CutterをCutterでテストできます。

その場合、cutter-1.2.2ディレクトリにて以下のコマンドを実行します[^4]。

{% raw %}
```
$ make check
...(途中略)...
Finished in 22.851902 seconds (total: 21.826769 seconds)

555 test(s), 2352 assertion(s), 45 failure(s), 2 error(s), 0 pending(s), 9 omission(s), 0 notification(s)
91.5315% passed
FAIL: run-test.sh
```
{% endraw %}

![Cutterをテストしたときの通知結果]({{ "/images/blog/20121212_5.png" | relative_url }} "Cutterをテストしたときの通知結果")

### まとめ

CutterをWindowsでgrowlnotifyと一緒に使う方法を紹介しました。

開発・テストを実行するサイクルに時間がかかる場合は、「通知」によりテストの結果にすぐ気づける仕組みがあると便利です。MinGW環境のビルドは遅いことが多いので、このような「通知」の仕組みをCutterと一緒に導入してみるのをオススメします。

Cutterについて興味がわいてきたら、まずは[チュートリアル](http://cutter.sourceforge.net/reference/ja/tutorial.html)から始めるとよいでしょう。

[^0]: その前に、既存のテストについて「本当にそのテストは必要なのか」を確認するべきです。必要のないテストがあった場合はその削除してテスト時間を短くします。

[^1]: cmd.exe

[^2]: 0.35.0以降が必要。

[^3]: pkg-configはGTK+に含まれています。

[^4]: たくさん失敗していますね。。。
