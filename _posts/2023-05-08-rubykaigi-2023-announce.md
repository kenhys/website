---
tags:
- ruby
- presentation
title: "事前情報：RubyKaigi 2023 - Ruby + ADBC - A single API between Ruby and DBs #rubykaigi"
author: kou
---

[RubyKaigi 2023](https://rubykaigi.org/2023/)で[Ruby + ADBC - A single API between Ruby and DBs](https://rubykaigi.org/2023/presentations/ktou.html)という[ADBC](https://arrow.apache.org/adbc/)の話をする須藤です。RubyKaigi 2023での私の話をより理解できるようになるために簡単に内容を紹介します。2023-04-27に開催されたSMSさん主催の[RubyKaigi 2023 予習イベント ～推しトーク紹介～](https://sms-tech.connpass.com/event/281969/)でもちょっと紹介しました。

なお、[クリアコードはシルバースポンサーとしてRubyKaigi 2023を応援](https://rubykaigi.org/2023/sponsors/#sponsor-436)しています。

<!--more-->

<div class="rabbit-slide rabbit-slide-wide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2023/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2023/" title="Ruby + ADBC - A single API between Ruby and DBs">Ruby + ADBC - A single API between Ruby and DBs</a>
  </div>
</div>

関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2023/)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-rubykaigi-2023)

### 背景

私はRubyが好きなのでデータ処理をするときもできるだけRubyを使いたいです。そのためにはRuby用のデータ処理ツールが必要です。ということで、Ruby用のデータ処理ツールを整備するプロジェクト[Red Data Tools](https://red-data-tools.github.io/ja/)を始めました。2016年ころの話です。

Red Data Toolsはいろいろ便利なデータ処理ツールを提供してきました。よくあるデータセットを共通のAPIでダウンロードできる[Red Datasets](https://github.com/red-data-tools/red-datasets)、可視化ツール[Charty](https://github.com/red-data-tools/charty)、データ処理プラットフォームApache ArrowのRubyバインディング[Red Arrowたち](https://github.com/apache/arrow/tree/main/ruby)、Red Arrowベースのデータフレーム[RedAmber](https://github.com/red-data-tools/red_amber)などです。

まだ十分ではありませんが、Rubyでもできることが着々と増えてきています。RubyKaigi 2023では最近はどういうことができるようになったかを紹介して、Rubyでデータ処理をしたい！という人を増やしたり、Ruby用のデータ処理ツールを開発したい！という人を増やしたいです。

### 内容

RubyKaigi 2023では、Red Data Toolsからは私と[@heronshoes](https://github.com/heronshoes)がRed Data Tools関連プロダクトを紹介します。私はADBCで、@heronshoesはデータフレームRedAmberです。

ADBCは*A*rrow *D*ata*b*ase *C*onnectivityの略で、ODBC（*O*pen *D*ata*b*ase *C*onnectivity）と同じような名前をつけたプロダクトです。ODBCと同じように各種データベースに同じAPIでアクセスできるようにするプロダクトです。以下は[ADBCの紹介記事（英語）](https://arrow.apache.org/blog/2023/01/05/introducing-arrow-adbc/)で使われている説明図です。「各種データベースに同じAPIでアクセスできる」ということがどういうことかわかりますね。

![ADBCのフロー]({% link images/blog/rubykaigi-2023-announce/ADBCFlow2.svg %})

<small>Apache-2.0 © 2016-2023 The Apache Software Foundation</small>

ADBCの重要な点は次の通りです。

* 「同じAPI」で各種データベースに接続できる（データベースが変わっても同じコードを使える）
  * [Active Record](https://rubygems.org/gems/activerecord)や[Sequel](https://sequel.jeremyevans.net/)もそうですね
* 大量データに最適化
* 分析用データに最適化

大量の分析対象のデータをデータベースから出し入れするときに便利で速いということです。それ以外の用途では既存の他のアプローチが向いています。つまり、ADBCはすべてのデータベース接続を置き換えるものではなく、既存のアプローチと補完関係になるものです。

RubyはRuby on RailsでWebアプリケーションを作る用途に使われることが多いです。その用途では、一度に扱うレコード数は1つ（CRUD）か多くても数10から100（検索結果のリストなど）程度です。ADBCが得意としている「大量データ」は（データの内容にもよりますが）少なくとも10万レコード以上です。たとえば、毎日のバッチ処理で普段のデータベース内のレコードを加工して分析用のデータベースに移動するような用途で扱うような規模のデータだとADBCのよさが活きてきます。

このような用途では従来は非常に複雑なSQLになってもSQLでがんばることが多かったです。もちろん、ADBCを使う場合でもできるだけSQLでがんばる方が速い（データに近いところで処理したほうが速い）ですが、SQLでは苦手な処理はデータを取得してインメモリーで処理するというアプローチでも性能を出せるようになります。どうしてそんなことができるのか、という話はRubyKaigi 2023で説明します。

ADBCは「同じAPI」で各種データベースと接続できるようにするので、各種データベースの違いはADBCのレイヤーで吸収することになります。つまり、ADBCがより便利になるためにはADBCの各種データベース対応を進める必要があります。

そのADBCの各種データベース対応の一つとして私は[Apache Arrow Flight SQL adapter for PostgreSQL](https://github.com/apache/arrow-flight-sql-postgresql)というプロダクトを開発しています。Apache Arrow Flight SQLというプロトコルでPostgreSQLにアクセスできるようにするプロダクトです。WebアプリケーションのバックエンドとしてPostgreSQLも広く使われていますが、このプロダクトが実用的になれば、ADBCを使ってPostgreSQLから高速に大量データを取り込んだり取り出したりできるようになります。

Rubyとは関係なくなってしまうのであまり時間をとって紹介しない予定ですが、これの実現方法が面白いんですよ！

PostgreSQLには拡張機能という仕組みがあり、PostgreSQLそのものを変更しなくてもPostgreSQLを拡張できます。しかし、同様の仕組みがあるMySQLとは違ってネットワーク周りを拡張（MySQLはmemcachedプロトコルやXプロトコルをこの仕組みで後付けしている）する仕組みはありません。しかし、このプロダクトは既存の拡張機能の仕組みで実現しています。どうやっているのか気になりますよね！気になる人は私の話を聞きに来たり、会場や懇親会などで直接私に聞いてください。紙あるいはホワイトボードがあれば説明できます。

自分もRuby用のデータ処理ツールを開発したい！という人は[Red Data Tools](https://red-data-tools.github.io/ja/)の[チャット](https://gitter.im/red-data-tools/ja)に来てください。一緒に整備していきましょう！そういう人が増えたら私のRubyKaigi 2023は成功です。

### まとめ

RubyKaigi 2023でADBCの話をします。[Red Data Tools](https://red-data-tools.github.io/ja/)の仲間が増えるといいな。

クリアコードはRubyKaigi 2023をスポンサーとして応援します。
