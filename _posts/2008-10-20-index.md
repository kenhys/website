---
tags:
- ruby
title: Rabbit 0.5.8リリース
---
[Ruby-GNOME2](http://ruby-gnome2.sourceforge.jp/ja/)を使って実装されているプレゼンテーションツール[Rabbit](http://raa.ruby-lang.org/project/rabbit) 0.5.8がリリースされました。
<!--more-->


0.5.8では部分的に[Clutter](http://clutter-project.org/)をサポートしています。

### Clutter

Clutterは高速で、視覚的にリッチで、アニメーションするGUIを作成するためのライブラリです。Clutterのこれらの特徴はOpenGLをバックエンドに使う事で実現されています。
ClutterはLinux/Mac OS X/Windowsなどマルチプラットフォームで動作します。さらに、組み込み環境でも動作し（[OpenGL ES](https://ja.wikipedia.org/wiki/OpenGL+ES)を利用）、デモ動画も[公開](http://www.clutter-project.org/blog/?p=58)されています。

ライブラリを使用する視点で見ると、[GStreamer](https://ja.wikipedia.org/wiki/GStreamer)/[cairo](https://ja.wikipedia.org/wiki/cairo)/[Pango](https://ja.wikipedia.org/wiki/Pango)/[GTK+](https://ja.wikipedia.org/wiki/GTK%2B)など[GNOME](https://ja.wikipedia.org/wiki/GNOME)関連のライブラリと親和性が高いこともあり、便利で使いやすいAPIになっています。

ClutterにはRuby/Python/Perl/Valaなど各種言語用のバインディングがあります。リッチなインターフェイスを作成したい場合にClutterを利用してみてはどうでしょうか。
