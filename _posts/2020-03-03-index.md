---
tags:
- notable-code
title: ノータブルコード4 - NULLよりも名前付きの番兵オブジェクト
---
[Mroonga](https://mroonga.org/ja/)という[MySQL](https://www.mysql.com/jp/)のストレージエンジンを開発している須藤です。MySQLのAPIはよく変わるのでMroongaの開発をしているとMySQLのソースコードを読む機会がよくあります。今日、MySQLのコードを読んでいて「お！」と思うコードがあったので4回目のノータブルコードとして紹介します。
<!--more-->


MySQLは基本的なデータ構造も独自に実装していることが多いです。今日紹介する[`List`](https://github.com/mysql/mysql-server/blob/8.0/sql/sql_list.h)も独自に実装しています。

多くの場合、リストは次のように「要素データへのポインタ」と「次の要素へのポインタ」を持つ構造をつなげて実装します。

```c
struct List {
  void *data;
  List *next;
};
```


そして、リストの終端に`NULL`を置いてリストの終わりを表現します。

```c
if (list->next) {
  printf("have more elements\n");
} else {
  printf("no more elements\n");
}
```


MySQLのリストの実装は`NULL`ではなくリストの終端を示す番兵オブジェクトを使っていました。

```cpp
extern MYSQL_PLUGIN_IMPORT list_node end_of_list;

class base_list {
  inline void empty() {
    elements = 0;
    first = &end_of_list;
    last = &first;
  }
};
```


私が「お！」と思ったのはGDBでデバッグしていたときです。GDBでは`p`で出力したアドレスが既知のグローバル変数や関数などの場合はその名前も出力してくれます。ここで`end_of_list`という名前が出てきたのです。

```text
(gdb) p (((const Item_cond *)select_lex->where_cond())->argument_list()->first->next->next
$1 = (list_node *) 0x555557f77550 <end_of_list>
(gdb) p (list_node *)0x555557f77550
$2 = (list_node *) 0x555557f77550 <end_of_list>
```


`->next`としたら`NULL`が返ってきても「あぁ、ここでリストは終わりなんだな」ということはわかるのですが、`end_of_list`という名前が見えてもたしかにリストが終わりだとことがわかるなと思いました。リストのときは`NULL`で十分だとは思いますが、もう少し複雑なもののときは`NULL`よりもなにか名前が付いた番兵オブジェクトを使うとデバッグが捗るときがあるんじゃないかと思いました。このテクニックを使う機会を見つけることが楽しみです。

今回はMySQLのリスト実装のコードで「お！」と思った名前付きの番兵オブジェクトを紹介しました。みなさんも`NULL`ではなく名前付きの番兵オブジェクトを使った方がよさそうなケースがないか考えてみてください。

ところで、そろそろみなさんも自分が「お！」と思ったコードを「ノータブルコード」として紹介してみたくなってきませんか？ということで、このブログへの寄稿も受け付けることにしました。まだ仕組みは整えていないのですが、とりあえず、 https://gitlab.com/clear-code/blog/issues にMarkdownっぽいマークアップで書いた原稿を投稿してもらえばいい感じに調整してここに載せます。寄稿したいという人がたくさんいるならもう少しちゃんとした仕組みを整えようと思っていますので、興味のある人はご連絡ください。寄稿してもらった記事の著作者は作者本人のままですが、ライセンスは[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)と[GFDL（バージョンなし、変更不可部分なし、表表紙テキストなし、裏表紙テキストなし）](https://www.gnu.org/copyleft/fdl.html)のデュアルライセンスにしてください。参考：[ククログのライセンス](/license/#blog)

それでは、次のノータブルコードをお楽しみに！
