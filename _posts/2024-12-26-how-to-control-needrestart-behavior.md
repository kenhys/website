---
title: debパッケージからneedrestartの挙動を抑制する方法
author: kenhys
tags:
  - debian
---

先日 [fluent-package v5.2.0](https://www.fluentd.org/blog/fluent-package-v5.2.0-has-been-released)をリリースしました。

v5.2.0の注目に値する機能として、対応しているバージョンのパッケージ間でダウンタイムなしのアップグレードが実現できるという点があります。
（つまり、v5.2.0にアップグレードした後、その次のバージョンに改めて更新するときにダウンタイムなしでパッケージを更新できるということです。）

この機能を実現するためには、パッケージ更新時のサービスの挙動を制御し、意図しないタイミングで勝手にサービスがリスタートされないようにする必要がありました。
本記事では、サービスのリスタートに関連するneedrestartの機能をパッケージ側から抑制する方法について説明します。

<!--more-->

### debパッケージにおけるneedrestartの影響について

needrestartとは、パッケージの更新時に再起動が必要なサービスを通知してくれるしくみです。

apt upgradeなどでパッケージを更新しているときに、最後にどのサービスを再起動するかどうか対話的に確認が求められることがありますが、
その機能はneedrestartによって実現されています。[^needrestart]

[^needrestart]: サーバー版でないなど、環境によってはneedrestartパッケージがそもそも標準ではインストールされていないこともあります。

詳細は [第718回needrestartで学ぶパッケージのフック処理](https://gihyo.jp/admin/serial/01/ubuntu-recipe/0718)によくまとめられているので、
そちらを参照するとよいでしょう。

fluent-packageでは、ダウンタイムなしのアップグレードを実現するため、サービスの再起動のタイミングを自前で制御しています。
そのため、意図しないタイミングでサービスの再起動が実行されうるneedrestartのしくみとは相性が悪いです。

ゆえに、fluent-packageの提供するfluentdサービスをneedrestartのチェック対象からはずすようにしています。

具体的には、次のようなファイルを`/etc/needrestart/conf.d/50-fluent-package.conf`として配置しています。[^conf]

[^conf]: https://github.com/fluent/fluent-package-builder/blob/master/fluent-package/templates/etc/needrestart/conf.d/50-fluent-package.conf

```text
# Configuration for needrestart

# Always suppress restarting by needrestart.
$nrconf{blacklist_rc} = [
  qr(^fluentd\.service$)
];
```

このようにすることで、意図しないタイミングでのfluentdサービスの再起動を抑制できます。
抑制できている場合には、パッケージの更新の際に次のように"No services need to be restarted"が出力されます。

```console
After this operation, 17.4 MB of additional disk space will be used.
Get:1 https://packages.treasuredata.com/5/ubuntu/focal focal/contrib amd64 td-agent all 5.2.0-1 [4348 B]
Get:2 https://packages.treasuredata.com/5/ubuntu/focal focal/contrib amd64 fluent-package amd64 5.2.0-1 [17.6 MB]
Fetched 17.6 MB in 3s (6796 kB/s)
...(途中省略)...
No services need to be restarted.

No containers need to be restarted.

No user sessions are running outdated binaries.

Installation completed. Happy Logging!
```

fluent-packageではCIでneedrestartがきちんと抑制できているかも確認しています。
needrestartがインストールされていて、再起動が必要であると判断されたサービスがない扱いとなり上記メッセージがでるのが期待値です。

これには、あらかじめneedrestartがインストールされているか`dpkg-query --show --showformat='${Version}' needrestart`としてチェックし、
パッケージ更新時のログに該当するメッセージが記録されているかを確認しています。[^ci]

[^ci]: https://github.com/fluent/fluent-package-builder/blob/master/fluent-package/apt/systemd-test/update-from-v4.sh#L33-L43

<div class="callout secondary">

これに関連して、Ubuntu 20.04 (focal)の場合に、パッケージがインストールされていなくてもdpkg-queryでバージョンを取得したときに成功するという意外な知見が得られました。
（Ubuntu 22.04やUbuntu 24.04では修正されている）

```console
$ cat /etc/os-release 
NAME="Ubuntu"
VERSION="20.04.6 LTS (Focal Fossa)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 20.04.6 LTS"
VERSION_ID="20.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=focal
UBUNTU_CODENAME=focal
$ dpkg-query --version
Debian dpkg-query package management program query tool version 1.19.7 (amd64).
This is free software; see the GNU General Public License version 2 or
later for copying conditions. There is NO warranty.
$ dpkg -l | grep needrestart
$ dpkg-query --show --showformat='${Version}' needrestart
$ echo $?
0
```

</div>

### rpmパッケージにおけるneedrestartの影響について

rpmの場合のneedrestartの影響はというと、次のようなバグ報告がなされています。

* [Needrestart package missing dependency](https://bugzilla.redhat.com/show_bug.cgi?id=2154293)

AlmaLinux 8/9, Amazon Linux 2/2023などでは、needrestartパッケージがそもそもないか、あるいはパッケージがあっても
依存関係が壊れていてインストールできない状態となっています。
そのため、needrestartの影響については無視してよさそうです。

### さいごに

今回は、サービスのリスタートに関連するneedrestartの機能をパッケージ側から抑制する方法について説明しました。

今回のトピックに関連するv5.2.0のダウンタイムなしのパッケージの更新自体は、fluent-package （通常版）向けに投入された新機能です。
LTS版への投入は、次期メジャーバージョンアップのタイミング（来年夏）を予定しています。

クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。

最新版を使ってみて何か気になる点があれば、ぜひ[GitHub](https://github.com/fluent/fluentd/issues)でコミュニティーにフィードバックをお寄せください。
また、[日本語用Q&A](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)も用意しておりますので、困ったことがあればぜひ質問をお寄せください。
