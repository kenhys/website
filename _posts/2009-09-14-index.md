---
tags:
- ruby
title: るびま0027号
---
週末に[るびま0027号](http://jp.rubyist.net/magazine/?0027)がリリースされました。5周年だそうです。おめでとうございます。
<!--more-->


### ささださんのコメント

n周年のときに毎年ささださんがコメントを書いていますが、今年のささださんのコメントが明るめになっているのが印象的です。「脱ささだ体制」が進んでいる影響でしょうか。

  * [Rubyist Magazine 一周年と、そしてこれから](http://jp.rubyist.net/magazine/?0009-ko1-comment)
  * [Rubyist Magazine 二周年を迎えて](http://jp.rubyist.net/magazine/?0016-ko1-comment)
  * [Rubyist Magazine 3 周年](http://jp.rubyist.net/magazine/?0021-ko1-comment)
  * [Rubyist Magazine 4周年に寄せて](http://jp.rubyist.net/magazine/?0024-4th-ko1)
  * [Rubyist Magazine 五周年](http://jp.rubyist.net/magazine/?0027-ko1-comment) <- イマココ

URLを集めていて気づいたのですが、4周年のときだけ「ko1-comment」ではなく、「4th-ko1」なんですね。

### おすすめ記事

今回のるびまにはActiveLdapの日本語チュートリアルなどで活躍されている[高瀬さん](http://d.hatena.ne.jp/tashen/)の[ActiveLdap を使ってみよう（前編）](http://jp.rubyist.net/magazine/?0027-ActiveLdap)という記事があります。

前編ということだけあり、基本的な部分から丁寧に解説されています。なかでも、前半のLDAPの説明部分はLDAPを知らない方にもわかりやすくまとまっています。ActiveLdapの入門だけではなく、LDAPの入門としても参照しやすい記事です。

ActiveLdapについても便利に使える感じが伝わる記事で、後編が楽しみです。

RubyでLDAPを操作したい場合はぜひ高瀬さんの記事を参考にしてください。

### まとめ

るびま0027号の興味深い記事を2つ紹介しました。

自分でも何かできることはないか、と思っている方は、ただのRubyistであるかずひこさんの[Rubyist にできること](http://jp.rubyist.net/magazine/?0027-WhatWeCanDo)がおすすめです。あるいは、記事の提供や編集でるびまに参加するというのもいかがでしょうか。

まだ[RubyNews](http://jp.rubyist.net/magazine/?0027-RubyNews)を読んだことがない方は一度読んでみてはいかがでしょうか。日本Ruby会議2009の間にたくさんのフリーソフトウェアがリリースされたことがわかります。

今回もここでは紹介しきれないくらい盛りだくさんの[るびま0027号](http://jp.rubyist.net/magazine/?0027)をぜひ読んでみて下さい。
