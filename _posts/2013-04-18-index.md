---
tags:
- ruby
title: Rubyで定義したメソッドに戻り値のYARD用ドキュメントを書く方法
---
### はじめに

YARDというRuby用のドキュメンテーションツールがあります。これまでに2回、YARD用のドキュメントの書き方についてククログで紹介しました。
<!--more-->


  * [Rubyの拡張ライブラリにYARD用のドキュメントを書く方法]({% post_url 2012-10-02-index %})
  * [Rubyで定義したメソッドの引数についてのドキュメントをYARD用に書く方法]({% post_url 2013-04-03-index %})

今回は前回の「[Rubyで定義したメソッドの引数についてのドキュメントをYARD用に書く方法]({% post_url 2013-04-03-index %})」という記事の続きで、Rubyで定義したメソッドに戻り値のYARD用ドキュメントを書く方法を紹介します。

まず、戻り値のYARD用ドキュメントを書くための`@return`タグ[^0]について説明したあと、実際にドキュメントを書くRubyのコードを示します。その後、実際に`@return`タグを使ってドキュメントを書き、YARDのコマンドを使ってHTMLのリファレンスマニュアルを作成します。

なお、YARDについてや、YARDのタグの使い方、HTMLのリファレンスマニュアルの生成の仕方については[前回の記事]({% post_url 2013-04-03-index %})を参照してください。

### `@return`タグについて

まず、`@return`タグについて説明します。`@return`タグはYARDのタグ（コードのメタデータを記述するための記法）の1つで、戻り値についての説明を書くためのタグです。他のタグと同様にコメントの中に書くことでYARDのタグとして認識されます。

`@return`タグの書式は以下の通りです。

{% raw %}
```
@return [戻り値のクラス] 説明
```
{% endraw %}

詳しい書き方は実際に書くときに説明します。では次に、実際にドキュメントを書くRubyのコードを示します。

### 戻り値のドキュメントを書くRubyのコード

戻り値のドキュメントを書くRubyのコードは、[前回の記事]({% post_url 2013-04-03-index %})にて、メソッド全体の説明と引数についての説明を書いたコードを使います。

{% raw %}
```ruby
# Returns Array containing number exceeding limit_age.
# For example, extract_overage([17, 43, 40, 56], 40) #=> [43, 56]
#
# @param ages [Array] numbers checked if they exceeded from limit_age.
# @param limit_age [Integer] limit_age limit used to extract
#   bigger ages than it.
def extract_overage(ages, limit_age=20)
  ages.select do |number|
    number > limit_age
  end
end
```
{% endraw %}

このコードを、以降は「サンプルコード」と呼びます。サンプルコードについての説明を[前回の記事]({% post_url 2013-04-03-index %})から再掲します。

サンプルコードは`extract_overage`メソッドを定義しています。`extract_overage`メソッドは、配列と数値を引数にとり、各要素から数値より大きい数（数値が指定されない場合は20より大きい数）を集めた配列を返します。例えば、`extract_overage([17, 43, 40, 56], 40)`を実行すると、`[43, 56]`を返します。

サンプルコードにはすでに、メソッド全体の説明（コメントの最初から2行目）と、引数についての説明（コメントの4行目から6行目）が書かれています。これは前回の記事で書いたドキュメントをそのまま使っています。メソッド全体の説明と引数全体の説明の書き方については、[前回の記事]({% post_url 2013-04-03-index %})を参照してください。

前回と同様に、この記事ではドキュメントの書いてある場所をわかりやすくするために、シグニチャーの部分だけを抜き出したものを示します。

{% raw %}
```ruby
# Returns Array containing numbers exceeding limit_age.
# For example, extract_overage([17, 43, 40, 56], 40) #=> [43, 56]
#
# @param ages [Array] numbers checked if they exceeded from limit_age.
# @param limit_age [Integer] limit_age limit used to extract
#   bigger ages than it.
def extract_overage(ages, limit_age=20)
end
```
{% endraw %}

なお、この状態でHTMLのリファレンスマニュアルを作成すると次のようになります。

![メソッド全体の説明と引数についての説明が追加されたリファレンスマニュアル]({{ "/images/blog/20130418_0.png" | relative_url }} "メソッド全体の説明と引数についての説明が追加されたリファレンスマニュアル")

メソッド全体の説明（画像の赤枠の部分）と、`@paramタグ`で書かれた説明（画像の緑の枠の部分）がHTMLのリファレンスマニュアルに表示されているのがわかります。今回はこのサンプルコードのシグニチャーに、`@return`タグを使って戻り値についてのドキュメントを書きます。

### `@return`タグの使い方

`@return`タグの書式を再掲します。

{% raw %}
```
@return [戻り値のクラス] 説明
```
{% endraw %}

`@return`タグの書式について説明します。戻り値のクラスのところには、戻り値のオブジェクトのクラスを示す任意の文字列を書きます。説明のところには、文章で戻り値の説明を書きます。例えば、今回の戻り値は配列なので、配列の中身について[^1]説明した文章を書きます。

また、1つのメソッドにつき`@return`タグは複数個書けるので、戻り値の値が変わる場合はその数だけ書くのがよいでしょう。例えば、`Array`クラスの`shift`メソッドは、レシーバー[^2]の配列が空でないときはその先頭の要素を返しますが、空の場合は`nil`を返します。このように戻り値が異なる場合は、戻り値が先頭の要素の時と`nil`の時についてそれぞれ`@return`タグを書きます。そうすると1つ1つの戻り値についての説明が分けて書かれるため読みやすくなります。この時、説明のところにはメソッドがその戻り値を返す条件を書くと、それぞれの戻り値がどんな時に返されるのかがわかりやすくなります。

なお、`@return`タグを書く場所ですが、`@param`タグの下がよいでしょう。なぜかというと、引数の説明の下に戻り値の説明が書いてあるとすんなり読めるからです。メソッドは引数を受け取り、その引数を使って処理をして、戻り値を返します。それと同様に、ドキュメントも引数、戻り値の順に配置しておくと、「引数を元にこのメソッドが戻り値が生成する」とメソッドの処理の流れと同じようにすんなり読めます[^3]。

それでは実際に書いてみます。サンプルコードのシグニチャーに`@return`タグを使ってドキュメントを書いたのが次のコードになります。

{% raw %}
```ruby
# Generates Array containing numbers exceeding limit_age. Numbers is members of ages.
# For example, extract_overage([17, 43, 40, 56], 40) #=> [43, 56]
#
# @param ages [Array] numbers checked if they exceeded from limit_age.
# @param limit_age [Integer] limit_age limit used to extract
#   bigger ages than it.
# @return [Array] Returns Array containing numbers exceeding limit_age.
def extract_overage(ages, limit_age=20)
end
```
{% endraw %}

戻り値は配列なので、戻り値のクラスのところには`Array`を書きます。`@return`タグで戻り値の説明は、メソッド全体の説明から該当する部分を抜き出して書きました。それに伴ってメソッド全体の説明も修正しました。このコードをexample.rbというファイルに保存し、次のコマンドでHTMLのリファレンスマニュアルを生成します。

{% raw %}
```
$ yardoc example.rb
```
{% endraw %}

生成されたHTMLのリファレンスマニュアルは次のようになります。

![戻り値の説明が追加されたリファレンスマニュアル]({{ "/images/blog/20130418_1.png" | relative_url }} "戻り値の説明が追加されたリファレンスマニュアル")

図の赤枠の部分に戻り値についての説明があるのがわかります。また、図の緑色の部分2ヶ所には、`@retrun`タグで書いた戻り値のクラスである`Array`が表示されているのがわかります。

### まとめ

今回は、前回の[引数についてのYARD用ドキュメントを書く方法]({% post_url 2013-04-03-index %})からの続きで、戻り値についてのYARD用ドキュメントを書く方法を説明しました。前回と今回の記事の内容を使うと、メソッドについての基本的な説明をYARDで書くことができます。

次は例を書くために使う`@example`タグについて説明します。

[^0]: 詳細は後述します。

[^1]: 今回のサンプルコードでは、指定したlimit_ageよりも大きい数値を集めた配列であること

[^2]: メソッドの操作対象となるオブジェクト

[^3]: なお、HTMLのリファレンスマニュアルではどちらを先に書いても引数の説明が先に表示されます。
