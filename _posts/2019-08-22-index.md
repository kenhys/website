---
tags:
- mozilla
title: Firefox ESR60からESR68に移行するには
---
2019年7月9日に、Firefox ESR (Extended Support Release) の最新バージョンとなる[Firefox 68.0esr](https://www.mozilla.org/en-US/firefox/68.0esr/releasenotes/)がリリースされました。
<!--more-->


この記事では、最新版への移行を検討されている企業ユーザーの皆様に向けて、ESR68に移行するための主要なスケジュールと、ESR60→ESR68の主な変更点について解説します。

### 移行スケジュール

ESR68に関連する主だったタイムラインを表として整理します（[公式スケジュール表](https://wiki.mozilla.org/Release_Management/Calendar)より整理・抜粋）

<table>
<tr><th></th><th>ESR60</th><th>ESR68</th><th>メモ</th></tr>
<tr><th>2019年05月21日 (火)</th><td>Firefox 60.7 ESR</td><td>-</td><td></td></tr>
<tr><th>2019年07月09日 (火)</th><td>Firefox 60.8 ESR</td><td>Firefox 68.0 ESR</td><td>Firefox 68ESR正式リリース</td></tr>
<tr><th>2019年09月03日 (火)</th><td>Firefox 60.9 ESR</td><td>Firefox 60.1 ESR</td><td></td></tr>
<tr><th>2019年10月22日 (火)</th><td>-</td><td>Firefox 68.2 ESR</td><td>Firefox 60ESRサポート終了</td></tr>
<tr><th>2019年12月10日 (火)</th><td>-</td><td>Firefox 68.3 ESR</td><td></td></tr>
</table>


この表の最も重要なポイントは **2019年10月22日のESR68.2のリリース** です。この日をもって、現行バージョンのESR60のサポートが終了するので、これが実質的なバージョンアップのデッドラインとなります。運用を担当されている方は、この日までに検証を終えて、各端末上でアップデートを実行できるようにする必要があります。

なお、日付はいずれも太平洋標準時基準（PST）です。日本時間では、おおむね当日の深夜帯から翌早朝にかけてのリリースとなります。この時差の問題は、「リリース日に合わせて計画を立てたが、肝心のバージョンが当日の営業時間内にリリースされなかった」というミスにつながるので、段取りを組み立てる際に頭にとめておく必要があります。

### ESR68の主なトピック

ここからは、ESR60からESR68で何が変わったのかという本題に移ります。変更箇所そのものは非常に膨大で、数万単位のコミットが加えられています。弊社の標準的なガイダンスでは、そこから要点を絞って約25項目を議論します。この記事では、そこから組織運用上、影響の大きい11項目をトピック別にお伝えします。

#### Symantec社の証明書がすべて失効します

ESR68からSymantecが発行した証明書は「信頼されない証明書」とみなされるようになります。具体的には、HTTPSでアクセスしようとした場合、警告画面が出現して、先に進めないようになります。

![Symantecの証明書警告]({{ "/images/blog/20190822_2.png" | relative_url }} "Symantecの証明書警告")

対象となる証明書は、Symantecの傘下ブランドのVeriSign・RappidSSL・Thawte・GeoTrustが含まれる点に注意してください。2017年に、Symantecは証明書事業をDigicertに譲渡しているので、この譲渡後に発行された証明書であれば問題ありません。とくに法人環境では社内向けのサイトが古い証明書を使っていないか確認しておく必要があります。

自社のサイトが該当しているか確認したい場合は、[Symantec SSL Checker](https://www.websecurity.symantec.com/support/ssl-checker)というツールでチェックできます。

本件の背景を含めた詳細は、[Mozilla Security Blogの記事](https://blog.mozilla.org/security/2018/03/12/distrust-symantec-tls-certificates/)を参照してください。

#### コンテンツブロック機能の対象が拡張されました

Firefoxにはトラッキングスクリプトをブロックする機能が含まれています。この機能が、ESR68で拡張され、暗号通貨マイニングなどを目的とするスクリプトもブロックできるようになりました。

これによって、設定可能な項目は4項目に増えました。以下に一覧表を示します。

<table>
<tr><th>種別</th><th>説明</th><th>デフォルト設定</th></tr>
<tr>
  <th>トラッカー</th>
  <td>ユーザーの行動を追跡するスクリプト<br>例 - google-analystics.com</td>
  <td>プライベートウィンドウのみブロック</td>
</tr>
<tr>
  <th>トラッキングクッキー</th>
  <td>トラッカーによってセットされるCookie</td>
  <td>ブロックしない</td>
</tr>
<tr>
  <th>暗号通貨マイニング</th>
  <td>ブラウザで暗号通貨をマイニングするスクリプト<br>例 - coinhive.com</td>
  <td>ブロックしない</td>
</tr>
<tr><th>フィンガープリント採取</th><td>ユーザーを一意識別する特殊な追跡スクリプト<br>例 - simility.com</td><td>ブロックしない</td></tr>
</table>


ブロック対象のリストはGitHubのレポジトリ [mozilla-services/shaver-prod-lists](https://github.com/mozilla-services/shavar-prod-lists/) で管理されています。Firefoxは定期的にMozillaのリスト配信サーバーと同期して、リストを取得します。リストの取得状況を確認したい場合は、アドレスバーに「about:url-classifier」と打ち込んでください。

注意が必要な点として、この機能を有効化した場合に、一部のサイトの動作への影響（主に埋め込み動画が再生されなくなる問題）が報告されている事に注意してください。

例) [Bugzilla 1557363 コンテンツブロック有効時にibm.comの動画が再生できない](https://bugzilla.mozilla.org/show_bug.cgi?id=1557363)

互換性を優先するか、安全性を優先するかに応じて、導入する設定を決める必要があります。

#### プライベートウィンドウでのアドオンの実行に許可が必要になります

アドオンがプライベートウィンドウでは原則として実行されなくなります。実行にはユーザーの明示的な許可が必要となり、ユーザーは画面から個別にアドオンを許可できるようになります。

![プライベートウィンドウのアドオン実行]({{ "/images/blog/20190822_0.png" | relative_url }} "プライベートウィンドウのアドオン実行")

この変更は、企業運用でアドオンを一括導入している場合に問題になります。例えば、管理用のアドオンを導入している場合に、ユーザーがプライベートウィンドウを開いたときにアドオンが実行されなくなってしまうためです。

従来の挙動（プライベートウィンドウでも原則としてアドオンを実行する）に戻したい場合は、次の設定を適用して下さい。

```javascript
pref("extensions.allowPrivateBrowsingByDefault", true)
```


#### RSSフィード機能が削除されました

FirefoxからRSSフィード機能（ライブブックマーク機能）が削除されました。従来はFirefoxにRSSフィードリーダーが標準搭載されていましたが、このリーダーがESR68からは使えなくなります。

![RSSフィードリーダー機能]({{ "/images/blog/20190822_4.png" | relative_url }} "RSSフィードリーダー機能")

設定などで従来の挙動に戻すことはできないため、代替としてはウェブベースのリーダーを使うか、RSSフィードに対応したアドオンを導入する必要があります。

なお、ESR68に更新する際に、ライブブックマークのバックアップが自動的に作成されます。具体的には「Firefox feeds backup.opml」というOPML形式のファイルがデスクトップに作成されるので、他のリーダーに移行する場合は、このファイルをインポートしてください。

この削除の背景については、ブログ記事「[Firefox removes core product support for RSS/Atom feeds](https://www.gijsk.com/blog/2018/10/firefox-removes-core-product-support-for-rss-atom-feeds/)」を参照してください。

#### Ctrl-Tabのタブ切替が「最終アクセス順」に変更になりました

ESR68ではCTRL-TAB押下時のタブ切り替えの挙動が大きく変わっています。CTRL-TABを押すと次のようなUIが表示されるようになりました（また、タブの切り替えの順番も変わっています）。

![CTRL-TAB押下時の表示]({{ "/images/blog/20190822_6.png" | relative_url }} "CTRL-TAB押下時の表示")

この変更が適用されるのは、新規にFirefoxをインストールした端末のみです。ESR60がインストールされている環境でバージョン更新を行った場合には、新しいUIは有効化されません（この挙動は設定画面の『Ctrl+Tabで最近使用した順にタブを切り替える』の設定で管理されています）。運用する端末で使い勝手を統一したい場合は、次の設定を導入してください。

```javascript
// falseで従来の挙動、trueで新しいUIを適用する
pref("browser.ctrlTab.recentlyUsedOrder", false);
```


#### ツールバーにFirefox Accountアイコンが追加されました

Firefox Account (Firefox Sync) とは、Mozillaのサーバーにブックマークや履歴を保存できる機能です。
ESR68では、ツールバーにFirefox Account用のアイコンが追加され、ここから機能にアクセスできるようになりました。

![Firefox Accountアイコン]({{ "/images/blog/20190822_1.png" | relative_url }} "Firefox Accountアイコン")

多くの企業では、情報セキュリティの観点からFirefox Account機能自体を無効化している場合が多いと思います。このアイコンを非表示にしたい場合には次の設定を導入します。

```javascript
pref("identity.fxaccounts.toolbar.enabled", false);
```


#### インストールパスごとにユーザープロファイルが新規作成されます

Firefoxのインストールパスごとにプロファイルが新規作成されるようになります。たとえば、次のように2つのFirefoxをインストールしていたとします。

  1. /usr/lib/firefox

  1. /usr/local/lib/firefox

従来、この2つのFirefoxは起動時に同じユーザープロファイルを読み込んでいました。しかし、2つのFirefoxのバージョンが異なる場合に、（Firefoxのプロファイルは前方互換性がないので）バージョンの異なるFirefoxが交互にプロファイルを読み書きする結果、ブックマークの情報などが破損してしまう可能性がありました。

例）[Support 1214384 Firefox 53にバージョンを戻すとブックマークが消失する](https://support.mozilla.org/en-US/questions/1214384)

そこで、Firefox 68 ESRからは(1)と(2)のFirefoxは、起動時に別個のプロファイルを作成するようになりました。これによって、1つの端末に複数のFirefoxをインストールした場合にもプロファイルの破損が起こらなくなりました。

この仕組みの詳細については、[Firefox 67以降のユーザープロファイルの仕様の詳細]({% post_url 2019-06-14-index %})をご参照ください。

#### Windows 10の再起動時に自動起動するようになりました

Windows 10で、セッション終了時にFirefoxを開いていた場合、次の起動時にFirefoxが自動的に起動するようになっています。
一般的なアプリケーションの挙動と揃えた形になりますが、この機能を無効化したい場合は次の設定を導入します。

```javascript
pref("toolkit.winRegisterApplicationRestart", false);
```


#### データ流出を起こしたサイトが検出されるようになりました

Firefoxに「データ流出を起こしたサイトを検知する機能」（Firefox Monitor機能）が導入されました。過去にデータ流出を引き起こしたサイトにアクセスした場合に、次のようなポップアップが表示されます。

![Firefox Monitor機能]({{ "/images/blog/20190822_5.png" | relative_url }} "Firefox Monitor機能")

警告の表示対象となるサイトの一覧は[Firefox Monitorの公式ページ](https://monitor.firefox.com/breaches)で確認できます。本記事の執筆時点で、約300サイトが対象サイトとして登録されています。

この機能を無効化したい場合は、次の設定を導入します。

```javascript
pref("extensions.fxmonitor.enabled", false);
```


#### MSI形式インストーラの提供開始

従来はWindows向けにはexe形式のインストーラのみが提供されていましたが、MSI形式のインストーラが提供されるようになりました。Windows 7以降で利用することができます。

ダウンロード方法などの詳細については[Mozillaの公式ヘルプ](https://support.mozilla.org/en-US/kb/deploy-firefox-msi-installers)を参照ください。

#### 自動更新の停止方法が変更になりました

企業環境では更新タイミングをコントロールするため、Firefoxの自動更新機能を無効化している場合が多いと思います。従来は、この無効化はAutoConfigを通じて実現することができましたが、ESR68からは「Policy Engine」という新しい仕組みの導入が必須になっています。

Policy Engineの詳細は、[Mozilla Firefox ESR60でのPolicy Engineによるポリシー設定の方法と設定項目のまとめ][20180512]を参照ください。自動更新の無効化が運用の前提になっている場合は、こちらの記事のガイドに沿ってPolicy Engineを導入してください。

### まとめ

上述の通り、Firefox 68.0esrには、企業での運用に影響を与える変更が入っています。移行期限まで、あと2ヶ月を切りましたので、計画的に移行をお進め下さい。
