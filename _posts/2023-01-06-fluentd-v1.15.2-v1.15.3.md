---
title: Fluentd v1.15.2 と v1.15.3 のリリースについて
author: daipom
tags:
  - fluentd
---

クリアコードは[Fluentd](http://www.fluentd.org)の開発に参加し、リリースを含めたメンテナンス作業を行っています。

2022年11月02日に、Fluentdの最新版となるv1.15.3をリリースしました。
2022年8月22日にリリースしたv1.15.2と合わせて、その主な修正内容をご紹介します。

* [CHANGELOG](https://github.com/fluent/fluentd/blob/master/CHANGELOG.md#release-v1153---20221102)
* リリースアナウンス
  * [Fluentd v1.15.3](https://www.fluentd.org/blog/fluentd-v1.15.3-has-been-released)
  * [Fluentd v1.15.2](https://www.fluentd.org/blog/fluentd-v1.15.2-has-been-released)

<!--more-->

### `out_file`: `append`オプションに関してv1.15.1で混入した不具合を修正

v1.15.1で、マルチワーカー環境下における[out_fileのappendオプション](https://docs.fluentd.org/output/file#append)について改良を行いました。

v1.15.1より前において、`append`オプションで同じファイルに複数のワーカーが同時に追記を行う場合、ワーカー間で書き込みが競合してファイルが破損する可能性がありました。
その問題を解決するため、ワーカー間の排他機能を提供する仕組みとして、[acquire_worker_lock](https://github.com/fluent/fluentd/blob/v1.15.1/lib/fluent/plugin/base.rb#L79-L90)を実装しました。
これは、ワーカープロセス毎に固有の一時ディレクトリーを用意し、その配下でロックファイルを管理することで、ワーカー間の排他を実現しています。

`out_file`プラグインの`append`オプションは、[この機能を利用することで](https://github.com/fluent/fluentd/blob/v1.15.1/lib/fluent/plugin/out_file.rb#L225-L227
)、マルチワーカー環境下でも安全に追記できるようになりました。
詳しくは以前のリリース記事をご覧ください。

* [Fluentd v1.15.1リリース]({% post_url 2022-08-05-fluentd-v1.15.1 %})

しかし、この修正により以下の2つの問題が発生しており、v1.15.2とv1.15.3でそれぞれ修正を行いました。

#### td-agentなど、`daemon`オプションを用いてfluentdを起動すると正しく動作しない問題

v1.15.1時点の修正が[fluentdのdaemonオプション](https://docs.fluentd.org/deployment/system-config#command-line-options)に対応しておらず、
td-agentなどで正しく動作しない問題がありました。
v1.15.2において修正しました。

ロックファイルの管理に用いるワーカープロセス毎に固有の一時ディレクトリーは、ワーカープロセスの終了時に不要になるので削除します。
本不具合は、`daemon`オプションが有効な場合に、誤って起動時に削除まで行ってしまい、一時ディレクトリーが存在しない状態で動作してしまうことによって引き起こされていました。
ワーカープロセス本体を実行する処理は通常は同期的な処理なのですが、`daemon`オプションが有効な場合には非同期処理になります。
当時この点を考慮漏れしていたため、誤って終了時の削除処理が呼ばれてしまっていました。

興味のある方は次の修正をご覧ください。

* https://github.com/fluent/fluentd/pull/3864

#### `<worker N-M>`の形でマルチワーカーを設定すると、起動に失敗する問題

v1.15.1時点の修正が[\<worker N-M\>のマルチワーカーの設定形式](https://docs.fluentd.org/deployment/multi-process-workers#less-than-worker-n-m-greater-than-directive)に対応しておらず、
この設定を利用している場合に起動に失敗する問題がありました。
v1.15.3において修正しました。

v1.15.1の`out_file`の`append`オプションでは、ロックファイルの管理に用いるワーカープロセス毎に固有の一時ディレクトリーについて、マルチワーカーで動作する場合に起動時に存在を確認する動作になっていました。
しかし、Fluentdは起動前に設定の確認を行うフェーズがあり、この確認処理をその時点でも行ってしまっていました。
起動時に一時ディレクトリーを用意する仕組みであるため、この確認フェーズ時点ではまだ一時ディレクトリーが存在せず、確認処理がエラーとなってしまいます。

ただし、Fluentdの複雑な仕様として、[\<system\>ディレクティブによるマルチワーカーの設定形式](https://docs.fluentd.org/deployment/multi-process-workers#less-than-worker-greater-than-directive)の場合に限り、
この確認フェーズを単一ワーカーとして行うというものがありました。
そのため、`<system>`ディレクティブのみでマルチワーカーを設定しているとこの問題が発生せず、`<worker N-M>`の形で設定をする場合のみ発生していました。

このFluentdの設定確認処理の複雑な仕様については、今後の改善を検討していきたいところです。

興味のある方は次の修正をご覧ください。

* https://github.com/fluent/fluentd/pull/3942

### Windows: `system`ディレクティブでログローテートを有効にすると、ローテートに失敗する問題を修正

ログローテートを有効にするには、元々コマンドラインオプションで指定を行う必要がありました。

* [--log-rotate-age](https://docs.fluentd.org/deployment/system-config#command-line-options)
* [--log-rotate-size](https://docs.fluentd.org/deployment/system-config#command-line-options)

その後v1.13.0で、`system`ディレクティブにおいても設定できるようになりました。

* [rotate_age](https://docs.fluentd.org/deployment/system-config#rotate_age)
* [rotate_size](https://docs.fluentd.org/deployment/system-config#rotate_size)

```xml
<system>
  <log>
    rotate_age 5
  </log>
</system>
```

しかし、この`system`ディレクティブによる設定方法を用いた場合に、Windowsにおいて不具合が発生することが分かりました。

前提として、WindowsにおいてFluentdのログローテートを有効にすると、プロセスごとに分かれたログファイルにログを出力します。

```
fluentd-supervisor-0.log # supervisorプロセスのログファイル
fluentd-0.log # 1つめのworkerプロセスのログファイル
fluentd-1.log # 2つめのworkerプロセスのログファイル
...
```

これは、Windowsにおいて複数のプロセスが1つのファイルを同時にローテートしようとすると、問題が発生するからです。

しかし、`system`ディレクティブの設定でログローテートを有効にした場合は、supervisorプロセスのログを1つめのworkerプロセスのログファイルに出力してしまっていました。

```
fluentd-0.log # 1つめのworkerプロセスのログファイルに、supervisorプロセスのログも出力されていた
fluentd-1.log # 2つめのworkerプロセスのログファイル
...
```

このためログローテート時にエラーが発生することがあります。

v1.15.3で本問題を修正し、supervisorプロセスのログを独立したログファイルに出力するようになりました。

興味のある方は次の修正をご覧ください。

* https://github.com/fluent/fluentd/pull/3939

### `system`: `enable_jit`設定を追加

v1.15.2において、`system`ディレクティブに[enable_jit](https://docs.fluentd.org/deployment/system-config#enable_jit-experimental)設定を追加しました。

```
<system>
    enable_jit true
</system>
```

この設定を有効にすることで、環境のRubyに合ったJIT(MJITやYJIT)が有効化されてワーカープロセスが立ち上がるようになります
（内部では、Rubyの`--jit`オプションを有効にしてワーカープロセスを立ち上げています）。

次の図は、`in_tail`プラグインと`parser_ltsv`で動かした場合のパフォーマンスになります（[この修正のPR](https://github.com/fluent/fluentd/pull/3857)で紹介しています）。
測定環境は Ubuntu 22.04 です。

![Fluentd enable_jit]({% link images/blog/fluentd-v1.15.2-v1.15.3/fluentd-enable-jit.png %})

このように、設定によっては10%~20%ほど高速化することを確認できています。
一方で、この図にはありませんが、`parser_json`や`parser_apache`で動かした場合にはほとんどパフォーマンス向上が見られませんでした。

このように、JITを有効化することでFluentdが高速化するかどうかは、設定や環境次第であると想定され、まだ十分に検証できていない状態です。

また、JITはRuby 3.1以前は実験的な機能と位置づけられているため、デフォルトではオフとなっています[^jit]。

* [YJITが搭載された Ruby 3.1.0 のリリースニュース](https://www.ruby-lang.org/ja/news/2021/12/25/ruby-3-1-0-released/)

もしこの機能を使ってみて、パフォーマンスが向上した、変わらなかった、動作に不具合が発生した、などが分かりましたら、ぜひ[GitHub](https://github.com/fluent/fluentd/issues)でフィードバックをお寄せください！

[^jit]: [2022-12-25 に公開されたRuby 3.2](https://www.ruby-lang.org/ja/news/2022/12/25/ruby-3-2-0-released/)では、JIT(YJIT)が実用段階になりました。

### まとめ

今回の記事では、Fluentd v1.15.2 と v1.15.3 について最新情報をお届けしました。

最新版を使ってみて、何か気になる点があればぜひ[GitHub](https://github.com/fluent/fluentd/issues)で開発チームまでフィードバックをお寄せください！

また、クリアコードはFluentdなどの自由なソフトウェアの開発・サポートを行っております。自分も仕事でFluentdの開発をしたい！という方は、[クリアコードの採用情報]({% link recruitment/index.md %})をぜひご覧ください。

