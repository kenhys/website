---
title: "Debianパッケージの更新によりPCが起動しなくなったときの復旧事例"
author: kenhys
tags:
  - debian
---

### はじめに

Debianには開発コードネームが`sid`とよばれる、いわゆる不安定版(`unstable`)があります。
`sid`の成果がテスト版や将来の安定版へと反映されていくしくみになっています。

不安定版とはいっても、それなりに開発・テストがなされているので、そうそう致命的な問題を踏むことはありません。[^impression]

[^impression]: 筆者はDebian `sid`のデスクトップ環境を周囲の影響もあり常用しています。不具合がないとはいいませんが、起動しなくなるほどの致命的な問題を踏み抜くことは非常にまれです。

今回は、非常にごく稀ではあるものの、不幸にもそのような不具合を踏んでしまった場合からの復旧手順を紹介します。

<!--more-->

### 起動しなくなる不具合の例

最近の事例としては、GRUBパッケージを特定バージョンに更新すると起動できなくなる事例がありました。

* [GRUB2 2.12~rc1-7 prevent machine to boot](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1051271)

原因はパッケージの依存関係の指定の誤りにより、GRUBの一部のパッケージが期待されるバージョンへと更新されなかったというものです。

パッケージの更新時にはとくにエラーが発生しなかったため、PCをシャットダウンして翌日起動しようとして初めて問題を認識しました。

この問題に関する時系列は次のようなものでした。

* 9/5 02:22:02 grub2 2.12~rc1-7がunstableでリリース
* 9/5 14:37:14 grub2 2.12~rc1-7へのアップデートがPCに適用される
* 9/5 18:54:00 作業終了にともないPCをシャットダウン
* 9/5 23:21:02 [#1051271](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1051271) がクリティカルなバグとして報告があがる
* 9/6 02:19:50 grub2 2.12~rc1-8がunstableでリリース
* 9/6 03:50:26 grub2 2.12~rc1-9がunstableでリリース
* 9/6 PCが起動できないことを認識し復旧作業を開始

起動できないことを認識した時点での症状は次のようなものでした。

* 起動しようとして失敗してシグナル消失、BIOSの画面表示になる
* BIOSから各NVMeやHDD等は見えている(しばらくまえにNVMeを交換したのでそれがハズレを引いたかと思ったがそうではなかった)
* BIOSのブートメニューから明示的に対象となるディスクを選択しても起動できない

ディスク暗号化を施しているので、通常パスフレーズの解除が求められるが、そこまで到達できていませんでした。
問題の認識時点では、おそらくGRUBがらみで失敗しているのではないかと予想しました。

そこで、[SystemRescue](https://www.system-rescue.org/)を利用して、実施どうなっているのかを確認してみる
ことにしました。


### 起動しなくなった暗号化ディスクにアクセスする

`SystemRescue`を入れたUSBメモリから起動し、`dmesg`でみるとNVMeのパーティションは次のように見えていました。

* nvme0n1: p1 p2 p3

`fdisk -l`で確認したところ、p1がEFI領域、p2がboot領域、p3がルートファイルシステムでした。


そこで次のようにしてp3パーティションを`cryptsetup`でアクセスできるようにしました。

```bash
cryptsetup luksOpen /dev/nvme0n1p3 debianroot
```

これでパスフレーズを入力して解除すると`LVM2_member`として認識されるようになるので、ボリューム(例ではjet-vgですが適宜環境に応じて読み替えてください)をマウントします。

```bash
mount /dev/jet-vg/root /mnt/debianroot
```

問題を引き起こしたパッケージの更新に該当しそうなものがないか、`/mnt/debianroot/var/log/dpkg.log`を眺めてみるとgrubの更新がかかっていました。

```text
2023-09-05 14:37:14 configure grub-common:amd64 2.12~rc1-7 <none>
2023-09-05 14:37:14 status unpacked grub-common:amd64 2.12~rc1-7
2023-09-05 14:37:14 status half-configured grub-common:amd64 2.12~rc1-7
2023-09-05 14:37:14 status installed grub-common:amd64 2.12~rc1-7
2023-09-05 14:37:14 configure libbrlapi0.8:amd64 6.6-4 <none>
2023-09-05 14:37:14 status unpacked libbrlapi0.8:amd64 6.6-4
2023-09-05 14:37:14 status half-configured libbrlapi0.8:amd64 6.6-4
2023-09-05 14:37:14 status installed libbrlapi0.8:amd64 6.6-4
2023-09-05 14:37:14 configure libgtk-4-common:all 4.12.1+ds-3 <none>
2023-09-05 14:37:14 status unpacked libgtk-4-common:all 4.12.1+ds-3
2023-09-05 14:37:14 status half-configured libgtk-4-common:all 4.12.1+ds-3
2023-09-05 14:37:14 status triggers-awaited libgtk-4-common:all 4.12.1+ds-3
2023-09-05 14:37:14 configure libaccountsservice0:amd64 23.13.9-4 <none>
2023-09-05 14:37:14 status unpacked libaccountsservice0:amd64 23.13.9-4
2023-09-05 14:37:14 status half-configured libaccountsservice0:amd64 23.13.9-4
2023-09-05 14:37:14 status installed libaccountsservice0:amd64 23.13.9-4
2023-09-05 14:37:14 configure grub-efi-amd64-bin:amd64 2.12~rc1-7 <none>
2023-09-05 14:37:14 status unpacked grub-efi-amd64-bin:amd64 2.12~rc1-7
2023-09-05 14:37:14 status half-configured grub-efi-amd64-bin:amd64 2.12~rc1-7
2023-09-05 14:37:14 status installed grub-efi-amd64-bin:amd64 2.12~rc1-7
```

そこで、https://www.debian.org/Bugs/ からパッケージ名で検索することで、 既知のバグである #1051271 にたどりつきました。

実際、バグ報告で言及されているように `grub-efi-amd64-signed`が更新されていませんでした。

* grub-common 2.12~rc1-7
* grub-efi-amd64 2.12~rc1-7
* grub-efi-amd64-bin 2.12~rc1-7
* grub2-common 2.12~rc1-7
* grub-efi-amd64-signed **1+2.06+13**


この時点では、修正済みのパッケージに更新すればよいことがわかっていたので、次のようにしました。

* 修正済みパッケージのdebパッケージをルートファイルシステム配下の任意の場所(実行例では/opt配下)にダウンロードしておく
* EFI領域とboot領域を所定の順序でマウントする(boot領域はルートファイルシステムの/bootに、EFI領域はルートファイルシステムの/boot/efiにマウントする)
* ホストのデバイス(/dev)をchrootで使えるようにする
* chrootでルートファイルシステムにはいってパッケージを更新する

具体的には次のようなコマンドを実行しました。


```bash
cd /mnt/debianroot
mount /dev/nvme0n1p2 /mnt/debianroot/boot
mount /dev/nvme0n1p1 /mnt/debianroot/boot/efi
mount -t proc proc proc/
mount -t sysfs sys sys/
mount -o bind /dev dev/
mount -t devpts pts dev/pts/
chroot /mnt/debianroot
cd /opt/
apt install -y *.deb
```


これにより、必要なパッケージに更新差し替えることができました。

あとは、chroot環境から抜けて次のようにファイルシステムをアンマウント後にrebootすることで解決できました。

```bash
umount /mnt/debianroot/proc
umount /mnt/debianroot/sys
umount /mnt/debianroot/dev/pts
umount /mnt/debianroot/dev
umount /mnt/debianroot
```

### おわりに

今回は、DebianでPCが起動しなくなってしまったときの復旧方法の事例を紹介しました。

いざというときの復旧手順を知っていると、トラブル発生時のリカバリを迅速に行うことができます。
あまり役立つ機会がないほうがよい知見ですが、覚えておくとよいかもしれません。
