---
tags:
- ruby
- presentation
title: 'RubyKaigi 2018 - My way with Ruby #rubykaigi'
---
[RubyKaigi](http://rubykaigi.org/2018)の2日目のキーノートスピーカーとして話した須藤です。今年もクリアコードは[シルバースポンサー](http://rubykaigi.org/2018/sponsors#clear-code)としてRubyKaigiを応援しました。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2018/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2018/" title="My way with Ruby">My way with Ruby</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2018/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/rubykaigi2018mywaywithruby-99853205)

  * [発表動画](https://www.youtube.com/watch?v=d7lDhsE1jXg)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-rubykaigi-2018)

なお、RubyKaigi 2018に合わせてRabbit Slide Showをレスポンシブ対応したので、画面が小さな端末でも見やすくなりました。

### 内容

例年の内容よりキーノートっぽい内容にできるといいなぁと思って内容を考えました。最初は[「インターフェイス」というテーマでまとめていた](https://github.com/kou/rabbit-slide-kou-rubykaigi-2018/blob/master/interface.rab)のですが、うまくまとまりませんでした。そのため、他の人と違う活動という観点でまとめてみました。その結果、「Rubyでできることを増やす」・「ライブラリーをメンテナンスする」という内容になりました。キーノートっぽかったでしょ？

この話を聞いて、私と同じように「Rubyでできることを増やす」・「ライブラリーをメンテナンスする」に取り組む人が増えるといいなぁと思ってこんな内容になりました。その取り組みの中で、Ruby本体をよくする機会もでてくるとさらにいいなぁと思っています。その気になった人はぜひ取り組んでみてください。

やりたいけどどこから始めればいいんだろうという人は[Red Data Tools](https://red-data-tools.github.io/ja/)に参加するのがよいでしょう。まずは[チャットで相談したり](https://gitter.im/red-data-tools/ja)、[東京での毎月の開発イベントに参加](https://speee.connpass.com/event/89824/)してください。

やりたくてお金はあるんだけど技術が足りない・時間が足りないという会社の人は、クリアコードにお仕事として発注してください。この話を聞いた人ならクリアコードに頼めば安心だと思ってくれるはず！ご相談は[問い合わせフォーム](/contact/?type=ruby)からどうぞ。

参考情報：

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">クリアコードさんにお仕事をお願いするのは超ナイスなアウトプットが得られるのでオススメです、TDはいっぱいお願いしてます <a href="https://twitter.com/hashtag/rubykaigi?src=hash&amp;ref_src=twsrc%5Etfw">#rubykaigi</a></p>&mdash; tagomoris (@tagomoris) <a href="https://twitter.com/tagomoris/status/1002364149559521280?ref_src=twsrc%5Etfw">2018年6月1日</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


やりたいんだけど時間が足りないという人はクリアコードに入社して仕事としてやるのを検討するのはどうでしょうか。ただ、そういう仕事がないと仕事の時間ではできないですし、そもそも仕事がないと給料を払うのが難しいです。そういうことも考えた上でまだ選択肢としてよさそうなら[まずは会社説明会に申し込んで](/recruitment/)ください。（このページの内容は少し古くなっているので更新しないといけない。。。。）

あ、そうだ、[「クリアコードをいい感じにする人」](/recruitment/fine-tuner.html)として入社して、私が「Rubyでできることを増やす」・「ライブラリーをメンテナンスする」に使える時間を増やすという方法もあるかも。うーん、間接的すぎて微妙かな。。。

### RubyData Workshop

RubyKaigi 2017に引き続き、RubyKaigi 2018でもRubyData Workshop（[Data Science in Ruby](http://rubykaigi.org/2018/presentations/mrkn2.html)、[Red Data Tools Lightning Talks](http://rubykaigi.org/2018/presentations/ktou2.html)）を開催しました。Rubyでデータ処理したくなったでしょ？その気になった人はRed Data Toolsに参加して一緒に取り組んでいきましょう。

Data Science in Rubyの資料は[RubyData/rubykaigi2018](https://github.com/RubyData/rubykaigi2018)にあります。

Red Data Tools Lighting Talksの資料（の一部）は以下にあります。

  * [RubyData Workshop #RubyKaigi2018 でLTをしてきたよ - hatappi.blog](http://blog.hatappi.me/entry/2018/06/02/094005)

  * [#RubyKaigi 2018 RubyData Workshop LTで「Red Data Tools -Red Chainer-」というタイトルで発表しました。 - naitohの日記](http://naitoh.hatenablog.com/entry/2018/06/02/180129)

なお、ワークショップのおやつのどら焼きは[エス・エム・エス](https://www.bm-sms.co.jp/)さんから提供してもらいました。ありがとうございます。

### コード懇親会

Rubyは楽しくプログラムを書けるように設計されています。実際、RubyKaigiに参加するような人たちはRubyで楽しくプログラムを書いています。だったら、Rubyでコードを書く懇親会は楽しいんじゃない？というアイディアを思いつきました。それを実現する企画が「コード懇親会」です。実現にあたり[Speee](https://speee.jp/)さんと[楽天 仙台支社](https://corp.rakuten.co.jp/about/map/index__p2.html)さんに協力してもらいました。ありがとうございます。

Speeeさんには運営や飲食物の提供などイベント開催のもろもろ、楽天さんには会場提供で協力してもらいました。参加者多数のため急遽定員を増やしたのですが、それにはSpeeeさんの飲食物の追加、楽天さんの机・椅子の追加がなければ実現できませんでした。

参加したみなさんは楽しんでくれたようです。興味がある人は[アンケート結果](https://github.com/speee/code-party/tree/master/feedback)を見てみてください。

参考情報：

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">コード懇親会めっちゃ楽しい</p>&mdash; yancya (@yancya) <a href="https://twitter.com/yancya/status/1002510250811932673?ref_src=twsrc%5Etfw">2018年6月1日</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


懇親会の様子：

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">とりあえずMatzに聞いてみる@コード懇親会<a href="https://twitter.com/hashtag/rubykaigi?src=hash&amp;ref_src=twsrc%5Etfw">#rubykaigi</a> <a href="https://twitter.com/hashtag/rubykaigi2018?src=hash&amp;ref_src=twsrc%5Etfw">#rubykaigi2018</a> <a href="https://t.co/8Ip0mSFozG">pic.twitter.com/8Ip0mSFozG</a></p>&mdash; Speee Developer team (@speee_pr) <a href="https://twitter.com/speee_pr/status/1002509597268033537?ref_src=twsrc%5Etfw">2018年6月1日</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr"><a href="https://twitter.com/hashtag/rubykaigi2018?src=hash&amp;ref_src=twsrc%5Etfw">#rubykaigi2018</a> <a href="https://twitter.com/hashtag/codeparty?src=hash&amp;ref_src=twsrc%5Etfw">#codeparty</a> 昨日のコード懇親会は、RubyKaigiに参加してコード書きたい欲が高まったRubyistの発露の場というかコミュニティになっていて大変に良かった <a href="https://t.co/9w3GvSRQZ6">pic.twitter.com/9w3GvSRQZ6</a></p>&mdash; Koichiro Ohba (@koichiroo) <a href="https://twitter.com/koichiroo/status/1002699973694115840?ref_src=twsrc%5Etfw">2018年6月1日</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


来年もあるかどうかはまだわかりません。「今回よかった！」と思った人はぜひインターネット上に思ったことなどをまとめてみてください。

今回はSpeeeさんに協力してもらいましたが、いろんなスポンサーが開催するようになるといいなぁと思っています。やりたい人・やりたい企業の方はぜひやってみてください。[コード懇親会のリポジトリーのREADME](https://github.com/speee/code-party/blob/master/README.md)に説明がありますし、声をかけてもらえれば相談にのります。Speeeさんの[コード懇親会レポート](http://tech.speee.jp/entry/2018/06/02/154701)も参考にしてください。

なお、「コード懇親会」という企画をSpeeeさんが独占するよりもみんなで共有する方がSpeeeさんにとってメリットがあります。「最初に開催したのはSpeee」ということで名声が広まるからです。よさそう！と思った人はどんどん「コード懇親会」を開催してください。そのまま真似してもいいですし、アレンジを加えながら開催してもよいです。[今回の実装](https://github.com/speee/code-party/)を自由に使ってください。

### リーダブルコードサイン会

3日目のAfternoon Breakのときにジュンク堂さんがサイン会をやっていました。通りかかったら[長田さん](https://geek-out.jp/column/entry/2018/02/22/110000)に声をかけてもらったのでサイン会に混ぜてもらって[リーダブルコードの解説]({% post_url 2012-06-11-index %})にサインしていました。4,5冊売れました。「すでに持っている」という人の方が多かった気がします。いい本だから何冊あってもいいよね！

### まとめ

RubyKaigi 2018でキーノートスピーカーとして話をしてきました。クリアコードは今年もシルバースポンサーとしてRubyKaigiを応援しました。

RubyData Workshop・コード懇親会・リーダブルコードサイン会のこともまとめました。

コード懇親会の進行のこともまとめようと思ったのですが、力尽きました。いつか、機会があれば。。。
